import 'package:flutter/material.dart';
import 'package:snap_fill/DashboardScreens/dashboard.dart';
import 'package:snap_fill/DashboardScreens/first_page.dart';
import 'package:snap_fill/SignInScreen/newpassword.dart';


class ChangePasswordSuccessfully extends StatefulWidget {
  @override
  _ChangePasswordSuccessfullyState createState() => _ChangePasswordSuccessfullyState();
}

class _ChangePasswordSuccessfullyState extends State<ChangePasswordSuccessfully> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Container(
          width: MediaQuery.of(context).size.width,

          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Password Changed",style: TextStyle(color: Color(0xff00587A),fontWeight: FontWeight.w600),textScaleFactor: 1.4,),
              Image(image:AssetImage("images/successfullpassword.png"),width: 150.0,height: 150.0,),
              GestureDetector(
                onTap: (){
                  Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
                      FirstTimeScreen(1)), (Route<dynamic> route) => false);

                },
                child: Container(
                  margin: EdgeInsets.only(left:MediaQuery.of(context).size.width*0.1,right: MediaQuery.of(context).size.width*0.1),
                  decoration: BoxDecoration(
                      color: Color(0xffEEEEEE),
                      borderRadius: BorderRadius.circular(15)
                  ),
                  height: 60.0,
                  // width: MediaQuery.of(context).size.width * 0.8,
                  child: Center(
                    child: Text("Confirm",
                      style: TextStyle(color: Colors.black),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      onWillPop: (){
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
            FirstTimeScreen(1)), (Route<dynamic> route) => false);
      },
    );
  }
}
