import 'package:flutter/material.dart';
import 'package:snap_fill/SignInScreen/create_account.dart';
import 'package:snap_fill/datamodel/sharedpreferenceapp.dart';
import 'package:snap_fill/settings/userprofile.dart';


class Setting extends StatefulWidget {
  @override
  _SettingState createState() => _SettingState();
}

class _SettingState extends State<Setting> {

SharedPreferenceApp shPrefApp = SharedPreferenceApp();

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
    child: Padding(
      padding: EdgeInsets.only(left:MediaQuery.of(context).size.width*0.1,right:MediaQuery.of(context).size.width*0.1,top:MediaQuery.of(context).size.height*0.15),

    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
       GestureDetector(
         onTap: (){
           Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => UserProfile()));
         },

         child: Row(
           children: [
             Image(image:AssetImage("images/userprofileimage.png")),
             SizedBox(width:20.0),
             Text("User profile",textScaleFactor: 1.2,)
           ],
         ),

       ),

        SizedBox(height:10.0),

        Divider(
          thickness: 1.0,
        ),

       SizedBox(height:10.0),

       GestureDetector(
         onTap: (){

         },

         child: Row(
           children: [
             Icon(Icons.arrow_drop_down_circle,color: Color(0xff00587A)),
             SizedBox(width:20.0),
             Text("Help",textScaleFactor: 1.2,)
           ],
         ),

       ),

       SizedBox(height:10.0),


       Divider(
         thickness: 1.0,
       ),

       SizedBox(height:10.0),

       GestureDetector(
         onTap: (){

           LogOut();

         },

         child: Row(
           children: [
             Icon(Icons.launch,color: Color.fromARGB(0XFF, 0XEB, 0X57, 0X57)),
             SizedBox(width:20.0),
             Text("Logout",textScaleFactor: 1.2,style: TextStyle(color:Color.fromARGB(0XFF, 0XEB, 0X57, 0X57)),)
           ],
         ),

       ),




      ],
    ),

    ),


    );
  }


  void LogOut() async
  {
    await shPrefApp.SetUserPersonalInformation("", "", "", "", "", "");
    await shPrefApp.SetUserAppInformation(0, "", "");
    await shPrefApp.SetSignUpInformation(0, "");
    await shPrefApp.SetCategoryAbout("");
    await shPrefApp.SetFormAvailable(0);
    await shPrefApp.SetPubStubCategoryAbout("");
    await shPrefApp.SetPubStubFormAvailable(0);

    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
        CreateAccount()), (Route<dynamic> route) => false);

  }

}
