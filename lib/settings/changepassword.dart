import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';
import 'package:snap_fill/datamodel/apisurl.dart';
import 'package:snap_fill/datamodel/appalertdialog.dart';
import 'package:snap_fill/datamodel/sharedpreferenceapp.dart';

import 'changepasswordsuccessfully.dart';


class ChangePassword extends StatefulWidget {
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  TextEditingController _oldPassword = TextEditingController();
  TextEditingController _newPassword = TextEditingController();
  TextEditingController _confirmPassword = TextEditingController();
  bool oldPasswordVisible = false;
  bool newPasswordVisible = false;
  bool confirmPasswordVisible = false;
  final _changePasswordForm = GlobalKey<FormState>();
  SharedPreferenceApp shPrefApp = SharedPreferenceApp();
  String userName = "";




  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(left: MediaQuery
              .of(context)
              .size
              .width * 0.1, right: MediaQuery
              .of(context)
              .size
              .width * 0.1, top: MediaQuery
              .of(context)
              .size
              .height * 0.1),

          child: Form(
            key: _changePasswordForm,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Image(image: AssetImage("images/backicon2.png"))
                    ),
                    SizedBox(width: 25.0),
                    Text("Change Password", textScaleFactor: 1.4,),

                  ],
                ),

                SizedBox(height: MediaQuery
                    .of(context)
                    .size
                    .height * 0.05,),

                TextFormField(
                  controller: _oldPassword,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Password can not be empty';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.grey[200], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.grey[200], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    errorBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.red[300], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    focusedErrorBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.red[300], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),

                    hintText: 'Password',
                    fillColor: Colors.grey[200],
                    filled: true,
                    //          hintStyle: TextStyle(fontSize: 12.0),
                    border: InputBorder.none,
                    prefixIcon: Icon(
                      Icons.lock_open,
                      color: Color(0xff00587A),
                    ),
                    suffixIcon: IconButton(
                      icon: oldPasswordVisible
                          ? Icon(Icons.visibility,
                          color: Color(0xff00587A))
                          : Icon(Icons.visibility_off,
                          color: Color(0xff00587A)),
                      onPressed: OldPasswordToggle,
                    ),
                  ),
                  obscureText: oldPasswordVisible ? false : true,
                ),
                SizedBox(height: MediaQuery
                    .of(context)
                    .size
                    .height * 0.03,),

                TextFormField(
                  controller: _newPassword,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'New Password can not be empty';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.grey[200], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.grey[200], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    errorBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.red[300], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    focusedErrorBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.red[300], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),

                    hintText: 'New Password',
                    fillColor: Colors.grey[200],
                    filled: true,
                    //          hintStyle: TextStyle(fontSize: 12.0),
                    border: InputBorder.none,
                    prefixIcon: Icon(
                      Icons.lock_open,
                      color: Color(0xff00587A),
                    ),
                    suffixIcon: IconButton(
                      icon: newPasswordVisible
                          ? Icon(Icons.visibility,
                          color: Color(0xff00587A))
                          : Icon(Icons.visibility_off,
                          color: Color(0xff00587A)),
                      onPressed: NewPasswordToggle,
                    ),
                  ),
                  obscureText: newPasswordVisible ? false : true,
                ),
                SizedBox(height: MediaQuery
                    .of(context)
                    .size
                    .height * 0.03,),

                TextFormField(
                  controller: _confirmPassword,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Confirm Password can not be empty';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.grey[200], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.grey[200], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    errorBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.red[300], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    focusedErrorBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.red[300], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),

                    hintText: 'Confirm Password',
                    fillColor: Colors.grey[200],
                    filled: true,
                    //          hintStyle: TextStyle(fontSize: 12.0),
                    border: InputBorder.none,
                    prefixIcon: Icon(
                      Icons.lock_open,
                      color: Color(0xff00587A),
                    ),
                    suffixIcon: IconButton(
                      icon: confirmPasswordVisible
                          ? Icon(Icons.visibility,
                          color: Color(0xff00587A))
                          : Icon(Icons.visibility_off,
                          color: Color(0xff00587A)),
                      onPressed: ConfirmPasswordToggle,
                    ),
                  ),
                  obscureText: confirmPasswordVisible ? false : true,
                ),


                SizedBox(height: 5.0,),


                SizedBox(height: MediaQuery
                    .of(context)
                    .size
                    .height * 0.05,),


                GestureDetector(
                  onTap: () {
                    if (_changePasswordForm.currentState.validate()) {
                      //Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => ChangePasswordSuccessfully()));

                      ChangePassword();
                    }
                    else {
                      print("invalid");
                    }
//                      Navigator.of(context)
//                          .push(new MaterialPageRoute(builder: (context) => NewPassword()));

                  },
                  child: Container(
                    decoration: BoxDecoration(
                        color: Color(0xff00587A),
                        borderRadius: BorderRadius.circular(15)
                    ),
                    height: 60.0,
                    // width: MediaQuery.of(context).size.width * 0.8,
                    child: Center(
                      child: Text("Confirm",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                )


              ],
            ),
          ),

        ),
      ),
    );
  }


  void OldPasswordToggle() {
    if (oldPasswordVisible) {
      oldPasswordVisible = false;
    }
    else {
      oldPasswordVisible = true;
    }

    setState(() {
      oldPasswordVisible;
    });
  }


  void NewPasswordToggle() {
    if (newPasswordVisible) {
      newPasswordVisible = false;
    }
    else {
      newPasswordVisible = true;
    }

    setState(() {
      newPasswordVisible;
    });
  }


  void ConfirmPasswordToggle() {
    if (confirmPasswordVisible) {
      confirmPasswordVisible = false;
    }
    else {
      confirmPasswordVisible = true;
    }

    setState(() {
      confirmPasswordVisible;
    });
  }






  void ChangePassword() async{

    if(_newPassword.text != _confirmPassword.text)
    {
      ShowErrorDialog("Password and Confirm Password do not match");
    }
    else
      {
        CircularIndicator();

        try {


          userName = await shPrefApp.GetUserName();

          print("USer NAme email"+userName.toString());

          String url = changePasswordApi+userName+"?app=snapfill";

          String json = '{"password":"${_oldPassword.text}","new_password":"${_newPassword.text}"}';

          print("Json is:"+json);
          Map<String, String> header1 = {"Content-Type": "application/json","Authorization":"9edd804ee6005ffdfbcc87faa0a42170"};

          Response response = await put(url, headers: header1, body: json);

          print("Response status code:"+response.statusCode.toString());

          if(response.statusCode == 200)
          {
            Navigator.of(context, rootNavigator: true).pop();
            ToastFunction("Password Changed Successfully");
            Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => ChangePasswordSuccessfully()));

          }

          else if(response.statusCode == 400)
          {
            Navigator.of(context, rootNavigator: true).pop();
            ShowErrorDialog(jsonDecode(response.body)["message"]);
          }


          else
          {
            Navigator.of(context, rootNavigator: true).pop();
            ShowErrorDialog("Your Internet is not Working Properly");
          }
        }
        catch(e) {
          Navigator.of(context, rootNavigator: true).pop();
          ShowErrorDialog("Check Your Internet Connection");
          print("Error:"+e.toString());
        }
      }



  }

  void CircularIndicator()
  {
    AppAlertDialog.ShowDialog(context,"Changing password");

  }

  void ShowErrorDialog(String ErrorMessage)
  {

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: (){

            },
            child: Dialog(
              insetPadding:EdgeInsets.symmetric(horizontal: 25.0),
              shape: RoundedRectangleBorder(
                  borderRadius:
                  BorderRadius.circular(20.0)
              ), //this right here
              child:  Column(
                mainAxisSize: MainAxisSize.min,

                children: [
                  SizedBox(height: MediaQuery.of(context).size.height*0.015,),
                  Image(image: AssetImage("images/errorimage.png"),width: 50.0,height: 50.0,),
                  SizedBox(height: MediaQuery.of(context).size.height*0.02,),
                  Container(
                    margin: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.05,right: MediaQuery.of(context).size.width*0.05),
                    child:Text(ErrorMessage,style: TextStyle(color: Color.fromARGB(0XFF, 0X3E, 0X40, 0X46)),textScaleFactor: 1.2,textAlign: TextAlign.center,),
                  ),

                  SizedBox(height: MediaQuery.of(context).size.height*0.02,),

                  GestureDetector(

                    onTap: (){
                      Navigator.pop(context);
                    },

                    child:Container(
                      //width: MediaQuery.of(context).size.width*0.5,
                      //height: MediaQuery.of(context).size.height*0.05,
                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.05,right: MediaQuery.of(context).size.width*0.05,top: MediaQuery.of(context).size.height*0.015,bottom: MediaQuery.of(context).size.height*0.015),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50.0),
                        color: Color(0xff00587A).withOpacity(0.8),
                      ),
                      child: Text("Okay, Acknowledge",style: TextStyle(color:Color.fromARGB(0XFF, 0XFF, 0XFF, 0XFF),fontWeight: FontWeight.bold),textScaleFactor: 0.9,),
                    ),
                  ),

                  SizedBox(height: MediaQuery.of(context).size.height*0.025,),


                ],
              ),

            ),
          );
        });

  }

  void ToastFunction(String message) {
    Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Color(0xff00587A).withOpacity(0.7),
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }


}




