import 'package:flutter/material.dart';
import 'package:snap_fill/datamodel/sharedpreferenceapp.dart';

import 'changepassword.dart';


class UserProfile extends StatefulWidget {
  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  TextEditingController _userEmailName = TextEditingController();
  TextEditingController _userProfileName = TextEditingController();
  SharedPreferenceApp shPrefApp = SharedPreferenceApp();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    GetUserData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
    body:SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.only(left:MediaQuery.of(context).size.width*0.1,right:MediaQuery.of(context).size.width*0.1,top:MediaQuery.of(context).size.height*0.1),

        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
           Row(
             children: [
               GestureDetector(
                   onTap: (){
                     Navigator.pop(context);
                   },
                   child: Image(image:AssetImage("images/backicon2.png"))
               ),
               SizedBox(width:25.0),
               Text("User profile",textScaleFactor: 1.4,),

             ],
           ),

             SizedBox(height: MediaQuery.of(context).size.height*0.05,),

             Text("Name",textScaleFactor: 1.2),
           SizedBox(height: 5.0,),
           TextFormField(
             controller: _userProfileName,
             //enabled: false,
             decoration: new InputDecoration(
               focusedBorder: OutlineInputBorder(
                   borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                   borderRadius: BorderRadius.circular(10)
               ),
               enabledBorder: OutlineInputBorder(
                   borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                   borderRadius: BorderRadius.circular(10)
               ),
               errorBorder: OutlineInputBorder(
                   borderSide: BorderSide(color: Colors.red[300], width: 0.0),
                   borderRadius: BorderRadius.circular(10)
               ),
               focusedErrorBorder: OutlineInputBorder(
                   borderSide: BorderSide(color: Colors.red[300], width: 0.0),
                   borderRadius: BorderRadius.circular(10)
               ),
               fillColor: Color.fromARGB(0XFF, 0XC4, 0XC4, 0XC4).withOpacity(0.4),
               filled: true,
               border: InputBorder.none,
               hintText: 'Name',
               contentPadding: EdgeInsets.symmetric(horizontal: 8.0),
             ),
           ),


           SizedBox(height: MediaQuery.of(context).size.height*0.02,),

           Text("Email",textScaleFactor: 1.2),
           SizedBox(height: 5.0,),
           TextFormField(
             controller: _userEmailName,
             //enabled: false,

             decoration: new InputDecoration(
               focusedBorder: OutlineInputBorder(
                   borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                   borderRadius: BorderRadius.circular(10)
               ),
               enabledBorder: OutlineInputBorder(
                   borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                   borderRadius: BorderRadius.circular(10)
               ),
               errorBorder: OutlineInputBorder(
                   borderSide: BorderSide(color: Colors.red[300], width: 0.0),
                   borderRadius: BorderRadius.circular(10)
               ),
               focusedErrorBorder: OutlineInputBorder(
                   borderSide: BorderSide(color: Colors.red[300], width: 0.0),
                   borderRadius: BorderRadius.circular(10)
               ),
               fillColor: Color.fromARGB(0XFF, 0XC4, 0XC4, 0XC4).withOpacity(0.4),
               filled: true,
               border: InputBorder.none,
               hintText: 'Email',
               contentPadding: EdgeInsets.symmetric(vertical: 8.0),
             ),
           ),

           SizedBox(height: MediaQuery.of(context).size.height*0.05,),


           GestureDetector(
             onTap: (){
               Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => ChangePassword()));
             },
             child: Container(
               decoration: BoxDecoration(
                   color: Colors.white,
                   borderRadius: BorderRadius.circular(15),
                 boxShadow: [
                   BoxShadow(
                     color: Colors.grey.withOpacity(0.5),
                     spreadRadius: 2,
                     blurRadius: 5,
                     offset: Offset(0, 3), // changes position of shadow
                   ),
                 ],
               ),
               height: 55.0,
               // width: MediaQuery.of(context).size.width * 0.8,
               child: Center(
                 child: Text("Change password",
                   style: TextStyle(color: Color(0xff00587A)),
                 ),
               ),
             ),
           ),



          ],
        ),

      ),
    ),
    );
  }


  void GetUserData() async
  {
    _userProfileName.text = await shPrefApp.GetUserProfileName();
    _userEmailName.text = await shPrefApp.GetUserName();

    setState(() {

    });
  }

}
