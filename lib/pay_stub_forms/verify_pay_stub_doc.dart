import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:snap_fill/DashboardScreens/add-new-or-license.dart';
import 'package:snap_fill/DashboardScreens/document-added.dart';
import 'package:snap_fill/datamodel/apisurl.dart';
import 'package:snap_fill/datamodel/appalertdialog.dart';
import 'package:snap_fill/datamodel/sharedpreferenceapp.dart';
import 'package:snap_fill/forms_pages/new_unemployement_insurance_form.dart';

class VerifyPayStubDoc extends StatefulWidget {
  String docLocation, docCategory, docAbout;
  int check;
  Map<String,dynamic> userLicenseData= Map<String,dynamic>();

  VerifyPayStubDoc(this.docLocation,this.docCategory,this.docAbout,this.check,this.userLicenseData);
  @override
  _VerifyPayStubDocState createState() => _VerifyPayStubDocState();
}

class _VerifyPayStubDocState extends State<VerifyPayStubDoc> {

  TextEditingController _netPay = TextEditingController();
  TextEditingController _companyAddress = TextEditingController();
  TextEditingController _companyPhoneNumber = TextEditingController();
  TextEditingController _payBeginDate = TextEditingController();
  TextEditingController _payEndDate = TextEditingController();
  TextEditingController _checkDate = TextEditingController();
  TextEditingController _residentialAddress = TextEditingController();
  TextEditingController _jobTitle = TextEditingController();
  TextEditingController _employerName = TextEditingController();


  Map<String, dynamic> documentData = Map<String, dynamic>();
  Map<String, dynamic> userDetails = Map<String,dynamic>();

  SharedPreferenceApp shPrefApp = SharedPreferenceApp();

  String userId;
  Random rand = Random();
  int randomNumber;
  String documentAbout;

  int randomNounce;
  int currentNounceBase;
  String messageCorrect = "Wait";

  String emailHash;
  String jsonData = "";


  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if(widget.check == 0 )
    {
      GetUserDetailsData();
    }
    else
    {
      Timer(new Duration(milliseconds: 1500), () {
        GetUserDocumentData();

      });

    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          leading: GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Icon(Icons.arrow_back_ios, color: Colors.black,)
          ),
          backgroundColor: Colors.white,
          title: Text("Verify Document Details",
            style: TextStyle(color: Color(0xff00587A)),),
          elevation: 0.0,
        ),


        body: messageCorrect == "Wait" ?
        Container()
            :

        SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [

                Text(
                "Your paystub has been scanned succesfully and the following details have been extracted. Please look through and confirm"),
                SizedBox(
                  height: 25.0,
                ),

                //----------------- First Name
                Text("NetPay"),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(10)
                  ),

                  child: TextField(
                    controller: _netPay,
                    decoration: new InputDecoration(
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      hintText: 'NetPay',
                    ),
                  ),
                ),

                //----------------- Last Name
                SizedBox(
                  height: 25.0,
                ),
                Text("Company Address"),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(10)
                  ),

                  child: TextField(
                    controller: _companyAddress,
                    decoration: new InputDecoration(
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      hintText: 'Company Address',
                    ),
                  ),
                ),


                SizedBox(
                  height: 25.0,
                ),
                Text("Company Phone Number"),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(10)
                  ),

                  child: TextField(
                    controller: _companyPhoneNumber,
                    decoration: new InputDecoration(
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      hintText: 'Company Phone Number',
                    ),
                  ),
                ),


                SizedBox(
                  height: 25.0,
                ),
                Text("Pay Begin Date"),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(10)
                  ),

                  child: TextField(
                    controller: _payBeginDate,
                    decoration: new InputDecoration(
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      hintText: 'Pay Begin Date',
                    ),
                  ),
                ),
                SizedBox(
                  height: 25.0,
                ),
                Text("Pay End Date"),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(10)
                  ),

                  child: TextField(
                    controller: _payEndDate,
                    decoration: new InputDecoration(
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      hintText: 'Pay End Date',
                    ),
                  ),
                ),

                SizedBox(
                  height: 25.0,
                ),
                Text("Check Date"),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(10)
                  ),

                  child: TextField(
                    controller: _checkDate,
                    decoration: new InputDecoration(
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      hintText: 'Check Date',
                    ),
                  ),
                ),

                SizedBox(
                  height: 25.0,
                ),
                Text("Residential Address"),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(10)
                  ),

                  child: TextField(
                    controller: _residentialAddress,
                    decoration: new InputDecoration(
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      hintText: 'Residential Address',
                    ),
                  ),
                ),

                SizedBox(
                  height: 25.0,
                ),
                Text("Job Title"),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(10)
                  ),

                  child: TextField(
                    controller: _jobTitle,
                    decoration: new InputDecoration(
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      hintText: 'Job Title',
                    ),
                  ),
                ),

                SizedBox(
                  height: 25.0,
                ),
                Text("Employer Name"),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(10)
                  ),

                  child: TextField(
                    controller: _employerName,
                    decoration: new InputDecoration(
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      hintText: 'Employer Name',
                    ),
                  ),
                ),
                SizedBox(
                  height: 30.0,
                ),
                GestureDetector(
                  onTap: () {
                    //Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => DocumentAdded()));
                    //GetUserDocumentData();
                    VerifyData();
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        color: Color(0xff00587A),
                        borderRadius: BorderRadius.circular(15)
                    ),
                    height: 50.0,
                    // width: MediaQuery.of(context).size.width * 0.8,
                    child: Center(child: Text("Save Information",
                      textScaleFactor: 0.9,
                      style: TextStyle(color: Colors.white),),),
                  ),
                ),

                SizedBox(
                  height: 30.0,
                ),
              ],
            ),
          ),
        )
    );
  }


  void GetUserDocumentData() async
  {
    try {
      CircularIndicator("Downloading documents");
      randomNounce = await GetNounce();
      print("Random nounce:" + randomNounce.toString());
      userId = await shPrefApp.GetUserIdSignUp();

      print("user id:" + userId.toString());
      String url = "https://pscore-sandbox-2.herokuapp.com/utils/download/A/$randomNounce/$userId?app=snapfill";
      String json = '{"search_params" : { "location" : "documents", "category" : "paystubs", "about" : "${widget
          .docAbout}"},"linksOnly" : "true","optimistic":"true","useParity" : "true"}';

      // print("Json is:" + json);


      Response response = await post(url, headers: header, body: json);

      print("Response status code:" + response.statusCode.toString());
      print("Response body doc:" + response.body);


      if (response.statusCode == 200) {
        Navigator.of(context, rootNavigator: true).pop();

        documentData = jsonDecode(response.body);

        print("document data:" + documentData.toString());


//        print("total Length Name:" +
//            documentData["data"]["package"].length.toString());
        print("Form Json:" +
            documentData["data"]["package"]["forms"][0].toString());

        String data = documentData["data"]["package"]["forms"][0].toString();
//        String formJson = data.substring(1, data.indexOf(":"));
//        print(formJson);

        //Agar map hoga to decode nhi hoga lakin agar string hoa to decode lazmi ha map ma convert krna ka liya
//        String forDecodeData = data.substring(0, data.indexOf(":")+4);
        print("For decode data:"+data[1].toString());

        if(data[1] == '"') {
          userDetails = jsonDecode(documentData["data"]["package"]["forms"][0]);
        }
        else
        {
          userDetails = documentData["data"]["package"]["forms"][0];
        }
        print("Net Pay:" + userDetails["NetPay"]);

        _netPay.text = userDetails["NetPay"];
        _companyAddress.text = userDetails["Company Address"];
        _companyPhoneNumber.text = userDetails["Company Phone Number"];
        _payBeginDate.text = userDetails["Pay Begin Date"];
        _payEndDate.text = userDetails["Pay End Date"];
        _checkDate.text = userDetails["Check Date"];
        _residentialAddress.text = userDetails["Residential Address"];
        _jobTitle.text = userDetails["Job Title"];
        _employerName.text = userDetails["Employer Name"];


        messageCorrect = "Not wait";

        setState(() {

        });
      }

      else if (response.statusCode == 400) {
        //Navigator.of(context, rootNavigator: true).pop();
        //print("invalid data");
        await GetUserDocumentData();
      }
      else
      {
        Navigator.of(context, rootNavigator: true).pop();
        messageCorrect = "Not Wait";
        setState(() {

        });
      }

    }
    catch (e) {
      messageCorrect = "Not wait";
      setState(() {

      });


      print("Error:" + e.toString());
    }
  }



  void VerifyData() async
  {
    if(_netPay.text.trim().isEmpty || _companyAddress.text.trim().isEmpty || _companyPhoneNumber.text.trim().isEmpty || _payBeginDate.text.trim().isEmpty || _payEndDate.text.trim().isEmpty || _checkDate.text.trim().isEmpty || _residentialAddress.text.trim().isEmpty || _jobTitle.text.trim().isEmpty || _employerName.text.trim().isEmpty)
    {
      //Navigator.of(context, rootNavigator: true).pop();
      ShowErrorDialog("Text Fields can not be Empty");
    }
    else
    {
      CircularIndicator("Please wait while we store your information securely");

      randomNounce = await GetNounce();
      print("Random nounce:" + randomNounce.toString());
      userId = await shPrefApp.GetUserIdSignUp();
      print("user id:" + userId.toString());
      //emailHash = await GenerateMd5();
      //print("Send to Server");
      //print("Email hash:" + emailHash.toString());
      //print("Image is:"+documentImage.toString());

      documentAbout = widget.docAbout;

      jsonData = '{"paystub" :{"NetPay" : "${_netPay.text}","Company Address" : "${_companyAddress.text}","Company Phone Number" : "${_companyPhoneNumber.text}","Pay Begin Date" : "${_payBeginDate.text}","Pay End Date" : "${_payEndDate.text}","Check Date" : "${_checkDate.text}","Residential Address" : "${_residentialAddress.text}","Job Title" : "${_jobTitle.text}","Employer Name" : "${_employerName.text}"}}';

      //jsonData = '{"drivers_license_form":{"First Name":"monty","Last Name":"ui","Nationality":"nigeria","Sex":"M","Date of Birth":"12-12-2016","Issue Date":"12-18-2019","Expiry Date":"12-12-2018"}}';

      //jsonData = '{"drivers_license_form" : { "First Name" : "montyy", "Last Name" : "ui", "Nationality" : "nigeria", "Sex" : "M", "Date of Birth" : "12-12-2016", "Issue Date" : "12-08-2019", "Expiry Date" : "12-12-2018"}}';
      print('$jsonData');



      var uri = Uri.parse(
          "https://pscore-sandbox-1.herokuapp.com/utils/upload/A/$randomNounce/$userId?app=snapfill");

      print("Uri is:" + uri.toString());
      var request = MultipartRequest("POST", uri);


      request.fields['location'] = 'documents';
      request.fields['category'] = 'paystubs';
      request.fields['about'] = documentAbout;
      request.fields['upload_json'] = '$jsonData';

      var response = await request.send();

      print("Response:" + response.statusCode.toString());

      if (response.statusCode == 200) {
        Navigator.of(context, rootNavigator: true).pop();

        await shPrefApp.SetPubStubFormAvailable(1);

        Navigator.push(context, MaterialPageRoute(
            builder: (BuildContext context) => DocumentAdded()));
        //Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => NewUnEmployementInsuranceForm(0,{})));
      }
      else if(response.statusCode == 204)
      {
        Navigator.of(context, rootNavigator: true).pop();
        ShowErrorDialog("Your Document can not be updated");
      }
      else {
        Navigator.of(context, rootNavigator: true).pop();
        ShowErrorDialog("Error in Uploading File");
      }
    }


  }

  Future<int> GetNounce() async
  {
    currentNounceBase = await shPrefApp.GetUserNonceBaseSignUp();
    print("Cached nounce:" + currentNounceBase.toString());
    //print("Random rand"+rand.nextInt(1000).toString());

    randomNumber = rand.nextInt(99);
    print("Random nounce:" + randomNumber.toString());
    return currentNounceBase * randomNumber;


    //floor();
  }


  Future<String> GenerateMd5() async {
    String userName = await shPrefApp.GetUserName();
    // print("MD:"+md5.convert(utf8.encode(userName)).toString());
    return md5.convert(utf8.encode(userName)).toString();
  }

  void CircularIndicator(String message)
  {
    AppAlertDialog.ShowDialog(context,message);

  }

  void ShowErrorDialog(String ErrorMessage)
  {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: (){

            },
            child: Dialog(
              insetPadding:EdgeInsets.symmetric(horizontal: 25.0),
              shape: RoundedRectangleBorder(
                  borderRadius:
                  BorderRadius.circular(20.0)
              ), //this right here
              child:  Column(
                mainAxisSize: MainAxisSize.min,

                children: [
                  SizedBox(height: MediaQuery.of(context).size.height*0.015,),
                  Image(image: AssetImage("images/errorimage.png"),width: 50.0,height: 50.0,),
                  SizedBox(height: MediaQuery.of(context).size.height*0.02,),
                  Container(
                    margin: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.05,right: MediaQuery.of(context).size.width*0.05),
                    child:Text(ErrorMessage,style: TextStyle(color: Color.fromARGB(0XFF, 0X3E, 0X40, 0X46)),textScaleFactor: 1.2,textAlign: TextAlign.center,),
                  ),

                  SizedBox(height: MediaQuery.of(context).size.height*0.02,),

                  GestureDetector(

                    onTap: (){
                      Navigator.pop(context);
                    },

                    child:Container(
                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.05,right: MediaQuery.of(context).size.width*0.05,top: MediaQuery.of(context).size.height*0.015,bottom: MediaQuery.of(context).size.height*0.015),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50.0),
                        color: Color(0xff00587A).withOpacity(0.8),
                      ),
                      child: Text("Okay, Acknowledge",style: TextStyle(color:Color.fromARGB(0XFF, 0XFF, 0XFF, 0XFF),fontWeight: FontWeight.bold),textScaleFactor: 0.9,),
                    ),
                  ),

                  SizedBox(height: MediaQuery.of(context).size.height*0.025,),


                ],
              ),

            ),
          );
        });

  }


  void GetUserDetailsData()
  {

    print("user license data ha:"+widget.userLicenseData.toString());

    _netPay.text = widget.userLicenseData["NetPay"];
    _companyAddress.text = widget.userLicenseData["Company Address"];
    _companyPhoneNumber.text = widget.userLicenseData["Company Phone Number"];
    _payBeginDate.text = widget.userLicenseData["Pay Begin Date"];
    _payEndDate.text = widget.userLicenseData["Pay End Date"];
    _checkDate.text = widget.userLicenseData["Check Date"];
    _residentialAddress.text = widget.userLicenseData["Residential Address"];
    _jobTitle.text = widget.userLicenseData["Job Title"];
    _employerName.text = widget.userLicenseData["Employer Name"];
    setState(() {
      messageCorrect = "Not Wait";
    });



  }


}