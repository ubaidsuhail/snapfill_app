import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:crypto/crypto.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';
import 'package:snap_fill/Audio_Assistant/audio_screen.dart';
import 'package:snap_fill/Chat_Screen/chatting-page.dart';
import 'package:snap_fill/DashboardScreens/dashboard.dart';
import 'package:snap_fill/Video_Assistant/video_screen.dart';
import 'package:snap_fill/constant.dart';
import 'package:snap_fill/datamodel/apisurl.dart';
import 'package:snap_fill/datamodel/appalertdialog.dart';
import 'package:snap_fill/datamodel/sharedpreferenceapp.dart';
import 'package:snap_fill/forms_pages/form_submitted.dart';

class PayStubUnEmployementInsuranceForm extends StatefulWidget {

  //int editUnEmployment;
  Map<String,dynamic> payStubUnEmploymentForm = Map<String,dynamic>();
  PayStubUnEmployementInsuranceForm(this.payStubUnEmploymentForm);


  @override
  _PayStubUnEmployementInsuranceFormState createState() => _PayStubUnEmployementInsuranceFormState();
}

class _PayStubUnEmployementInsuranceFormState extends State<PayStubUnEmployementInsuranceForm> {

  bool _value = false;
  bool isTapped = false;
  bool isOpen = false;

  double height = 120.0;
  double speedHeight = 40.0;

  int workCanada = 2;
  int physicalAddress = 1;
  int school = 2;

  TextEditingController _employerName = TextEditingController();
  TextEditingController _employerMailingAddress = TextEditingController();
  TextEditingController _totalWages = TextEditingController();
  TextEditingController _lastEmployerName = TextEditingController();
  TextEditingController _lastEmployerMailingAddress = TextEditingController();
  TextEditingController _lastEmployerTelephoneAddress = TextEditingController();
  TextEditingController _previousEmploynment = TextEditingController();
  TextEditingController _daysWorked = TextEditingController();
  TextEditingController _paid = TextEditingController();
  TextEditingController _fullPartTime = TextEditingController();
  TextEditingController _workPerWeek = TextEditingController();
  TextEditingController _grosseryWages = TextEditingController();


  String categoryDocumentAbout = "";
  String unEmploymentFormAbout = "";

  SharedPreferenceApp shPrefApp = SharedPreferenceApp();

  String userId;
  Random rand = Random();
  int randomNumber;
  String jsonData = "";

  int randomNounce;
  int currentNounceBase;

  String waitMessage = "Waiting";

  Map<String,dynamic> unEmploymentData = Map<String,dynamic>();

  String emailHash;
  DateTime dateTime;

  String unEmploymentLocation = "";
  String unEmploymentCategory = "";
  String unEmploymentAbout = "";
  String unEmploymentNounce = "";
  String unEmploymentUserId = "";
  String unEmploymentOwner = "";


  bool workUpdate = false;
  bool physicalUpdate = false;
  bool schoolUpdate = false;


  bool military1 = false;
  bool military2 = false;
  String ethnicAnswer="American Indian or Alaska Native";
  List<Map<String,dynamic>> ethnicDropdown = [
    {
      "questions":"American Indian or Alaska Native",
    },
    {
      "questions":"Asian",
    },
    {
      "questions":"Black or African American.",
    },
    {
      "questions":"Hispanic or Latino",
    },
    {
      "questions":"Native Hawaiian or Other Pacific Islander",
    },
    {
      "questions":"White",
    },

  ];


  @override
  void initState() {
    // TODO: implement initState
    super.initState();

//    if(widget.editUnEmployment == 0)
//    {
//
//      Timer(new Duration(milliseconds: 1000), () {
//        UploadUserUnEmployementForm();
//      });
//    }
//    else
//    {
//      EditUserUnemploymentForm();
//    }

  GetUnEmploymentPayStubForm();


  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kAppMainColor,
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: GestureDetector(
            onTap: (){
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back_ios,color: Colors.black,size: 20,)
        ),
        title: Text("Unemployment Insurance Form",style: TextStyle(color: Color(0xff00587A)),),
        elevation: 0.0,
      ),


      body:
// waitMessage == "Waiting"
//          ?
//      Container()
//
//          :

      SingleChildScrollView(
        child: Column(
          children: [


            Container(
              height: MediaQuery.of(context).size.height *0.76,
              //color: Colors.red,
              child: Stack(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [

                  Container(
                    height: MediaQuery.of(context).size.height *0.76,
                    color: Colors.white,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width*0.05),
                      child: SingleChildScrollView(
                        child: Column(
                          // crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Proceed with filling the unemployment insurance form and verify the fields that have been auto-filled.",textScaleFactor: 1,),
                            SizedBox(height: 15.0,),
                            Divider(
                              thickness: 1,
                            ),
                            SizedBox(height: 15.0,),

                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("1.",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),),
                                Expanded(child: Text("Did you work in Canada during the last 18 months?",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),)),
                              ],
                            ),

                            SizedBox(height: 10.0,),
                            Row(
                                children: <Widget>[
                                  SizedBox(width: 10.0,),
                                  SizedBox(
                                    width: 20.0,
                                    height: 20.0,
                                    child: Radio(
                                      value: 1,
                                      activeColor: kAppMainColor.withOpacity(0.8),
                                      groupValue: workCanada,
                                      onChanged: workCanadaChange,
                                    ),
                                  ),
                                  SizedBox(width: 10.0,),
                                  Text(
                                    'Yes',
                                    style: TextStyle(color:kAppMainColor),textScaleFactor: 1.1,
                                  )
                                ]
                            ),

                            SizedBox(height: 10.0,),

                            Row(
                                children: <Widget>[
                                  SizedBox(width:10.0),
                                  SizedBox(
                                    width: 20.0,
                                    height: 20.0,
                                    child: Radio(
                                      value: 2,
                                      activeColor: kAppMainColor.withOpacity(0.8),
                                      groupValue: workCanada,
                                      onChanged: workCanadaChange,
                                    ),
                                  ),
                                  SizedBox(width: 10.0,),
                                  Text(
                                    'No',
                                    style: TextStyle(color:kAppMainColor),textScaleFactor: 1.1,
                                  )
                                ]
                            ),

                            SizedBox(height: 15.0,),

                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("2.",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),),
                                Expanded(child: Text("What is the Employer Name?",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),)),
                              ],
                            ),

                            SizedBox(height: 10.0,),
                            Container(
                              height: 45,
                              decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.circular(10)
                              ),

                              child: TextField(
                                controller: _employerName,
                                decoration: new InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  hintText: 'Employer Name',
                                  contentPadding: EdgeInsets.only(top:5.0,left: 10.0,right:5.0),
                                ),
                              ),
                            ),


                            SizedBox(height: 15.0,),

                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("3.",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),),
                                Expanded(child: Text("What is the Employer Mailing Address(include your city, state and ZIP code)?",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),)),
                              ],
                            ),

                            SizedBox(height: 10.0,),
                            Container(
                              height: 45,
                              decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.circular(10)
                              ),

                              child: TextField(
                                controller: _employerMailingAddress,
                                decoration: new InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  hintText: 'Employer Mailing Address',
                                  contentPadding: EdgeInsets.only(top:5.0,left: 10.0,right:5.0),
                                ),
                              ),
                            ),



                            SizedBox(height: 15.0,),

                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("4.",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),),
                                Expanded(child: Text("What is Total Wages(in \$)?",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),)),
                              ],
                            ),

                            SizedBox(height: 10.0,),
                            Container(
                              height: 45,
                              decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.circular(10)
                              ),

                              child: TextField(
                                controller: _totalWages,
                                decoration: new InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  hintText: 'Total Wages',
                                  contentPadding: EdgeInsets.only(top:5.0,left: 10.0,right:5.0),
                                ),
                              ),
                            ),


                            SizedBox(height: 15.0,),

                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("5.",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),),
                                Expanded(child: Text("What is the complete name of your very last employer?",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),)),
                              ],
                            ),

                            SizedBox(height: 10.0,),
                            Container(
                              height: 45,
                              decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.circular(10)
                              ),

                              child: TextField(
                                controller: _lastEmployerName,
                                decoration: new InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  hintText: 'Last Employer Name',
                                  contentPadding: EdgeInsets.only(top:5.0,left: 10.0,right:5.0),
                                ),
                              ),
                            ),


                            SizedBox(height: 15.0,),

                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("6.",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),),
                                Expanded(child: Text("What is the mailing address of your very last employer?",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),)),
                              ],
                            ),

                            SizedBox(height: 10.0,),
                            Container(
                              height: 45,
                              decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.circular(10)
                              ),

                              child: TextField(
                                controller: _lastEmployerMailingAddress,
                                decoration: new InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  hintText: 'Last Employer Mailing Address',
                                  contentPadding: EdgeInsets.only(top:5.0,left: 10.0,right:5.0),
                                ),
                              ),
                            ),


                            SizedBox(height: 15.0,),

                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("7.",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),),
                                Expanded(child: Text("Is the physical address of your very last employer the same as their mailing address?",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),)),
                              ],
                            ),

                            SizedBox(height: 10.0,),
                            Row(
                                children: <Widget>[
                                  SizedBox(width: 10.0,),
                                  SizedBox(
                                    width: 20.0,
                                    height: 20.0,
                                    child: Radio(
                                      value: 1,
                                      activeColor: kAppMainColor.withOpacity(0.8),
                                      groupValue: physicalAddress,
                                      onChanged: militaryPhysicalRadioChange,
                                    ),
                                  ),
                                  SizedBox(width: 10.0,),
                                  Text(
                                    'Yes',
                                    style: TextStyle(color:kAppMainColor),textScaleFactor: 1.1,
                                  )
                                ]
                            ),
                            SizedBox(height: 10.0,),
                            Row(
                                children: <Widget>[
                                  SizedBox(width:10.0),
                                  SizedBox(
                                    width: 20.0,
                                    height: 20.0,
                                    child: Radio(
                                      value: 2,
                                      activeColor: kAppMainColor.withOpacity(0.8),
                                      groupValue: physicalAddress,
                                      onChanged: militaryPhysicalRadioChange,
                                    ),
                                  ),
                                  SizedBox(width: 10.0,),
                                  Text(
                                    'No',
                                    style: TextStyle(color:kAppMainColor),textScaleFactor: 1.1,
                                  )
                                ]
                            ),



                            SizedBox(height: 15.0,),

                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("8.",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),),
                                Expanded(child: Text("What is the telephone number of your very last employer at their physical address?",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),)),
                              ],
                            ),

                            SizedBox(height: 10.0,),

                            Container(
                              height: 45,
                              decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.circular(10)
                              ),

                              child: TextField(
                                controller: _lastEmployerTelephoneAddress,
                                decoration: new InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  hintText: 'Last Employer Telephone Address',
                                  contentPadding: EdgeInsets.only(top:5.0,left: 10.0,right:5.0),
                                ),
                              ),
                            ),

                            SizedBox(height: 15.0,),

                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("9.",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),),
                                Expanded(child: Text("Are you currently working for or do you expect to work for any school or educational institution or a public or nonprofit employer performing school-related work?",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),)),
                              ],
                            ),

                            SizedBox(height: 10.0,),
                            Row(
                                children: <Widget>[
                                  SizedBox(width: 10.0,),
                                  SizedBox(
                                    width: 20.0,
                                    height: 20.0,
                                    child: Radio(
                                      value: 1,
                                      activeColor: kAppMainColor.withOpacity(0.8),
                                      groupValue: school,
                                      onChanged: schoolRadioChange,
                                    ),
                                  ),
                                  SizedBox(width: 10.0,),
                                  Text(
                                    'Yes',
                                    style: TextStyle(color:kAppMainColor),textScaleFactor: 1.1,
                                  )
                                ]
                            ),

                            SizedBox(height: 10.0,),

                            Row(
                                children: <Widget>[
                                  SizedBox(width:10.0),
                                  SizedBox(
                                    width: 20.0,
                                    height: 20.0,
                                    child: Radio(
                                      value: 2,
                                      activeColor: kAppMainColor.withOpacity(0.8),
                                      groupValue: school,
                                      onChanged: schoolRadioChange,
                                    ),
                                  ),
                                  SizedBox(width: 10.0,),
                                  Text(
                                    'No',
                                    style: TextStyle(color:kAppMainColor),textScaleFactor: 1.1,
                                  )
                                ]
                            ),

                            SizedBox(height: 15.0,),

                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("10.",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),),
                                Expanded(child: Text("Provide your employment and wages information for the past 18 months.",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),)),
                              ],
                            ),

                            SizedBox(height: 10.0,),

                            Container(
                              height: 45,
                              decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.circular(10)
                              ),

                              child: TextField(
                                controller: _previousEmploynment,
                                decoration: new InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  hintText: 'Previous Employment and Wages Information',
                                  contentPadding: EdgeInsets.only(top:5.0,left: 10.0,right:5.0),
                                ),
                              ),
                            ),

                            SizedBox(height: 15.0,),

                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("11.",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),),
                                Expanded(child: Text("Provide No of days Worked?",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),)),
                              ],
                            ),

                            SizedBox(height: 10.0,),

                            Container(
                              height: 45,
                              decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.circular(10)
                              ),

                              child: TextField(
                                controller: _daysWorked,
                                decoration: new InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  hintText: 'Days Worked',
                                  contentPadding: EdgeInsets.only(top:5.0,left: 10.0,right:5.0),
                                ),
                              ),
                            ),


                            SizedBox(height: 15.0,),

                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("12.",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),),
                                Expanded(child: Text("How were you paid? (e.g., weekly, monthly, etc.)?",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),)),
                              ],
                            ),

                            SizedBox(height: 10.0,),

                            Container(
                              height: 45,
                              decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.circular(10)
                              ),

                              child: TextField(
                                controller: _paid,
                                decoration: new InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  hintText: 'Paid',
                                  contentPadding: EdgeInsets.only(top:5.0,left: 10.0,right:5.0),
                                ),
                              ),
                            ),

                            SizedBox(height: 15.0,),

                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("13.",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),),
                                Expanded(child: Text("Did you work full-time or part-time?",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),)),
                              ],
                            ),

                            Container(
                              height: 45,
                              decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.circular(10)
                              ),

                              child: TextField(
                                controller: _fullPartTime,
                                decoration: new InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  hintText: 'Full-time or part-time',
                                  contentPadding: EdgeInsets.only(top:5.0,left: 10.0,right:5.0),
                                ),
                              ),
                            ),


                            SizedBox(height: 15.0,),

                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("14.",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),),
                                Expanded(child: Text("How many hours did you work per week?",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),)),
                              ],
                            ),

                            Container(
                              height: 45,
                              decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.circular(10)
                              ),

                              child: TextField(
                                controller: _workPerWeek,
                                decoration: new InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  hintText: 'Work per week',
                                  contentPadding: EdgeInsets.only(top:5.0,left: 10.0,right:5.0),
                                ),
                              ),
                            ),

                            SizedBox(height: 15.0,),

                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("15.",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),),
                                Expanded(child: Text("What are your gross wages for your last week of work? For Unemployment Insurance purposes, a week begins on Sunday and ends the following Saturday?",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),)),
                              ],
                            ),

                            Container(
                              height: 45,
                              decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.circular(10)
                              ),

                              child: TextField(
                                controller: _grosseryWages,
                                decoration: new InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  hintText: 'Grossery Wages',
                                  contentPadding: EdgeInsets.only(top:5.0,left: 10.0,right:5.0),
                                ),
                              ),
                            ),


                            SizedBox(height: MediaQuery.of(context).size.height*0.1,),

                          ],
                        ),
                      ),
                    ),
                  ),

                  Padding(
                    padding:  EdgeInsets.only(right: 15,bottom: speedHeight),
                    child: SpeedDial(
                      // both default to 16
                      marginRight: 18,
                      marginBottom: 20,
                      animatedIcon: AnimatedIcons.menu_close,
                      animatedIconTheme: IconThemeData(size: 22.0,color: Colors.white),
                      // this is ignored if animatedIcon is non null
                      // child: Icon(Icons.add),
                      // visible: _dialVisible,
                      // If true user is forced to close dial manually
                      // by tapping main button and overlay is not rendered.
                      closeManually: true,
                      curve: Curves.bounceIn,
                      overlayColor: Colors.black,
                      overlayOpacity: 0.5,
                      onOpen: () {
                        setState(() {
                          isOpen = true;
                        });
                      },
                      onClose: () {
                        setState(() {
                          isOpen = false;
                        });
                      },

                      tooltip: 'Speed Dial',
                      heroTag: 'speed-dial-hero-tag',
                      backgroundColor:Color(0xff00587A),
                      foregroundColor: Colors.white,
                      elevation: 0.0,
                      shape: CircleBorder(),
                      children: [
                        SpeedDialChild(
                            child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Image(image: AssetImage('images/videoicon.png'),),
                            ),
                            backgroundColor: Color(0xff00587A),
                            // label: 'First',
                            labelStyle: TextStyle(fontSize: 18.0),
                            onTap: (){
                              Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => VideoAssistant()));
                            }
                        ),
                        SpeedDialChild(
                            child: Icon(Icons.keyboard_voice),
                            backgroundColor: Color(0xff00587A),
                            // label: 'Second',
                            labelStyle: TextStyle(fontSize: 18.0),
                            onTap: (){
                              Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => AuidoAssitant()));
                            }
                        ),
                        SpeedDialChild(
                            child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Image(image: AssetImage('images/chat.png'),),
                            ),
                            backgroundColor: Color(0xff00587A),
                            // label: 'Third',
                            labelStyle: TextStyle(fontSize: 18.0),
                            onTap: (){
                              Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => Chatting()));
                            }
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),

            Container (

              decoration: BoxDecoration( color: kAppMainColor,
                  borderRadius: BorderRadius.only(topRight: Radius.circular(15),topLeft: Radius.circular(15))
              ),
              //duration: Duration (seconds: 1),
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height*0.1,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [

                  Container(
                    decoration: BoxDecoration(color: Colors.white,
                        borderRadius: BorderRadius.circular(10)
                    ),
                    height: 7,
                    width: 200.0,

                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      GestureDetector(
                        onTap: ()
                        {
//                          Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
//                              Dashboard(0)), (Route<dynamic> route) => false);
                        Navigator.pop(context);
                        },
                        child: Container(
                          decoration: BoxDecoration(color: kBlueColor.withOpacity(0.5),
                              borderRadius: BorderRadius.circular(10)
                          ),
                          height: 35,
                          width: MediaQuery.of(context).size.width * 0.35,
                          child: Center(
                            child: Text(
                              "Discard",
                              style: TextStyle(
                                  color: Colors.white
                              ),
                            ),
                          ),

                        ),
                      ),
                      GestureDetector(
                        onTap: (){
                          //alertDialog();
                          VerifyUnEmploymentForm();
                        },
                        child: Container(
                          decoration: BoxDecoration(color: Colors.white,
                              borderRadius: BorderRadius.circular(10)
                          ),
                          height: 35,
                          width: MediaQuery.of(context).size.width * 0.35,
                          child: Center(
                            child: Text(
                              "Submit",
                              style: TextStyle(
                                  color: kAppMainColor
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                ],),
            ),
          ],
        ),
      ),

    );

  }
  void alertDialog(){
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius:
                BorderRadius.circular(20.0)), //this right here
            child: Container(
              height: 200,
              child: Padding(
                padding: const EdgeInsets.all(18.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [

                    Text("Want to leave?",
                      textScaleFactor: 1.1,
                      style: TextStyle(
                          color: Color(0xff00587A)
                      ),
                    ),

                    Text("Before you leave, it’s advised that you save your progress so far in this form.",
                      textScaleFactor: 0.9,
                      style: TextStyle(
                          color: Color(0xff00587A)
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        GestureDetector(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => FormSubmitted()));
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                                border: Border.all(color: kAppMainColor,width: 0.8)
                            ),
                            height: 35.0,
                            width: 120.0,
                            child: Center(
                              child: Text("Save & Exit"),
                            ),
                          ),
                        ),
                        SizedBox(width: 12.0,),
                        Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color: kAppMainColor
                          ),
                          height: 35.0,
                          width: 90.0,
                          child: Center(
                            child: Text("Stay",style: TextStyle(color: Colors.white),),
                          ),
                        ),
                        SizedBox(width: 10.0,),
                      ],
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }


  void workCanadaChange(int radioValue)
  {
    setState(() {
      workCanada = radioValue;
    });
  }

  void militaryPhysicalRadioChange(int militaryRadioValue)
  {
    setState(() {
      physicalAddress = militaryRadioValue;
    });
  }

  void schoolRadioChange(int schoolRadioValue)
  {
    setState(() {
      school = schoolRadioValue;
    });
  }

//  void UploadUserUnEmployementForm() async
//  {
//
//    try {
//      CircularIndicator("Downloading forms");
//      categoryDocumentAbout = await shPrefApp.GetCategoryAbout();
//      randomNounce = await GetNounce();
//      print("Random nounce:"+randomNounce.toString());
//      userId = await shPrefApp.GetUserIdSignUp();
//      print("user id:"+userId.toString());
//
//      emailHash = await GenerateMd5();
//      print("Email hash:"+emailHash.toString());
//
//      dateTime = DateTime.now();
//
//      unEmploymentFormAbout = "UnemploymentForms_"+emailHash+dateTime.toString();
//
//
//      categoryDocumentAbout = await shPrefApp.GetCategoryAbout();
//
//
//
//      jsonData = '{"unemployment_form_1" : {"Did you work in a state other than California during the last 18 months?" : { "YES" : false, "NO" : false },"What is your full name?" : "documents::licenses_and_IDs::$categoryDocumentAbout::First Name;documents::licenses_and_IDs::$categoryDocumentAbout::Last Name","What is your birth date?" : "documents::licenses_and_IDs::$categoryDocumentAbout::Date of Birth","What is your gender?" : "documents::licenses_and_IDs::$categoryDocumentAbout::Sex","Do you have a Driver License issued to you by a State/entity? If Yes, What STATE" : "documents::licenses_and_IDs::$categoryDocumentAbout::Nationality!\'_INPUT_\'.split('').slice(-2)[0]","What is your mailing address? (Include your city, State, and ZIP code)" : "documents::licenses_and_IDs::$categoryDocumentAbout::Nationality","If you do not live in California, what is the name of the County in which you live?" : "","What race or ethnic group do you identify with? Check one of the following" : {},"Are you a Military Veteran?" : { "YES" : false, "NO" : false }}}';
//
//
//      print("'$jsonData'");
//
//
//      var uri =Uri.parse("https://pscore-sandbox-1.herokuapp.com/utils/upload/A/$randomNounce/$userId?app=snapfill");
//
//      print("Uri is:"+uri.toString());
//      var request = MultipartRequest("POST",uri);
//
//      request.fields['location'] = 'forms';
//      request.fields['category'] = 'unemployment';
//      request.fields['about'] = unEmploymentFormAbout;
//      request.fields['upload_json'] = '$jsonData';
//
//
//      var response = await request.send();
//
//      print("Response:"+response.statusCode.toString());
//
//
//      if(response.statusCode == 200)
//      {
//
//        await GetUserUnEmploymentData();
//
//      }
//
//      else if(response.statusCode == 400)
//      {
//        Navigator.of(context, rootNavigator: true).pop();
//        await UploadUserUnEmployementForm();
//      }
//
//      else{
//        Navigator.of(context, rootNavigator: true).pop();
//        setState(() {
//          waitMessage = "Not Wait";
//        });
//
//        ToastFunction("Connection Timed Out");
//      }
//
//    }
//    catch(e) {
//      Navigator.of(context, rootNavigator: true).pop();
//      setState(() {
//        waitMessage = "Not Wait";
//      });
//      ToastFunction("Check Your Internet Connection");
//      print("Error:"+e.toString());
//    }
//
//  }




//  void GetUserUnEmploymentData() async
//  {
//    try {
//      randomNounce = await GetNounce();
//      print("Random nounce:"+randomNounce.toString());
//      userId = await shPrefApp.GetUserIdSignUp();
//      print("user id:"+userId.toString());
//
//
//
//      categoryDocumentAbout = await shPrefApp.GetCategoryAbout();
//
//
//      String url = "https://pscore-sandbox-3.herokuapp.com/utils/download/A/$randomNounce/$userId?app=snapfill";
//      String json = '{"search_params" : {  "location" : "forms", "category" : "unemployment", "about" : "$unEmploymentFormAbout" }}';
//
//      // print("Json is:" + json);
//
//
//      Response response = await post(url, headers: header, body: json);
//
//      print("Response status code:" + response.statusCode.toString());
//      print("Response body Unemplyment:" + response.body);
//
//
//
//      if(response.statusCode == 200)
//      {
//        Navigator.of(context, rootNavigator: true).pop();
//        unEmploymentData = jsonDecode(response.body);
//
//        print("Full Name:"+unEmploymentData["data"]["package"]["forms"][0]["What is your full name?"]);
//
////        _fullName.text = unEmploymentData["data"]["package"]["forms"][0]["What is your full name?"];
////        _birthDate.text = unEmploymentData["data"]["package"]["forms"][0]["What is your birth date?"];
////        _gender.text = unEmploymentData["data"]["package"]["forms"][0]["What is your gender?"];
////        _address.text = unEmploymentData["data"]["package"]["forms"][0]["What is your mailing address? (Include your city, State, and ZIP code)"];
//
//
//        unEmploymentLocation = unEmploymentData["data"]["package"]["forms"][0]["location"];
//        unEmploymentCategory = unEmploymentData["data"]["package"]["forms"][0]["category"];
//        unEmploymentAbout = unEmploymentData["data"]["package"]["forms"][0]["about"];
//        unEmploymentNounce = unEmploymentData["data"]["package"]["forms"][0]["nonce"];
//        unEmploymentUserId = unEmploymentData["data"]["package"]["forms"][0]["user_id"];
//        unEmploymentOwner = unEmploymentData["data"]["package"]["forms"][0]["owner"];
//
//
//        setState(() {
//          waitMessage = "Not Wait";
//        });
//
//      }
//
//      else if(response.statusCode == 400)
//      {
//        Navigator.of(context, rootNavigator: true).pop();
//        await GetUserUnEmploymentData();
//      }
//
//      else{
//        Navigator.of(context, rootNavigator: true).pop();
//        setState(() {
//          waitMessage = "Not Wait";
//        });
//        ToastFunction("Connection Timed Out");
//      }
//
//    }
//    catch(e) {
//      Navigator.of(context, rootNavigator: true).pop();
//      setState(() {
//        waitMessage = "Not Wait";
//      });
//      ToastFunction("Check Your Internet Connection");
//      print("Error:"+e.toString());
//    }
//  }



  void VerifyUnEmploymentForm() async
  {

//    print(unEmploymentLocation);
//    print(unEmploymentCategory);
//    print(unEmploymentAbout);
//    print(unEmploymentNounce);
//    print(unEmploymentUserId);
//    print(unEmploymentOwner);

    //print("word radio"+workRadio.toString());

    workCanada == 1 ? workUpdate = true : workUpdate = false;

    physicalAddress == 1 ? physicalUpdate = true : physicalUpdate = false;

    school == 1 ? schoolUpdate = true : schoolUpdate = false;


    try {
      CircularIndicator("Verify UnEmployment form");
      randomNounce = await GetNounce();
      print("Random nounce:"+randomNounce.toString());
      userId = await shPrefApp.GetUserIdSignUp();
      print("user id:"+userId.toString());


      //jsonData = '{"unemployment_form_1" :{"Did you work in a state other than California during the last 18 months?": {"YES": $state1,"NO": $state2},"What is your full name?": "${_fullName.text}","What is your birth date?": "${_birthDate.text}","What is your gender?": "${_gender.text}","Do you have a Driver License issued to you by a State/entity? If Yes, What STATE": "${_state.text}","What is your mailing address? (Include your city, State, and ZIP code)": "${_address.text}","If you do not live in California, what is the name of the County in which you live?": "${_country.text}","What race or ethnic group do you identify with? Check one of the following": {"race1": "${ethnicAnswer}"},"Are you a Military Veteran?": {"YES": $military1,"NO": $military2},"location": "$unEmploymentLocation","category": "$unEmploymentCategory","about": "$unEmploymentAbout","nonce": "$unEmploymentNounce","user_id": "$unEmploymentUserId","owner": "$unEmploymentOwner"} }';

      jsonData = '{"unemployment_form_1" : {"Did you work in a state other than California during the last 18 months?" : { "YES" : "${widget.payStubUnEmploymentForm["Did you work in a state other than California during the last 18 months?"]["YES"]}", "NO" : "${widget.payStubUnEmploymentForm["Did you work in a state other than California during the last 18 months?"]["NO"]}"},"What is your full name?" : "${widget.payStubUnEmploymentForm["What is your full name?"]}","What is your birth date?" : "${widget.payStubUnEmploymentForm["What is your birth date?"]}","What is your gender?" : "${widget.payStubUnEmploymentForm["What is your gender?"]}","Do you have a Driver License issued to you by a State/entity? If Yes, What STATE" : "${widget.payStubUnEmploymentForm["Do you have a Driver License issued to you by a State/entity? If Yes, What STATE"]}","What is your mailing address? (Include your city, State, and ZIP code)" : "${widget.payStubUnEmploymentForm["What is your mailing address? (Include your city, State, and ZIP code)"]}","If you do not live in California, what is the name of the County in which you live?" : "${widget.payStubUnEmploymentForm["If you do not live in California, what is the name of the County in which you live?"]}","What race or ethnic group do you identify with? Check one of the following" : {"race1":"${widget.payStubUnEmploymentForm["What race or ethnic group do you identify with? Check one of the following"]["race1"]}"},"Are you a Military Veteran?" : { "YES" : "${widget.payStubUnEmploymentForm["Are you a Military Veteran?"]["YES"]}", "NO" : "${widget.payStubUnEmploymentForm["Are you a Military Veteran?"]["YES"]}"},"Did you work in Canada during the last 18 months?" : $workUpdate,"Employer Name" : "${_employerName.text}","Employer Mailing Address(include your city, state and ZIP code)" : "${_employerMailingAddress.text}","Total Wages(in \$)" : "${_totalWages.text}","What is the complete name of your very last employer?" : "${_lastEmployerName.text}","What is the mailing address of your very last employer?" : "${_lastEmployerMailingAddress.text}","Is the physical address of your very last employer the same as their mailing address?" : $physicalAddress,"What is the telephone number of your very last employer at their physical address?" : "${_lastEmployerTelephoneAddress.text}","Are you currently working for or do you expect to work for any school or educational institution or a public or nonprofit employer performing school-related work?" : $schoolUpdate,"Provide your employment and wages information for the past 18 months.":"${_previousEmploynment.text}","Dates Worked":"${_daysWorked.text}","How were you paid? (e.g., weekly, monthly, etc.)?":"${_paid.text}","Did you work full-time or part-time?":"${_fullPartTime.text}","How many hours did you work per week?":"${_workPerWeek.text}","What are your gross wages for your last week of work? For Unemployment Insurance purposes, a week begins on Sunday and ends the following Saturday.":"${_grosseryWages.text}"}}';

      print("'$jsonData'");


      var uri =Uri.parse("https://pscore-sandbox-1.herokuapp.com/utils/upload/A/$randomNounce/$userId?app=snapfill");

      print("Uri is:"+uri.toString());
      var request = MultipartRequest("POST",uri);


      request.fields['location'] = 'forms';
      request.fields['category'] = 'unemployment';
      request.fields['about'] = widget.payStubUnEmploymentForm["about"];;
      request.fields['upload_json'] = '$jsonData';


      var response = await request.send();

      print("Response:"+response.statusCode.toString());


      if(response.statusCode == 200)
      {
        Navigator.of(context, rootNavigator: true).pop();
        print("response");
        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => FormSubmitted()));

      }

      else if(response.statusCode == 400)
      {
        Navigator.of(context, rootNavigator: true).pop();
        ShowErrorDialog("Error:Please Retry");
      }

      else{
        Navigator.of(context, rootNavigator: true).pop();
        ShowErrorDialog("Connection Timed Out");
      }

    }
    catch(e) {
      Navigator.of(context, rootNavigator: true).pop();
      ShowErrorDialog("Check Your Internet Connection");
      print("Error:"+e.toString());
    }

  }


  void GetUnEmploymentPayStubForm()
  {

    _employerName.text = widget.payStubUnEmploymentForm["Employer Name"];
    _employerMailingAddress.text =  widget.payStubUnEmploymentForm["Employer Mailing Address(include your city, state and ZIP code)"];
    _totalWages.text = widget.payStubUnEmploymentForm["Total Wages(in \$)"];
    _lastEmployerName.text =widget.payStubUnEmploymentForm["What is the complete name of your very last employer?"];
    _lastEmployerMailingAddress.text =widget.payStubUnEmploymentForm["What is the mailing address of your very last employer?"];
    _lastEmployerTelephoneAddress.text =widget.payStubUnEmploymentForm["What is the telephone number of your very last employer at their physical address?"];
    _previousEmploynment.text =widget.payStubUnEmploymentForm["Provide your employment and wages information for the past 18 months."];
    _daysWorked.text = widget.payStubUnEmploymentForm["Dates Worked"];
    _paid.text = widget.payStubUnEmploymentForm["How were you paid? (e.g., weekly, monthly, etc.)?"];
    _fullPartTime.text = widget.payStubUnEmploymentForm["Did you work full-time or part-time?"];
    _workPerWeek.text = widget.payStubUnEmploymentForm["How many hours did you work per week?"];
    _grosseryWages.text = widget.payStubUnEmploymentForm["What are your gross wages for your last week of work? For Unemployment Insurance purposes, a week begins on Sunday and ends the following Saturday."];


    widget.payStubUnEmploymentForm["Did you work in Canada during the last 18 months?"] == false ?  workCanada = 2 : workCanada = 1;
    widget.payStubUnEmploymentForm["Is the physical address of your very last employer the same as their mailing address?"] == false ? physicalAddress = 2 : physicalAddress = 1;
    widget.payStubUnEmploymentForm["Are you currently working for or do you expect to work for any school or educational institution or a public or nonprofit employer performing school-related work?"] == false ? school = 2 : school = 1;






    setState(() {

    });


  }





  Future<int> GetNounce() async
  {
    currentNounceBase =  await shPrefApp.GetUserNonceBaseSignUp();
    print("Cached nounce:"+currentNounceBase.toString());
    //print("Random rand"+rand.nextInt(1000).toString());

    randomNumber = rand.nextInt(99);
    print("Random Number:"+randomNumber.toString());
    return currentNounceBase * randomNumber;

  }


  Future<String> GenerateMd5() async {

    String userName = await shPrefApp.GetUserName();
    // print("MD:"+md5.convert(utf8.encode(userName)).toString());
    return md5.convert(utf8.encode(userName)).toString();
  }




  void CircularIndicator(String message)
  {
    AppAlertDialog.ShowDialog(context,message);

  }
  void ShowErrorDialog(String ErrorMessage)
  {

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: (){

            },
            child: Dialog(
              insetPadding:EdgeInsets.symmetric(horizontal: 25.0),
              shape: RoundedRectangleBorder(
                  borderRadius:
                  BorderRadius.circular(20.0)
              ), //this right here
              child:  Column(
                mainAxisSize: MainAxisSize.min,

                children: [
                  SizedBox(height: MediaQuery.of(context).size.height*0.015,),
                  Image(image: AssetImage("images/errorimage.png"),width: 50.0,height: 50.0,),
                  SizedBox(height: MediaQuery.of(context).size.height*0.02,),
                  Container(
                    margin: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.05,right: MediaQuery.of(context).size.width*0.05),
                    child:Text(ErrorMessage,style: TextStyle(color: Color.fromARGB(0XFF, 0X3E, 0X40, 0X46)),textScaleFactor: 1.2,textAlign: TextAlign.center,),
                  ),

                  SizedBox(height: MediaQuery.of(context).size.height*0.02,),

                  GestureDetector(

                    onTap: (){
                      Navigator.pop(context);
                    },

                    child:Container(
                      //width: MediaQuery.of(context).size.width*0.5,
                      //height: MediaQuery.of(context).size.height*0.05,
                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.05,right: MediaQuery.of(context).size.width*0.05,top: MediaQuery.of(context).size.height*0.015,bottom: MediaQuery.of(context).size.height*0.015),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50.0),
                        color: Color(0xff00587A).withOpacity(0.8),
                      ),
                      child: Text("Okay, Acknowledge",style: TextStyle(color:Color.fromARGB(0XFF, 0XFF, 0XFF, 0XFF),fontWeight: FontWeight.bold),textScaleFactor: 0.9,),
                    ),
                  ),

                  SizedBox(height: MediaQuery.of(context).size.height*0.025,),


                ],
              ),

            ),
          );
        });

  }

  void ToastFunction(String message)
  {
    Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Color(0xff00587A).withOpacity(0.7),
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }




}
