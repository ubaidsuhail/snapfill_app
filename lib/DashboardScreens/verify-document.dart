import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:crypto/crypto.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:snap_fill/DashboardScreens/add-new-or-license.dart';
import 'package:snap_fill/DashboardScreens/document-added.dart';
import 'package:snap_fill/datamodel/apisurl.dart';
import 'package:snap_fill/datamodel/appalertdialog.dart';
import 'package:snap_fill/datamodel/sharedpreferenceapp.dart';
import 'package:snap_fill/forms_pages/new_unemployement_insurance_form.dart';

class VerifyDocument extends StatefulWidget {
  String docLocation, docCategory, docAbout;
  int check;
  Map<String,dynamic> userLicenseData= Map<String,dynamic>();

  VerifyDocument(this.docLocation,this.docCategory,this.docAbout,this.check,this.userLicenseData);
  @override
  _EditDocumentState createState() => _EditDocumentState();
}

class _EditDocumentState extends State<VerifyDocument> {

  TextEditingController _firstName = TextEditingController();
  TextEditingController _lastName = TextEditingController();
  TextEditingController _nationality = TextEditingController();
  TextEditingController _sex = TextEditingController();
  TextEditingController _dateOfBirth = TextEditingController();
  TextEditingController _issueDate = TextEditingController();
  TextEditingController _expiryDate = TextEditingController();
  Map<String, dynamic> documentData = Map<String, dynamic>();
  Map<String, dynamic> userDetails = Map<String,dynamic>();

  SharedPreferenceApp shPrefApp = SharedPreferenceApp();

  String userId;
  Random rand = Random();
  int randomNumber;
  String documentAbout;

  int randomNounce;
  int currentNounceBase;
  String messageCorrect = "Wait";

  String emailHash;
  String jsonData = "";


  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if(widget.check == 0 )
      {
        GetUserDetailsData();
      }
      else
        {
          Timer(new Duration(milliseconds: 1500), () {
            GetUserDocumentData();

          });

    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          leading: GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Icon(Icons.arrow_back_ios, color: Colors.black,)
          ),
          backgroundColor: Colors.white,
          title: Text("Verify Document Details",
            style: TextStyle(color: Color(0xff00587A)),),
          elevation: 0.0,
        ),


        body: messageCorrect == "Wait" ?
        Container()
            :

        SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [

                Text(
                    "Your national passport has been scanned succesfully and the following details have been extracted. Please look through and confirm"),
                SizedBox(
                  height: 25.0,
                ),

                //----------------- First Name
                Text("First Name"),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(10)
                  ),

                  child: TextField(
                    controller: _firstName,
                    decoration: new InputDecoration(
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      hintText: 'First Name',
                    ),
                  ),
                ),

                //----------------- Last Name
                SizedBox(
                  height: 25.0,
                ),
                Text("Last Name"),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(10)
                  ),

                  child: TextField(
                    controller: _lastName,
                    decoration: new InputDecoration(
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      hintText: 'Last Name',
                    ),
                  ),
                ),


                SizedBox(
                  height: 25.0,
                ),
                Text("Nationality"),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(10)
                  ),

                  child: TextField(
                    controller: _nationality,
                    decoration: new InputDecoration(
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      hintText: 'Nationality',
                    ),
                  ),
                ),


                SizedBox(
                  height: 25.0,
                ),
                Text("Sex"),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(10)
                  ),

                  child: TextField(
                    controller: _sex,
                    decoration: new InputDecoration(
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      hintText: 'Sex',
                    ),
                  ),
                ),
                SizedBox(
                  height: 25.0,
                ),
                Text("Date of Birth"),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(10)
                  ),

                  child: TextField(
                    controller: _dateOfBirth,
                    decoration: new InputDecoration(
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      hintText: '01-01-1981',
                    ),
                  ),
                ),

                SizedBox(
                  height: 25.0,
                ),
                Text("Issue Date"),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(10)
                  ),

                  child: TextField(
                    controller: _issueDate,
                    decoration: new InputDecoration(
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      hintText: '11-29-2019',
                    ),
                  ),
                ),

                SizedBox(
                  height: 25.0,
                ),
                Text("Expiry Date"),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(10)
                  ),

                  child: TextField(
                    controller: _expiryDate,
                    decoration: new InputDecoration(
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      hintText: '11-30-2009',
                    ),
                  ),
                ),
                SizedBox(
                  height: 30.0,
                ),
                GestureDetector(
                  onTap: () {
                    //Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => DocumentAdded()));
                    //GetUserDocumentData();
                    VerifyData();
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        color: Color(0xff00587A),
                        borderRadius: BorderRadius.circular(15)
                    ),
                    height: 50.0,
                    // width: MediaQuery.of(context).size.width * 0.8,
                    child: Center(child: Text("Save Information",
                      textScaleFactor: 0.9,
                      style: TextStyle(color: Colors.white),),),
                  ),
                ),

                SizedBox(
                  height: 30.0,
                ),
              ],
            ),
          ),
        )
    );
  }


  void GetUserDocumentData() async
  {
    try {
      CircularIndicator("Downloading documents");
      randomNounce = await GetNounce();
      print("Random nounce:" + randomNounce.toString());
      userId = await shPrefApp.GetUserIdSignUp();

      print("user id:" + userId.toString());
      String url = "https://pscore-sandbox-2.herokuapp.com/utils/download/A/$randomNounce/$userId?app=snapfill";
      String json = '{"search_params" : { "location" : "documents", "category" : "licenses_and_IDs", "about" : "${widget
          .docAbout}"},"linksOnly" : "true","optimistic":"true","useParity" : "true"}';

     // print("Json is:" + json);


      Response response = await post(url, headers: header, body: json);

      print("Response status code:" + response.statusCode.toString());
      print("Response body doc:" + response.body);


      if (response.statusCode == 200) {
        Navigator.of(context, rootNavigator: true).pop();

        documentData = jsonDecode(response.body);

        print("document data:" + documentData.toString());


//        print("total Length Name:" +
//            documentData["data"]["package"].length.toString());
        print("Form Json:" +
            documentData["data"]["package"]["forms"][0].toString());

        String data = documentData["data"]["package"]["forms"][0].toString();
//        String formJson = data.substring(1, data.indexOf(":"));
//        print(formJson);

        //Agar map hoga to decode nhi hoga lakin agar string hoa to decode lazmi ha map ma convert krna ka liya
//        String forDecodeData = data.substring(0, data.indexOf(":")+4);
print("For decode data:"+data[1].toString());

        if(data[1] == '"') {
          userDetails = jsonDecode(documentData["data"]["package"]["forms"][0]);
        }
        else
          {
            userDetails = documentData["data"]["package"]["forms"][0];
          }
        print("First Names:" + userDetails["First Name"]);

        _firstName.text = userDetails["First Name"];
        _lastName.text = userDetails["Last Name"];
        _nationality.text = userDetails["Nationality"];
        _sex.text = userDetails["Sex"];
        _dateOfBirth.text = userDetails["Date of Birth"];
        _issueDate.text = userDetails["Issue Date"];
        _expiryDate.text = userDetails["Expiry Date"];


        messageCorrect = "Not wait";

        setState(() {

        });
      }

      else if (response.statusCode == 400) {
        //Navigator.of(context, rootNavigator: true).pop();
        //print("invalid data");
        await GetUserDocumentData();
      }
      else
        {
          Navigator.of(context, rootNavigator: true).pop();
    messageCorrect = "Not Wait";
    setState(() {

    });
        }

    }
    catch (e) {
      messageCorrect = "Not wait";
      setState(() {

      });


      print("Error:" + e.toString());
    }
  }



  void VerifyData() async
  {
    if(_firstName.text.trim().isEmpty || _lastName.text.trim().isEmpty || _nationality.text.trim().isEmpty || _sex.text.trim().isEmpty || _dateOfBirth.text.trim().isEmpty || _issueDate.text.trim().isEmpty || _expiryDate.text.trim().isEmpty)
      {
        //Navigator.of(context, rootNavigator: true).pop();
        ShowErrorDialog("Text Fields can not be Empty");
      }
      else
        {
          CircularIndicator("Please wait while we store your information securely");

          randomNounce = await GetNounce();
          print("Random nounce:" + randomNounce.toString());
          userId = await shPrefApp.GetUserIdSignUp();
          print("user id:" + userId.toString());
          //emailHash = await GenerateMd5();
          //print("Send to Server");
          //print("Email hash:" + emailHash.toString());
          //print("Image is:"+documentImage.toString());

          documentAbout = widget.docAbout;

          jsonData = '{"drivers_license_form":{"First Name":"${_firstName
              .text}","Last Name":"${_lastName.text}","Nationality":"${_nationality
              .text}","Sex":"${_sex.text}","Date of Birth":"${_dateOfBirth
              .text}","Issue Date":"${_issueDate.text}","Expiry Date":"${_expiryDate
              .text}"}}';

          //jsonData = '{"drivers_license_form":{"First Name":"monty","Last Name":"ui","Nationality":"nigeria","Sex":"M","Date of Birth":"12-12-2016","Issue Date":"12-18-2019","Expiry Date":"12-12-2018"}}';

          //jsonData = '{"drivers_license_form" : { "First Name" : "montyy", "Last Name" : "ui", "Nationality" : "nigeria", "Sex" : "M", "Date of Birth" : "12-12-2016", "Issue Date" : "12-08-2019", "Expiry Date" : "12-12-2018"}}';
          print('$jsonData');



          var uri = Uri.parse(
              "https://pscore-sandbox-1.herokuapp.com/utils/upload/A/$randomNounce/$userId?app=snapfill");

          print("Uri is:" + uri.toString());
          var request = MultipartRequest("POST", uri);


          request.fields['location'] = 'documents';
          request.fields['category'] = 'licenses_and_IDs';
          request.fields['about'] = documentAbout;
          request.fields['upload_json'] = '$jsonData';

          var response = await request.send();

          print("Response:" + response.statusCode.toString());

          if (response.statusCode == 200) {
            Navigator.of(context, rootNavigator: true).pop();

            await shPrefApp.SetFormAvailable(1);

            Navigator.push(context, MaterialPageRoute(
                builder: (BuildContext context) => DocumentAdded()));
            //Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => NewUnEmployementInsuranceForm(0,{})));
          }
          else if(response.statusCode == 204)
          {
            Navigator.of(context, rootNavigator: true).pop();
            ShowErrorDialog("Your Document can not be updated");
          }
          else {
            Navigator.of(context, rootNavigator: true).pop();
            ShowErrorDialog("Error in Uploading File");
          }
        }


  }

  Future<int> GetNounce() async
  {
    currentNounceBase = await shPrefApp.GetUserNonceBaseSignUp();
    print("Cached nounce:" + currentNounceBase.toString());
    //print("Random rand"+rand.nextInt(1000).toString());

    randomNumber = rand.nextInt(99);
    print("Random nounce:" + randomNumber.toString());
    return currentNounceBase * randomNumber;


    //floor();
  }


  Future<String> GenerateMd5() async {
    String userName = await shPrefApp.GetUserName();
    // print("MD:"+md5.convert(utf8.encode(userName)).toString());
    return md5.convert(utf8.encode(userName)).toString();
  }

  void CircularIndicator(String message)
  {
    AppAlertDialog.ShowDialog(context,message);

  }

  void ShowErrorDialog(String ErrorMessage)
  {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: (){

            },
            child: Dialog(
              insetPadding:EdgeInsets.symmetric(horizontal: 25.0),
              shape: RoundedRectangleBorder(
                  borderRadius:
                  BorderRadius.circular(20.0)
              ), //this right here
              child:  Column(
                mainAxisSize: MainAxisSize.min,

                children: [
                  SizedBox(height: MediaQuery.of(context).size.height*0.015,),
                  Image(image: AssetImage("images/errorimage.png"),width: 50.0,height: 50.0,),
                  SizedBox(height: MediaQuery.of(context).size.height*0.02,),
                  Container(
                    margin: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.05,right: MediaQuery.of(context).size.width*0.05),
                    child:Text(ErrorMessage,style: TextStyle(color: Color.fromARGB(0XFF, 0X3E, 0X40, 0X46)),textScaleFactor: 1.2,textAlign: TextAlign.center,),
                  ),

                  SizedBox(height: MediaQuery.of(context).size.height*0.02,),

                  GestureDetector(

                    onTap: (){
                      Navigator.pop(context);
                    },

                    child:Container(
                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.05,right: MediaQuery.of(context).size.width*0.05,top: MediaQuery.of(context).size.height*0.015,bottom: MediaQuery.of(context).size.height*0.015),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50.0),
                        color: Color(0xff00587A).withOpacity(0.8),
                      ),
                      child: Text("Okay, Acknowledge",style: TextStyle(color:Color.fromARGB(0XFF, 0XFF, 0XFF, 0XFF),fontWeight: FontWeight.bold),textScaleFactor: 0.9,),
                    ),
                  ),

                  SizedBox(height: MediaQuery.of(context).size.height*0.025,),


                ],
              ),

            ),
          );
        });

  }


  void GetUserDetailsData()
  {
    _firstName.text = widget.userLicenseData["First Name"];
    _lastName.text = widget.userLicenseData["Last Name"];
    _nationality.text = widget.userLicenseData["Nationality"];
    _sex.text = widget.userLicenseData["Sex"];
    _dateOfBirth.text = widget.userLicenseData["Date of Birth"];
    _issueDate.text = widget.userLicenseData["Issue Date"];
    _expiryDate.text = widget.userLicenseData["Expiry Date"];

    setState(() {
      messageCorrect = "Not Wait";
    });



  }


}