import 'package:flutter/material.dart';
import 'package:snap_fill/Audio_Assistant/audio_screen.dart';
import 'package:snap_fill/Chat_Screen/chatting-page.dart';
import 'package:snap_fill/DashboardScreens/Scan_Licence_2.dart';
import 'package:snap_fill/DashboardScreens/camera_screen.dart';
import 'package:snap_fill/DashboardScreens/dashboard.dart';

class FirstTimeScreen extends StatefulWidget {
  int firstTimeSetup;
  FirstTimeScreen(this.firstTimeSetup);
  @override
  _AddNewLicenseState createState() => _AddNewLicenseState();
}

class _AddNewLicenseState extends State<FirstTimeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [

            SizedBox(
              height: 50.0,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(widget.firstTimeSetup == 0 ? "First Time Setup" : "Add new license or ID",
                    textScaleFactor: 1.4,
                    style: TextStyle(color: Color(0xff00587A),fontWeight: FontWeight.w600),
                  ),
                  widget.firstTimeSetup == 0
                  ?
                  GestureDetector(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => Dashboard(1)));
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(vertical: 12.0,horizontal: 12.0),
                      decoration: BoxDecoration(
                          color: Color.fromRGBO(0, 54, 61, 0.1),
                        borderRadius: BorderRadius.circular(10)
                      ),

                        // background: rgba(0, 54, 61, 0.1)

                      child: Center(
                        child: Text("Skip Setup",textScaleFactor: 1.1,style: TextStyle(color: Color(0xff00587A)),),
                      ),
                    ),
                  )
                      :
                      Container(),
                ],
              ),
            ),
            SizedBox(
              height: 150.0,
            ),
            Center(
              child: Container(
                width: MediaQuery.of(context).size.width * 0.8,
                child: Text(widget.firstTimeSetup == 0 ? "Let us help you get started with snapfill" : "How do you want proceed?",
                  textScaleFactor: 1.5,
                  style: TextStyle(color: Colors.black,fontSize: 15),
                ),
              ),
            ),
            SizedBox(
              height: 30.0,
            ),

            Center(
              child: GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => Dashboard(1)));
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                            color: Colors.blueGrey.withOpacity(0.15),
                            spreadRadius: 5,
                            blurRadius: 8
                        )
                      ],
                      borderRadius: BorderRadius.circular(10)
                  ),

                  height: 70.0,
                  width: MediaQuery.of(context).size.width * 0.8,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: Row(
                      children: [
                        Image(image: AssetImage('images/camera.png'),width: 35.0,height: 35.0,),
                        SizedBox(width: 15,),
                        Expanded(child: Text("Continue with document Scanner"))
                      ],
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 25.0,
            ),

            Center(
              child: GestureDetector(
                onTap: (){
                  Navigator.of(context)
                      .push(new MaterialPageRoute(builder: (context) => Chatting()));
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                            color: Colors.blueGrey.withOpacity(0.15),
                            spreadRadius: 5,
                            blurRadius: 8
                        )
                      ],
                      borderRadius: BorderRadius.circular(10)
                  ),

                  height: 70.0,
                  width: MediaQuery.of(context).size.width * 0.8,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: Row(
                      children: [
                        Image(image: AssetImage('images/chat-bot.png'),width: 35.0,height: 35.0,),
                        SizedBox(width: 15,),
                        Text("Continue with chat bot")
                      ],
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 25.0,
            ),

            Center(
              child: GestureDetector(
                onTap: (){
                  Navigator.of(context)
                      .push(new MaterialPageRoute(builder: (context) => AuidoAssitant()));
                },
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                          color: Colors.blueGrey.withOpacity(0.15),
                          spreadRadius: 5,
                          blurRadius: 8
                      )
                    ],
                    borderRadius: BorderRadius.circular(10),

                  ),

                  height: 70.0,
                  width: MediaQuery.of(context).size.width * 0.8,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: Row(
                      children: [
                        Image(image: AssetImage('images/audio.png'),width: 35.0,height: 35.0,),
                        SizedBox(width: 15,),
                        Text("Continue with audio assistant")
                      ],
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 15.0,
            ),

          ],
        ),
      ),
    );
  }
}
