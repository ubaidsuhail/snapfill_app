import 'package:flutter/material.dart';
import 'package:snap_fill/DashboardScreens/dashboard.dart';
import 'package:snap_fill/constant.dart';
import 'package:snap_fill/forms_pages/new_unemployement_insurance_form.dart';

class DocumentAdded extends StatefulWidget {
  @override
  _FormSubmittedState createState() => _FormSubmittedState();
}

class _FormSubmittedState extends State<DocumentAdded> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: Center(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Document added"),
              Image(image: AssetImage('images/form-sub.png')),

              GestureDetector(
                onTap: (){
                  Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
                      Dashboard(1)), (Route<dynamic> route) => false);

                 // Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => NewUnEmployementInsuranceForm(0,{})));
                },
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: kAppMainColor
                  ),
                  height: 60.0,
                  width: MediaQuery.of(context).size.width * 0.8,
                  child: Center(
                    child: Text(
                      'Continue',style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
