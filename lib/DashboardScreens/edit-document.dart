import 'package:flutter/material.dart';
import 'package:snap_fill/DashboardScreens/add-new-or-license.dart';

class EditDocument extends StatefulWidget {
  @override
  _EditDocumentState createState() => _EditDocumentState();
}

class _EditDocumentState extends State<EditDocument> {
  String Specialization = "United States of America";
  List States = [
    {'id': 0, 'name': 'United States of America', 'image': 'assets/icons/surgeon_doctor.jpg'},
    {'id': 1, 'name': 'Patient', 'image': 'assets/icons/patient.png'},
    {'id': 2, 'name': 'Nurse', 'image': 'assets/icons/nurse_main.jpg'},
    {'id': 3, 'name': 'Partner', 'image': 'assets/icons/vendor.png'}
  ];
  String gender = "Female";
  List genderType = [
    {'id': 0, 'name': 'Female', 'image': 'assets/icons/surgeon_doctor.jpg'},
    {'id': 1, 'name': 'Male', 'image': 'assets/icons/patient.png'},

  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: Icon(Icons.arrow_back_ios,color: Colors.black,),
        backgroundColor: Colors.white,
        title: Text("Edit Document Details",style: TextStyle(color: Color(0xff00587A)),),
        elevation: 0.0,
      ),


      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 25.0,
              ),

              //----------------- First Name
              Text("First Name"),
            SizedBox(
              height: 10.0,
            ),
          Container(
            decoration: BoxDecoration(
              color: Colors.grey[200],
                borderRadius: BorderRadius.circular(10)
            ),

            child: TextField(

              decoration: new InputDecoration(
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                  borderRadius: BorderRadius.circular(10)
                ),
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                    borderRadius: BorderRadius.circular(10)
                ),
                hintText: 'Mobile Number',
              ),
            ),
          ),

              //----------------- Last Name
              SizedBox(
                height: 25.0,
              ),
              Text("Last Name"),
              SizedBox(
                height: 10.0,
              ),
              Container(
                decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(10)
                ),

                child: TextField(

                  decoration: new InputDecoration(
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    hintText: 'Mobile Number',
                  ),
                ),
              ),


              SizedBox(
                height: 25.0,
              ),
              Text("Nationality"),
              SizedBox(
                height: 10.0,
              ),
          Container(
                   height: 55.0,
                   // width: MediaQuery.of(context).size.width * 0.9,
                   decoration: BoxDecoration(
                       color: Colors.grey[200],
                       borderRadius: BorderRadius.circular(
                         10.0,
                       ),

                   ),
                   padding: EdgeInsets.only(left: 0, right: 0, top: 0),
//                    color: Colors.white,
                   child: Row(
                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                     children: <Widget>[
                       Expanded(
                         child: DropdownButtonHideUnderline(
                           child: ButtonTheme(
                             alignedDropdown: true,
                             child: DropdownButton<String>(
                               value: Specialization,
                               iconSize: 30,
                               icon: (Icon(
                                 Icons.arrow_drop_down,
                                 color: Colors.black,
                               )),
                               style: TextStyle(
                                 color: Colors.black54,
                                 fontSize: 16,
                               ),
                               hint: Text(
                                 'Select Usertype',
                                 textScaleFactor: 0.85,
                                 style: TextStyle(color: Colors.black),
                               ),
                               onChanged: (String newValue) {
                                 setState(() {
                                   Specialization = newValue;
                                   print(Specialization);
                                 });
                               },
                               items: States?.map((item) {
                                     return new DropdownMenuItem(
                                       child: new Text(
                                         item['name'],
                                         style: TextStyle(
                                             color: Colors.black),
                                       ),
                                       value: item['name'].toString(),
                                     );
                                   })?.toList() ??
                                   [],
                             ),
                           ),
                         ),
                       ),
                     ],
                   ),
                 ),


              SizedBox(
                height: 25.0,
              ),
              Text("Sex"),
              SizedBox(
                height: 10.0,
              ),
              Container(
                height: 55.0,
                // width: MediaQuery.of(context).size.width * 0.9,
                decoration: BoxDecoration(
                  color: Colors.grey[200],
                  borderRadius: BorderRadius.circular(
                    10.0,
                  ),

                ),
                padding: EdgeInsets.only(left: 0, right: 0, top: 0),
//                    color: Colors.white,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      child: DropdownButtonHideUnderline(
                        child: ButtonTheme(
                          alignedDropdown: true,
                          child: DropdownButton<String>(
                            value: gender,
                            iconSize: 30,
                            icon: (Icon(
                              Icons.arrow_drop_down,
                              color: Colors.black,
                            )),
                            style: TextStyle(
                              color: Colors.black54,
                              fontSize: 16,
                            ),
                            hint: Text(
                              'Select Usertype',
                              textScaleFactor: 0.85,
                              style: TextStyle(color: Colors.black),
                            ),
                            onChanged: (String newValue) {
                              setState(() {
                                gender = newValue;
                                print(gender);
                              });
                            },
                            items: genderType?.map((item) {
                              return new DropdownMenuItem(
                                child: new Text(
                                  item['name'],
                                  style: TextStyle(
                                      color: Colors.black),
                                ),
                                value: item['name'].toString(),
                              );
                            })?.toList() ??
                                [],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 25.0,
              ),
              Text("Date of Birth"),
              SizedBox(
                height: 10.0,
              ),
              Container(
                decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(10)
                ),

                child: TextField(

                  decoration: new InputDecoration(
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    hintText: '01-01-1981',
                  ),
                ),
              ),

              SizedBox(
                height: 25.0,
              ),
              Text("Expiry Date"),
              SizedBox(
                height: 10.0,
              ),
              Container(
                decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(10)
                ),

                child: TextField(

                  decoration: new InputDecoration(
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    hintText: '11-29-2019',
                  ),
                ),
              ),

              SizedBox(
                height: 25.0,
              ),
              Text("Issue Date"),
              SizedBox(
                height: 10.0,
              ),
              Container(
                decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(10)
                ),

                child: TextField(

                  decoration: new InputDecoration(
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    hintText: '11-30-2009',
                  ),
                ),
              ),
              SizedBox(
                height: 30.0,
              ),
              GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => AddNewLicense()));
                },
                child: Container(
                  decoration: BoxDecoration(
                    color: Color(0xff00587A),
                    borderRadius: BorderRadius.circular(15)
                  ),
                  height: 50.0,
                  width: MediaQuery.of(context).size.width * 0.4,
                  child: Center(child: Text("Verify",
                    textScaleFactor: 0.9,
                    style: TextStyle(color: Colors.white),),),
                ),
              ),

              SizedBox(
                height: 30.0,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
