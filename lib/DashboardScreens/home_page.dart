import 'dart:async';

import 'package:flutter/material.dart';
import 'package:snap_fill/Connect-to-Linkdedn/connect-likedin.dart';
import 'package:snap_fill/DashboardScreens/License-ids-page.dart';
import 'package:snap_fill/DashboardScreens/Scan_License.dart';
import 'package:snap_fill/forms_pages/unemployment_insurance_form.dart';
import 'package:snap_fill/pay_stub_forms/pay_stub_unemployment_form.dart';
import 'package:snap_fill/pay_stub_forms/pay_stubs_list.dart';


class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex = 0;

  List dummyDataSlider = [
    {"id": 0, "names": "LICENSE & IDS", "image": "images/home-image-1.png"},
    {"id": 1, "names": "Passport", "image": "images/home-image-2.png"},
    {"id": 2, "names": "Certificates", "image": "images/home-image-3.png"},
    {"id": 3, "names": "Degrees", "image": "images/home-image-4.png"},
    {"id": 4, "names": "Resume", "image": "images/home-image-6.png"},
    {"id": 5, "names": "Pay Stub", "image": "images/home-image-7.png"},
    {"id": 6, "names": "Utility Bills", "image": "images/home-image-8.png"},
    {"id": 7, "names": "W2 Form", "image": "images/home-image-9.png"},
    {"id": 8, "names": "Connect Likedin", "image": "images/home-image-10.png"},
    {"id": 9, "names": "Others", "image": "images/home-image-11.png"},
  ];
  _onSelected(int index) {
    setState(() => _selectedIndex = index);
  }
  @override
  void initState() {
//    new Timer(new Duration(milliseconds: 2000), () {
//      alertDialog();
//    });

    super.initState();
  }
  void alertDialog(){
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius:
                BorderRadius.circular(20.0)), //this right here
            child: Padding(
              padding: const EdgeInsets.all(12.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text("Privacy Policy",
                  textScaleFactor: 1.1,
                    style: TextStyle(
                      color: Color(0xff00587A)
                    ),
                  ),
                  SizedBox(height: 15.0,),
                  Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Text("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
                      textScaleFactor: 0.9,
                      style: TextStyle(
                          color: Color(0xff00587A)
                      ),
                    ),
                  ),
                  SizedBox(height: 15.0,),
                  GestureDetector(
                    onTap: (){
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      padding: EdgeInsets.only(top:10.0,bottom:10.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                          color: Color(0xff00587A)
                      ),
             //           height: 35.0,
                      width: 150.0,
                      child: Center(
                        child: Text("Accept",style: TextStyle(color: Colors.white),),
                      ),
                    ),
                  ),
                  SizedBox(height: 15.0,),
                ],
              ),
            ),
          );
        });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.white,
        title: Padding(
          padding: const EdgeInsets.only(left: 15),
          child: Text("Snapfill",style: TextStyle(color: Color(0xff00587A)),),
        ),
        elevation: 0.0,
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [

            SizedBox(height: 25.0,),

            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25.0),
              child: Text("My Documents",
              style: TextStyle(color: Color(0xff00587A),fontSize: 18),
              ),
            ),
            SizedBox(height: 20.0,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15.0),
              child: GridView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: 10,
                  gridDelegate:
                  new SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    childAspectRatio: 1.2,
                    mainAxisSpacing: 10.0,
                    crossAxisSpacing: 5.0,
                  ),
                  itemBuilder:
                      (BuildContext context, int index) {
                    return GestureDetector(
                      onTap: () {
                        _onSelected(index);
                        if(dummyDataSlider[index]['names'] == "LICENSE & IDS")
                         {
                           Navigator.push(
                               context,
                               MaterialPageRoute(
                                   builder: (context) =>
                                       ScanLicenses()));
                         }
                        else if(dummyDataSlider[index]['names'] == "Pay Stub")
                        {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      PayStubsList()));

//                          Navigator.push(
//                              context,
//                              MaterialPageRoute(
//                                  builder: (context) =>
//                                      PayStubUnEmployementInsuranceForm(0,{})));
                        }
                        else if(dummyDataSlider[index]['names'] == "Connect Likedin")
                        {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      ConnectToLikedIn()));
                        }

                        else{
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      LicensePageScreen()));
                        }


                      },
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
//                                  width: 40.0,
//                         height: 100.0,
                          decoration: BoxDecoration(
                            // border: Border.all(width: 3,color: Colors.green,style: BorderStyle.solid)
                            // border: Border.all(
                            //   color: _selectedIndex != null &&
                            //       _selectedIndex == index
                            //       ? Color(0xff00587A)
                            //       : Colors.transparent,
                            // ),
                            boxShadow: [
                              BoxShadow(
                                color:Colors.grey[500].withOpacity(0.2),
                                spreadRadius: 2,
                                blurRadius: 5,
//                                        offset: Offset(
//                                            1, 5), // changes position of shadow
                              ),
                            ],
                            color: _selectedIndex != null &&
                                _selectedIndex == index
                                ? Colors.white
                                : Colors.white,
                            borderRadius: BorderRadius.circular(10
                            ),
                          ),
//                                              height: 40.0,
//                                              width: 50.0,
                          child: Center(
                              child: Column(
                                mainAxisAlignment:
                                MainAxisAlignment.center,
                                crossAxisAlignment:
                                CrossAxisAlignment.center,
                                children: <Widget>[
                                  Container(
                                    // color:Colors.red,
                                    height: 70.0,
                                    width: 70.0,
                                    child: Image(
                                      image: AssetImage(
                                        dummyDataSlider[index]['image'],
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: 15.0,),
                                  Text(dummyDataSlider[index]['names'])

                                  // Flexible(
                                  //   flex: 1,
                                  //   child: Padding(
                                  //       padding:
                                  //       const EdgeInsets.all(
                                  //           8.0),
                                  //       child: Column(
                                  //         crossAxisAlignment:
                                  //         CrossAxisAlignment
                                  //             .start,
                                  //         children: [
                                  //           Text(
                                  //             amazonData[index]
                                  //             ['asin_name'],
                                  //             textScaleFactor:
                                  //             0.8,
                                  //             overflow:
                                  //             TextOverflow
                                  //                 .ellipsis,
                                  //             maxLines: 3,
                                  //             style: TextStyle(
                                  //                 color: _selectedIndex !=
                                  //                     null &&
                                  //                     _selectedIndex ==
                                  //                         index
                                  //                     ? Color(
                                  //                     0xfff99000)
                                  //                     : Colors
                                  //                     .grey),
                                  //           ),
                                  //           SizedBox(
                                  //             height: 5.0,
                                  //           ),
                                  //           Text(
                                  //             "Price: " +
                                  //                 amazonData[index]
                                  //                 [
                                  //                 'asin_price']
                                  //                     .toString(),
                                  //             textScaleFactor:
                                  //             0.8,
                                  //             overflow:
                                  //             TextOverflow
                                  //                 .ellipsis,
                                  //             maxLines: 3,
                                  //             style: TextStyle(
                                  //                 color: _selectedIndex !=
                                  //                     null &&
                                  //                     _selectedIndex ==
                                  //                         index
                                  //                     ? Colors
                                  //                     .orange
                                  //                     : Colors
                                  //                     .grey),
                                  //           ),
                                  //           SizedBox(
                                  //             height: 5.0,
                                  //           ),
                                  //           Text(
                                  //             "Brand Name: " +
                                  //                 amazonData[index]
                                  //                 [
                                  //                 'brand_name']
                                  //                     .toString(),
                                  //             textScaleFactor:
                                  //             0.8,
                                  //             overflow:
                                  //             TextOverflow
                                  //                 .ellipsis,
                                  //             maxLines: 3,
                                  //             style: TextStyle(
                                  //                 color: _selectedIndex !=
                                  //                     null &&
                                  //                     _selectedIndex ==
                                  //                         index
                                  //                     ? Colors
                                  //                     .orange
                                  //                     : Colors
                                  //                     .grey),
                                  //           ),
                                  //           SizedBox(
                                  //             height: 5.0,
                                  //           ),
                                  //           Text(
                                  //             "Total Reviews: " +
                                  //                 amazonData[index]
                                  //                 [
                                  //                 'total_review']
                                  //                     .toString(),
                                  //             textScaleFactor:
                                  //             0.8,
                                  //             overflow:
                                  //             TextOverflow
                                  //                 .ellipsis,
                                  //             maxLines: 3,
                                  //             style: TextStyle(
                                  //                 color: _selectedIndex !=
                                  //                     null &&
                                  //                     _selectedIndex ==
                                  //                         index
                                  //                     ? Color(
                                  //                     0xfff99000)
                                  //                     : Colors
                                  //                     .grey),
                                  //           ),
                                  //           SizedBox(
                                  //             height: 5.0,
                                  //           ),
                                  //           Text(
                                  //             "Rating: " +
                                  //                 amazonData[index]
                                  //                 [
                                  //                 'rating']
                                  //                     .toString(),
                                  //             textScaleFactor:
                                  //             0.8,
                                  //             overflow:
                                  //             TextOverflow
                                  //                 .ellipsis,
                                  //             maxLines: 3,
                                  //             style: TextStyle(
                                  //                 color: _selectedIndex !=
                                  //                     null &&
                                  //                     _selectedIndex ==
                                  //                         index
                                  //                     ? Color(
                                  //                     0xfff99000)
                                  //                     : Colors
                                  //                     .grey),
                                  //           ),
                                  //         ],
                                  //       )),
                                  // ),
                                ],
                              )),
                        ),
                      ),
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
