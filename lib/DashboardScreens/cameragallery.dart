import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:snap_fill/DashboardScreens/verify-document.dart';
import 'package:snap_fill/datamodel/appalertdialog.dart';
import 'package:snap_fill/datamodel/sharedpreferenceapp.dart';

import 'camera_screen.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:math';
import 'package:async/async.dart';
import 'package:path/path.dart' as bt;
import 'package:crypto/crypto.dart';

class CameraGallery extends StatefulWidget {
  @override
  _CameraGalleryState createState() => _CameraGalleryState();
}

class _CameraGalleryState extends State<CameraGallery> {

  File documentImage;
  final picker = ImagePicker();
   PickedFile pickedFile;
   int randomNounce;
   int currentNounceBase;

   SharedPreferenceApp shPrefApp = SharedPreferenceApp();
   String userId;
   Random rand = Random();
   int randomNumber;
   String documentAbout;
   String emailHash;
   String jsonData = "";
   DateTime dateTime;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        leading: GestureDetector(
          onTap: (){
            Navigator.pop(context);
          },
            child: Icon(Icons.arrow_back_ios,color: Colors.black,size: 15,)
        ),
        title: Text("Scan Licenses & IDs",style: TextStyle(color: Color(0xff00587A)),),
      ),

      body: Column(children: [
        Container(

          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Center(
              child: Column(

                children: [
                  SizedBox(
                    height: 15,
                  ),

                  SizedBox(height: 30.0,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      GestureDetector(
                        onTap: (){
                         // Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => CameraScreen()));

                          GetCameraImage();

                        },
                        child: Column(
                          children: [
                            Image(image: AssetImage('images/camera.png'),width: 70.0,height: 70.0,),
                            SizedBox(
                              height: 15,
                            ),
                            Text("Camera")
                          ],
                        ),
                      ),
                      GestureDetector(
                        onTap: (){
                          GetGalleryImage();
                        },
                        child: Column(
                          children: [
                            Image(image: AssetImage('images/galerry.png'),width: 70.0,
                              height: 70.0,),
                            SizedBox(
                              height: 15,
                            ),
                            Text("Select document")
                          ],
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
        documentImage == null ?
        Spacer()
        :
            Expanded(
         child:Image.file(documentImage),
            ),

        SizedBox(height:20.0),

        GestureDetector(
          onTap: (){
           // alertDialog();
            Continue();
          },
          child: Container(
            decoration: BoxDecoration(
                color: Color(0xff00587A),
                borderRadius: BorderRadius.circular(15)
            ),
            height: 50.0,
            width: MediaQuery.of(context).size.width *0.8,
            child: Center(
              child:  Text("Continue",style: TextStyle(color: Colors.white),),
            ),
          ),

        ),

        SizedBox(
          height: 10.0,
        )

      ],),

    );
  }

  void GetCameraImage() async
  {
    print("get camera image");
    pickedFile = await picker.getImage(source: ImageSource.camera);

      if (pickedFile != null) {
        documentImage = File(pickedFile.path);
        await CropImage(documentImage);
      } else {
        print('No image selected.');
      }
    setState(() {

    });
      print("Image:"+documentImage.toString());
  }

  void GetGalleryImage() async
  {
    pickedFile = await picker.getImage(source: ImageSource.gallery);

    if (pickedFile != null) {
      documentImage = File(pickedFile.path);

      await CropImage(documentImage);

    } else {
      print('No image selected.');
    }
    setState(() {

    });
    print("Image:"+documentImage.toString());
  }


  void CropImage(File documentImages) async
  {

    File croppedFile = await ImageCropper.cropImage(
        sourcePath: documentImages.path,
        aspectRatioPresets: [
          CropAspectRatioPreset.square,
          CropAspectRatioPreset.ratio3x2,
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio4x3,
          CropAspectRatioPreset.ratio16x9
        ],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Snapfill Crop Image',
            toolbarColor: Color.fromARGB(0XFF, 0X00, 0X58, 0X7A).withOpacity(0.7),
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          minimumAspectRatio: 1.0,
        )
    );

    if(croppedFile !=null)
      {
        documentImage = croppedFile;
      }




  }


  void Continue() async
  {

    CircularIndicator();

    randomNounce = await GetNounce();
    print("Random nounce:"+randomNounce.toString());
    userId = await shPrefApp.GetUserIdSignUp();
    print("user id:"+userId.toString());
    emailHash = await GenerateMd5();
    print("Send to Server");
    print("Email hash:"+emailHash.toString());
    print("Image is:"+documentImage.toString());
    dateTime = DateTime.now();

    documentAbout = "Licenses_"+emailHash+dateTime.toString();

    //To save current about of category for unemployement form
    await shPrefApp.SetCategoryAbout(documentAbout);


    jsonData = '{"drivers_license_form":{"First Name":"","Last Name":"","Nationality":"","Sex":"","Date of Birth":"","Issue Date":"","Expiry Date":""}}';

   // jsonData = '{"\\n	drivers_license_form" : {\n		"First Name" : "",\n		"Last Name" : "",\n		"Nationality" : "",\n		"Sex" : "",\n		"Date of Birth" : "",\n		"Issue Date" : "",\n		"Expiry Date" : ""\n	}\n}';

    //json1 = jsonEncode(jsonData.toString());

    print("'$jsonData'");


    var stream =  ByteStream(DelegatingStream.typed(documentImage.openRead()));
    var length = await documentImage.length();
    var uri =Uri.parse("https://pscore-sandbox-1.herokuapp.com/utils/upload/A/$randomNounce/$userId?app=snapfill");

    print("Uri is:"+uri.toString());
    var request = MultipartRequest("POST",uri);

    var multipartFile = MultipartFile("license.jpg",stream,length,filename: bt.basename(documentImage.path));
    //request.headers.addAll({"Content-Type":"application/x-www-form-urlencoded"});

    request.fields['location'] = 'documents';
    request.fields['category'] = 'licenses_and_IDs';
    request.fields['about'] = documentAbout;
    request.fields['upload_json'] = '$jsonData';
    request.fields['useEasyOCR'] = "true";
    request.files.add(multipartFile);

    var response = await request.send();

    print("Response:"+response.statusCode.toString());
//print("Response:"+response.);

    if(response.statusCode == 200)
    {
      Navigator.of(context, rootNavigator: true).pop();
      //ShowErrorDialog(jsonDecode(response.body)["message"]);
     // Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => VerifyDocument("documents","licenses_and_IDs", documentAbout)));

      Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => CameraScreen("documents","licenses_and_IDs",documentAbout,documentImage)));
    }

    else if(response.statusCode == 204)
      {
        Navigator.of(context, rootNavigator: true).pop();
        ShowErrorDialog("You Already Scanned Document");
      }

    else{
      Navigator.of(context, rootNavigator: true).pop();
      ShowErrorDialog("Error in Uploading File");
    }



  }


  Future<int> GetNounce() async
  {
//    userId = await shPrefApp.GetUserIdSignUp();
//
//    print("user id:"+userId);

    currentNounceBase =  await shPrefApp.GetUserNonceBaseSignUp();
    print("Cached nounce:"+currentNounceBase.toString());
    //print("Random rand"+rand.nextInt(1000).toString());

    randomNumber = rand.nextInt(99);
    print("Random Number:"+randomNumber.toString());
    return currentNounceBase * randomNumber;

  }



  Future<String> GenerateMd5() async {

    String userName = await shPrefApp.GetUserName();
   // print("MD:"+md5.convert(utf8.encode(userName)).toString());
    return md5.convert(utf8.encode(userName)).toString();
  }





  void CircularIndicator()
  {
    AppAlertDialog.ShowDialog(context,"Uploading document");

  }
  void ShowErrorDialog(String ErrorMessage)
  {

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: (){

            },
            child: Dialog(
              insetPadding:EdgeInsets.symmetric(horizontal: 25.0),
              shape: RoundedRectangleBorder(
                  borderRadius:
                  BorderRadius.circular(20.0)
              ), //this right here
              child:  Column(
                mainAxisSize: MainAxisSize.min,

                children: [
                  SizedBox(height: MediaQuery.of(context).size.height*0.015,),
                  Image(image: AssetImage("images/errorimage.png"),width: 50.0,height: 50.0,),
                  SizedBox(height: MediaQuery.of(context).size.height*0.02,),
                  Container(
                    margin: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.05,right: MediaQuery.of(context).size.width*0.05),
                    child:Text(ErrorMessage,style: TextStyle(color: Color.fromARGB(0XFF, 0X3E, 0X40, 0X46)),textScaleFactor: 1.2,textAlign: TextAlign.center,),
                  ),

                  SizedBox(height: MediaQuery.of(context).size.height*0.02,),

                  GestureDetector(

                    onTap: (){
                      Navigator.pop(context);
                    },

                    child:Container(
                      //width: MediaQuery.of(context).size.width*0.5,
                      //height: MediaQuery.of(context).size.height*0.05,
                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.05,right: MediaQuery.of(context).size.width*0.05,top: MediaQuery.of(context).size.height*0.015,bottom: MediaQuery.of(context).size.height*0.015),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50.0),
                        color: Color(0xff00587A).withOpacity(0.8),
                      ),
                      child: Text("Okay, Acknowledge",style: TextStyle(color:Color.fromARGB(0XFF, 0XFF, 0XFF, 0XFF),fontWeight: FontWeight.bold),textScaleFactor: 0.9,),
                    ),
                  ),

                  SizedBox(height: MediaQuery.of(context).size.height*0.025,),


                ],
              ),

            ),
          );
        });

  }





}
