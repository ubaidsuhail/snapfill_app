import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:snap_fill/DashboardScreens/edit-document.dart';


class LicensePageScreen extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<LicensePageScreen> {
  int _selectedIndex = 0;

  List dummyDataSlider = [
    {"id": 0, "names": "LICENSE & IDS", "image": "images/home-image-1.png"},
    {"id": 1, "names": "Passport", "image": "images/home-image-2.png"},
    {"id": 2, "names": "Certificates", "image": "images/home-image-3.png"},
    {"id": 3, "names": "Degrees", "image": "images/home-image-4.png"},
    {"id": 4, "names": "Resume", "image": "images/home-image-6.png"},
    {"id": 5, "names": "Pay Stub", "image": "images/home-image-7.png"},
    {"id": 6, "names": "Utility Bills", "image": "images/home-image-8.png"},
    {"id": 7, "names": "W2 Form", "image": "images/home-image-9.png"},
    {"id": 8, "names": "Connect Likedin", "image": "images/home-image-10.png"},
    {"id": 9, "names": "Others", "image": "images/home-image-11.png"},
  ];
  _onSelected(int index) {
    setState(() => _selectedIndex = index);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Icon(Icons.arrow_back_ios,color: Colors.black,),
        backgroundColor: Colors.white,
        title: Text("License & IDs",style: TextStyle(color: Color(0xff00587A)),),
        elevation: 0.0,
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [


            Padding(

              padding: const EdgeInsets.symmetric(horizontal: 25.0),
              child: Container(
                height: 70.0,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [

                      BoxShadow(
                          color: Colors.blueGrey.withOpacity(0.1),
                          spreadRadius: 5,
                          blurRadius: 5
                      )
                    ]
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 15),
                  child: Row(
                    // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Image(image: AssetImage("images/license.png"),
                        width: 50.0,
                        height: 50.0,
                      ),
                      SizedBox(width: 10.0,),
                      Text("Add new license or IDs ",
                        style: TextStyle(color: Color(0xff00587A),),
                        textScaleFactor: 1.1,
                      )
                    ],),
                ),

              ),
            ),
            SizedBox(height: 20.0,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25.0),
              child: Text("My Licenses and IDs",
                style: TextStyle(color: Color(0xff00587A),fontSize: 15),
              ),
            ),
            SizedBox(height: 20.0,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Card(
                child: ListTile(
                  leading: Image(image: AssetImage("images/id.png"),
                    width: 50.0,
                    height: 50.0,
                  ),
                  title: Text("Drivers license",
                    style: TextStyle(color: Color(0xff00587A),fontSize: 15),
                  ),
                  trailing: Container(
                    width: 50.0,
                    child: Row(children: [
                      GestureDetector(
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => EditDocument()));
                        },
                        child: Image(image: AssetImage("images/edit.png"),
                          width: 20.0,
                          height: 20.0,
                        ),
                      ),
                      SizedBox(width: 5.0,),
                      Image(image: AssetImage("images/trash.png"),
                        width: 20.0,
                        height: 20.0,
                      ),
                    ],),
                  ),
                ),
              ),
            ),
            SizedBox(height: 20.0,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Card(
                child: ListTile(
                  leading: Image(image: AssetImage("images/id.png"),
                    width: 50.0,
                    height: 50.0,
                  ),
                  title: Text("Work Permit",
                    style: TextStyle(color: Color(0xff00587A),fontSize: 15),
                  ),
                  trailing: Container(
                    width: 50.0,
                    child: Row(children: [
                      GestureDetector(
                        onTap: (){
                          Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => EditDocument()));
                        },
                        child: Image(image: AssetImage("images/edit.png"),
                          width: 20.0,
                          height: 20.0,
                        ),
                      ),
                      SizedBox(width: 5.0,),
                      Image(image: AssetImage("images/trash.png"),
                        width: 20.0,
                        height: 20.0,
                      ),
                    ],),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
