import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/services.dart';
import 'package:snap_fill/DashboardScreens/License-ids-page.dart';
import 'package:snap_fill/DashboardScreens/home_page.dart';
import 'package:snap_fill/forms_pages/form_screen_one.dart';
import 'package:snap_fill/settings/setting.dart';

import 'camera_screen.dart';
import 'first_page.dart';

class Dashboard extends StatefulWidget {

  int bottomBarIndex;

  Dashboard(this.bottomBarIndex);


  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard>
    with TickerProviderStateMixin {
  String home = '', wishList = '', support = '', more = '';
  bool isEnglishLanguage;
  TabController tabController;
  List getSearch = [];
  List getSetting = [];
  var Data;
  String appName = "";
  @override
  void initState() {
    if(widget.bottomBarIndex == 0)
      {
        tabController = TabController(initialIndex: 0, length: 3, vsync: this);
      }
   else
     {
      tabController = TabController(initialIndex: 1, length: 3, vsync: this);
    } // alertDialog();
    super.initState();
  }







  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: (){
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
            FirstTimeScreen(1)), (Route<dynamic> route) => false);
      },
      child: Scaffold(
        backgroundColor: Color.fromRGBO(222, 222, 222, 1),


        body: TabBarView(
          controller: tabController,
          physics: NeverScrollableScrollPhysics(),
          children: [
            FormPageOne(),
            HomePage(),
            Setting(),
          ],
        ),
        bottomNavigationBar: CurvedNavigationBar(
          height: 60.0,
          backgroundColor: Colors.white70,
          color: Colors.white,
          buttonBackgroundColor: Color(0xff00587A),
          index: widget.bottomBarIndex == 0 ? 0 : 1,
          items: <Widget>[
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: Image(
                image: AssetImage('images/file-text.png',),
                color: tabController.index == 0 ? Colors.white :Colors.grey,
                width: 20.0,
                height: 20.0,),
            ),
             Padding(
               padding: const EdgeInsets.all(8.0),
               child: Image(
                image: AssetImage('images/home-icon.png'),
                 color: tabController.index == 1 ? Colors.white :Colors.grey,
                width: 25.0,
                height: 25.0,),
             ),
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: Image(
                image: AssetImage('images/settings.png'),
                color: tabController.index == 2 ? Colors.white :Colors.grey,
                width: 20.0,
                height: 20.0,),
            ),
          ],
          onTap: (index) {
            setState(() {
//            _page = index;
            tabController.index = index;

            });
          },
        ),
//      drawer: Drawer(
//        // Add a ListView to the drawer. This ensures the user can scroll
//        // through the options in the drawer if there isn't enough vertical
//        // space to fit everything.
//
//        child: Container(
//          color: Color(0xffbb00d0).withOpacity(0.105),
//          child: Column(
////          crossAxisAlignment: CrossAxisAlignment.start,
//            mainAxisAlignment: MainAxisAlignment.spaceBetween,
//            children: <Widget>[
//              Column(
//                crossAxisAlignment: CrossAxisAlignment.start,
//                children: <Widget>[
//                  DrawerHeader(
//                      child: Column(
//                    crossAxisAlignment: CrossAxisAlignment.start,
//                    mainAxisAlignment: MainAxisAlignment.end,
//                    children: <Widget>[
//                      Container(
//                        child: Center(
//                          child: Text(
//                            'T',
//                            style: TextStyle(
//                                color: Colors.white,
//                                fontSize: 25.0,
//                                fontWeight: FontWeight.bold),
//                          ),
//                        ),
//                        height: 70.0,
//                        width: 70.0,
//                        decoration: BoxDecoration(
//                            color: Colors.indigo, shape: BoxShape.circle),
//                      ),
//                      SizedBox(
//                        height: 10.0,
//                      ),
//                      Text(
//                        name,
//                        style: TextStyle(
//                            fontWeight: FontWeight.bold, fontSize: 15.0),
//                      ),
//                      SizedBox(
//                        height: 10.0,
//                      ),
//                      Text(email),
//                    ],
//                  )),
//                  GestureDetector(
//                    child: ListTile(
//                      leading: Image(
//                        image: AssetImage('assets/images/profile.png'),
//                        width: 30.0,
//                        height: 30.0,
//                      ),
//                      title: Text('Profile'),
//                      onTap: () {
//                        Navigator.push(
//                          context,
//                          MaterialPageRoute(builder: (context) => Coming()),
//                        );
//                      },
//                    ),
//                  ),
//                  ListTile(
//                    leading: Image(
//                      image: AssetImage('assets/images/history.png'),
//                      width: 30.0,
//                      height: 30.0,
//                    ),
//                    title: Text('History'),
//                    onTap: () {
//                      Navigator.push(
//                        context,
//                        MaterialPageRoute(builder: (context) => Coming()),
//                      );
//                    },
//                  ),
//                  ListTile(
//                    leading: Image(
//                      image: AssetImage('assets/images/phone.png'),
//                      width: 30.0,
//                      height: 30.0,
//                    ),
//                    title: Text('Appointments'),
//                    onTap: () {
//                      Navigator.push(
//                        context,
//                        MaterialPageRoute(builder: (context) => Coming()),
//                      );
//                    },
//                  ),
//                  GestureDetector(
//                    child: ListTile(
//                      leading: Image(
//                        image: AssetImage('assets/images/pay-card.png'),
//                        width: 30.0,
//                        height: 30.0,
//                      ),
//                      title: Text('Payment'),
//                      onTap: () {
////                        Navigator.push(
////                          context,
////                          MaterialPageRoute(
////                              builder: (context) => PaymentOptions()),
////                        );
//                      },
//                    ),
//                  ),
//                ],
//              ),
//              ListTile(
//                leading: Image(
//                  image: AssetImage('assets/images/logout.png'),
//                  width: 30.0,
//                  height: 30.0,
//                ),
//                title: Text('Logout'),
//                onTap: () {
//                  Navigator.push(
//                    context,
//                    MaterialPageRoute(builder: (context) => Coming()),
//                  );
//                },
//              )
//            ],
//          ),
//        ),
//      ),
      ),
    );
  }

//  Future getCategories() async{
//
//    String url = fetching_setting_api;
//    Response response = await get(url);
//    int statusCode = response.statusCode;
//    print(response.statusCode.toString());
//    print(response.body.toString());
////    print(placeBidNotifications.length);
//
////    String jobId = widget.my_job_id;
////    String json = '{"JobId":"$jobId"}';
////    print(json);
////    Response response = await post(url,body: json);
//    int statusCode2 = response.statusCode;
//
//    print('Status code 2: '+statusCode2.toString());
////    print(response.statusCode.toString());
//
//
//
//    if(response.statusCode == 200 && response.body.toString() == "No Notifications Available")
//    {
//      print('Response Status Code ');
//    }
//    else if(response.statusCode == 200)
//    {
//      getSearch = jsonDecode(response.body);
//
//    }
//  }

//  Future FetchSettings() async{
//
//    String url = fetching_setting_api;
//    Response response = await get(url);
//    int statusCode = response.statusCode;
//    print(response.statusCode.toString());
//    print(response.body.toString());
////    print('Status code 2: '+statusCode2.toString());
////    print(response.statusCode.toString());
//
//
//    if(response.statusCode == 200 && response.body.toString() == "No Notifications Available")
//    {
//      showDialog(
//          context: context,
//          builder: (context) {
//            return AlertDialog(
//              title: Text(
//                "Error",
//              ),
//              content: Text("No BIDS Available"),
//              actions: <Widget>[
//                FlatButton(
//                  child: Text("Ok"),
//                  onPressed: () {
//                    Navigator.pop(context);
//                  },
//                )
//              ],
//            );
//          });
//    }
//    else if(response.statusCode == 200)
//    {
//      getSetting = jsonDecode(response.body);
//      setState(() {
//        appName = getSetting[0]['app_name'];
//        print('Picture Path is :'+appName);
//      });
////      print("Settings is :"+getSetting.toString());
//    }
//    else
//    {
//      showDialog(
//          context: context,
//          builder: (context) {
//            return AlertDialog(
//              title: Text(
//                "Error",
//              ),
//              content: Text("Internet Connection"),
//              actions: <Widget>[
//                FlatButton(
//                  child: Text("Ok"),
//                  onPressed: () {
//                    Navigator.pop(context);
//                  },
//                )
//              ],
//            );
//          });
//    }
//
//  }

}
