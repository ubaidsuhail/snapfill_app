import 'dart:async';
import 'dart:io';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:snap_fill/DashboardScreens/verify-document.dart';
import 'package:snap_fill/constant.dart';
import 'package:snap_fill/datamodel/appalertdialog.dart';


class CameraScreen extends StatefulWidget {

  String docLocation,docCategory,docAbout;
  File documentImage;

  CameraScreen(this.docLocation,this.docCategory,this.docAbout,this.documentImage);

  @override
  _CameraScreenState createState() => _CameraScreenState();
}

class _CameraScreenState extends State<CameraScreen> {

String message = "Wait";

  @override
  void initState() {
    Timer(new Duration(milliseconds: 1000), () {
      CircularIndicator();
    });

    Timer(new Duration(milliseconds: 9000), () {
      Navigator.of(context, rootNavigator: true).pop();
      message = "Not Wait";
      setState(() {

      });
    });

    super.initState();
  }
  bool toggleValue = false;
  bool isClick = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body:message == "Wait"
      ?
          Container()
//       Center(
//           child:Container(
//            margin: EdgeInsets.only(left:MediaQuery.of(context).size.width*0.4,right:MediaQuery.of(context).size.width*0.4),
//             decoration: BoxDecoration(
//               color:Colors.white,
//               shape: BoxShape.circle,
//               boxShadow: [
//                 BoxShadow(
//                   color: Colors.grey[300].withOpacity(0.5),
//                   spreadRadius: 5,
//                   blurRadius: 7,
//                   offset: Offset(0, 3), // changes position of shadow
//                 ),
//               ],
//             ),
//             height: MediaQuery.of(context).size.height*0.07,
//             child: Center(child: CircularProgressIndicator(),),
//           )
//       )
      :

      Column(
        // crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [

          SizedBox(height: MediaQuery.of(context).size.height*0.1),

          Container(
            width: MediaQuery.of(context).size.width*0.9,
          height: MediaQuery.of(context).size.height*0.3,
          child: Image.file(widget.documentImage)
          ),

          Expanded(
            child: Container(

                // height: MediaQuery.of(context).size.height * 0.4,
                width:  MediaQuery.of(context).size.width,
//                decoration: BoxDecoration(
//                    color: Colors.white,
//                    borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20)),
//                    boxShadow: [
//                      BoxShadow(
//                          color: Colors.grey,
//                          offset: Offset(0, 0),
//                          blurRadius: 20,
//                          spreadRadius: 3)
//                    ]
//                ),
                child: Center(
                  child: Column(
                    children: [
                      SizedBox(height: 20.0,),
                      Text('Document Scanned Successfully',style: TextStyle(color: kAppMainColor),
                      textScaleFactor: 1.1,
                      ),
                      Image(image: AssetImage("images/form-sub.png"),width: 200,),
                      SizedBox(
                        height: 15.0,
                      ),
                      Center(
                        child: GestureDetector(
                          onTap: (){
                            print("Docabout is:"+widget.docAbout);
                            Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => VerifyDocument(widget.docLocation,widget.docCategory, widget.docAbout,1,{})));
                            //Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => VerifyDocument()));
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                color: Color(0xff00587A),
                                borderRadius: BorderRadius.circular(15)
                            ),
                            height: 50.0,
                            width: MediaQuery.of(context).size.width *0.8,
                            child: Center(
                              child:  Text("Continue",style: TextStyle(color: Colors.white),),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 15.0,
                      ),
                    ],
                  ),
                ),
              ),
          ),
          SizedBox(height: MediaQuery.of(context).size.height*0.1),

        ],
      ),
    );
  }
  toggleButton(){
    setState(() {
      toggleValue = !toggleValue;
    });
  }
void CircularIndicator()
{
  AppAlertDialog.ShowDialog(context,"Just a few seconds. scanning your document.");

}
}
