import 'package:flutter/material.dart';
import 'package:snap_fill/DashboardScreens/camera_screen.dart';




class ScanLicensePageTwo extends StatefulWidget {
  @override
  _ScanLicensePageTwoState createState() => _ScanLicensePageTwoState();
}

class _ScanLicensePageTwoState extends State<ScanLicensePageTwo> {
  void alertDialog(){
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius:
                BorderRadius.circular(20.0)), //this right here
            child:
            Container(
              height: 250,
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Center(
                  child: Column(

                    children: [
                      SizedBox(
                        height: 15,
                      ),
                      Text("Select an option to proceed",
                        textScaleFactor: 1.1,
                        style: TextStyle(
                            color: Color(0xff00587A)
                        ),
                      ),
                      SizedBox(height: 30.0,),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          GestureDetector(
                            onTap: (){
                              //Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => CameraScreen()));
                            },
                            child: Column(
                              children: [
                                Image(image: AssetImage('images/camera.png'),width: 70.0,height: 70.0,),
                                SizedBox(
                                  height: 15,
                                ),
                                Text("Camera")
                              ],
                            ),
                          ), Column(
                            children: [
                              Image(image: AssetImage('images/galerry.png'),width: 70.0,
                              height: 70.0,),
                              SizedBox(
                                height: 15,
                              ),
                              Text("Gallery")
                            ],
                          ),
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        leading: GestureDetector(
          onTap: ()
            {
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back_ios,color: Colors.black,size: 15,)
        ),
        title: Text("Scan Licenses & IDs",style: TextStyle(color: Color(0xff00587A)),),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 25),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: MediaQuery.of(context).size.height *0.03),
              Text("Document Name"),
              SizedBox(
                height: 10.0,
              ),
              Container(
                decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(10)
                ),

                child: TextField(

                  decoration: new InputDecoration(
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    hintText: 'Mobile Number',
                  ),
                ),
              ),
              SizedBox(
                height: 25.0,
              ),
              Text("Description"),
              SizedBox(
                height: 10.0,
              ),
              Container(
                height: 150,
                decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(5)
                ),

                child: TextField(

                  decoration: new InputDecoration(
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    hintText: 'My new ID card that reflects my change of name',
                    hintStyle: TextStyle(fontSize: 12)
                  ),
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height *0.35),
              Center(
                child: GestureDetector(
                  onTap: (){
                    alertDialog();
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        color: Color(0xff00587A),
                        borderRadius: BorderRadius.circular(15)
                    ),
                    height: 50.0,
                    width: MediaQuery.of(context).size.width *0.8,
                    child: Center(
                      child:  Text("Continue",style: TextStyle(color: Colors.white),),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 30.0,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
