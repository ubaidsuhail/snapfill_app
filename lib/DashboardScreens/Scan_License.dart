import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';
import 'package:snap_fill/DashboardScreens/Scan_Licence_2.dart';
import 'package:snap_fill/DashboardScreens/verify-document.dart';
import 'package:snap_fill/datamodel/apisurl.dart';
import 'package:snap_fill/datamodel/appalertdialog.dart';
import 'package:snap_fill/datamodel/sharedpreferenceapp.dart';

import 'cameragallery.dart';
import 'dashboard.dart';





class ScanLicenses extends StatefulWidget {
  @override
  _ScanLicensesState createState() => _ScanLicensesState();
}

class _ScanLicensesState extends State<ScanLicenses> {
  Map<String, dynamic> documentData = Map<String, dynamic>();
  Map<String, dynamic> userDetails = Map<String, dynamic>();

  SharedPreferenceApp shPrefApp = SharedPreferenceApp();

  String userId;
  Random rand = Random();
  int randomNumber;
  String documentAbout;

  int randomNounce;
  int currentNounceBase;
  String messageCorrect = "Wait";

  String emailHash;
  String jsonData = "";

  List<Map<String, dynamic>> userLicenseList = List<Map<String, dynamic>>();
  String data;

  List imageList = List();
  String image;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Timer(new Duration(milliseconds: 1200), () {
      GetUserLicenseData();
    });
//    GetUserLicenseData();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: Color.fromARGB(0XFF, 0X00, 0X58, 0X7A).withOpacity(1.0),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 50.0,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [

                Row(
                  children: [
                    GestureDetector(onTap:(){
                      Navigator.pop(context);
                    },
                        child: Icon(Icons.arrow_back_ios, size: 18,)),
                    SizedBox(width: 5.0,),
                    Text("Licenses & IDs",
                      textScaleFactor: 1.3,
                      style: TextStyle(color: Colors.black,),
                    ),
                  ],
                ),

              ],
            ),
          ),

          //SizedBox(height: 20.0,),
//          Padding(
//            padding: const EdgeInsets.symmetric(horizontal: 25.0),
//            child: Text("Scan Licenses & IDs",
//              style: TextStyle(color: Color(0xff00587A), fontSize: 18),
//            ),
//          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25.0, vertical: 10),
            child: Text(
              "Take a picture of your licenses and IDs to extract the information on it",
              style: TextStyle(color: Colors.black,),textScaleFactor: 1.0,
            ),
          ),
          SizedBox(height: 25.0,),
          Padding(

            padding: const EdgeInsets.symmetric(horizontal: 25.0),
            child: GestureDetector(
              onTap: () {
                Navigator.push(context, MaterialPageRoute(
                    builder: (BuildContext context) => CameraGallery()));
              },
              child: Container(
                height: 70.0,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [

                      BoxShadow(
                          color: Colors.blueGrey.withOpacity(0.1),
                          spreadRadius: 5,
                          blurRadius: 5
                      )
                    ]
                ),
                child: Padding(
                  padding: const EdgeInsets.only(left: 15),
                  child: Row(
                    // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Image(image: AssetImage("images/license.png"),
                        width: 50.0,
                        height: 50.0,
                      ),
                      SizedBox(width: 10.0,),
                      Text("Add new license or ID",
                        style: TextStyle(color: Colors.black,),
                        textScaleFactor: 1.1,
                      )
                    ],),
                ),

              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25.0, vertical: 15),
            child: Text("My Licenses & IDs",
              style: TextStyle(color: Color(0xff00587A), fontSize: 16),
            ),
          ),
//            Center(
//               child: Image(image: AssetImage("images/folder.png"),width: 60.0,height: 60.0,),
//             ),

          messageCorrect == "Wait" ?

              Container()
//          Center(
//              child:
//              Container(
//                margin: EdgeInsets.only(left: MediaQuery
//                    .of(context)
//                    .size
//                    .width * 0.4, right: MediaQuery
//                    .of(context)
//                    .size
//                    .width * 0.4),
//                decoration: BoxDecoration(
//                  color: Colors.white,
//                  shape: BoxShape.circle,
//                  boxShadow: [
//                    BoxShadow(
//                      color: Colors.grey[300].withOpacity(0.5),
//                      spreadRadius: 5,
//                      blurRadius: 7,
//                      offset: Offset(0, 3), // changes position of shadow
//                    ),
//                  ],
//                ),
//                height: MediaQuery
//                    .of(context)
//                    .size
//                    .height * 0.07,
//                child: Center(child: CircularProgressIndicator(),),
//              )
//          )
              :
          messageCorrect == "Not Wait" ?


          Expanded(
              child: ListView.builder(
                  itemCount: userLicenseList.length,
                  padding: EdgeInsets.zero,
                  itemBuilder: (BuildContext context, int index) {
                    return Card(
                      child: ListTile(
                        leading: Image.network(imageList[index],width: 60.0,height:60.0,),
                        title: Text("Drivers license",
                          style: TextStyle(
                              color: Color(0xff00587A), fontSize: 15),
                        ),
                        trailing: Container(
                          width: 50.0,
                          child: Row(children: [
                            GestureDetector(
                              onTap: () {
                                print(
                                    userLicenseList[index]["about"].toString());
                                Navigator.push(context, MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        VerifyDocument(
                                            "documents", "licenses_and_IDs",
                                            userLicenseList[index]["about"]
                                                .toString(), 0,
                                            userLicenseList[index])));
                              },
                              child: Image(image: AssetImage("images/edit.png"),
                                width: 20.0,
                                height: 20.0,
                              ),
                            ),
                            SizedBox(width: 10.0,),
                            GestureDetector(
                              onTap: () {
                                DeleteData(index,
                                    userLicenseList[index]["about"].toString());
                              },
                              child: Image(
                                image: AssetImage("images/trash.png"),
                                width: 20.0,
                                height: 20.0,
                              ),
                            ),
                          ],),
                        ),
                      ),
                    );
                  }

              )

          )

              :
          Expanded(
            child: Container(
              width:MediaQuery.of(context).size.width,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image(image: AssetImage("images/folder.png"),
                    width: 60.0,
                    height: 60.0,),
                  Text(messageCorrect),
                ],
              ),
            ),
          ),


          messageCorrect == "Not Wait" ?
          Container()
              :
          Spacer(),

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(15)
                  ),
                  height: 40.0,
                  width: MediaQuery
                      .of(context)
                      .size
                      .width * 0.35,
                  child: Center(
                    child: Text(
                      "Back", style: TextStyle(color: Color(0xff00587A)),),
                  ),
                ),
              ),
              GestureDetector(
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(
                      builder: (BuildContext context) => CameraGallery()));
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: Color(0xff00587A),
                      borderRadius: BorderRadius.circular(15)
                  ),
                  height: 40.0,
                  width: MediaQuery
                      .of(context)
                      .size
                      .width * 0.35,
                  child: Center(
                    child: Text("Next", style: TextStyle(color: Colors.white),),
                  ),
                ),
              ),

            ],
          ),
          SizedBox(height: 10.0,),
        ],
      ),

    );
  }


  void GetUserLicenseData() async
  {
    userLicenseList = [];
    CircularIndicator("Extracting user license list");
    try {
      randomNounce = await GetNounce();
      print("Random nounce:" + randomNounce.toString());
      userId = await shPrefApp.GetUserIdSignUp();

      print("user id:" + userId.toString());
      String url = "https://pscore-sandbox-2.herokuapp.com/utils/download/A/$randomNounce/$userId?app=snapfill";
      String json = '{"search_params" : { "location" : "documents", "category" : "licenses_and_IDs", "fetch_last" : "30"},"linksOnly" : "true","optimistic":"true","useParity" : "true"}';


      Response response = await post(url, headers: header, body: json);

      print("Response status code:" + response.statusCode.toString());
      print("Response body doc:" + response.body);


      if (response.statusCode == 200) {
//        Navigator.of(context, rootNavigator: true).pop();


        documentData = jsonDecode(response.body);

        print("document data:" + documentData.toString());

        print("Form Json:" +
            documentData["data"]["package"]["forms"][0].toString());

        print("Form length" +
            documentData["data"]["package"]["forms"].length.toString());

        //data = documentData["data"]["package"]["forms"][1].toString();
        //print("${documentData["data"]["package"]["images"][1]}");
        print("For decode data:"+data.toString());
        //documentData["data"]["package"]["forms"].length
        for (int i = 0; i < documentData["data"]["package"]["images"].length;
        i++) {
          data = documentData["data"]["package"]["forms"][i].toString();
          if (data[1] == '"') {
            userDetails = jsonDecode(documentData["data"]["package"]["forms"][i]);
            userLicenseList.add(userDetails);
            image = documentData["data"]["package"]["images"][i];
            //imageList.add(image);

          }
          else {
            userDetails = documentData["data"]["package"]["forms"][i];
            userLicenseList.add(userDetails);
//            image = base64Decode(documentData["data"]["package"]["images"][i]);
//            imageList.add(image);
            image = documentData["data"]["package"]["images"][i];
            imageList.add(image);
          }
        }

        print("First Names:" + userDetails["First Name"]);



        messageCorrect = "Not Wait";

        Navigator.of(context, rootNavigator: true).pop();

        setState(() {

        });
      }

      else if (response.statusCode == 400) {
        Navigator.of(context, rootNavigator: true).pop();
        //print("invalid data");
        await GetUserLicenseData();
      }

      else if (response.statusCode == 404) {
        Navigator.of(context, rootNavigator: true).pop();
        messageCorrect = "There are no licenses";
        setState(() {

        });
      }
      else {
        Navigator.of(context, rootNavigator: true).pop();
        messageCorrect = "Error coming shown license";
        setState(() {

        });
      }
    }
    catch (e) {
      Navigator.of(context, rootNavigator: true).pop();
      messageCorrect = "Error in shown license";
      setState(() {

      });


      print("Error:" + e.toString());
    }
  }

  Future<int> GetNounce() async
  {
    currentNounceBase = await shPrefApp.GetUserNonceBaseSignUp();
    print("Cached nounce:" + currentNounceBase.toString());
    //print("Random rand"+rand.nextInt(1000).toString());

    randomNumber = rand.nextInt(99);
    print("Random nounce:" + randomNumber.toString());
    return currentNounceBase * randomNumber;


    //floor();
  }

  Future<void> DeleteData(int indexDelete, String deleteAbout) async {
    try {
      CircularIndicator("Deleting document");
      randomNounce = await GetNounce();
      print("Random nounce:" + randomNounce.toString());
      userId = await shPrefApp.GetUserIdSignUp();

      String url = "https://pscore-sandbox-5.herokuapp.com/utils/delete/A/$randomNounce/$userId?app=snapfill";

      String json = '{"search_params" : { "location" : "documents", "category" : "licenses_and_IDs", "about" : "$deleteAbout"}}';
      Response response = await post(url, headers: header, body: json);

      print("Response status code:" + response.statusCode.toString());
      print("Current data Response body:" + response.body);

      if (response.statusCode == 200) {
        Navigator.of(context, rootNavigator: true).pop();


        userLicenseList.removeAt(indexDelete);
        imageList.removeAt(indexDelete);

        setState(() {
          userLicenseList;
        });

        ToastFunction("Delete Successfully");
      }

      else if (response.statusCode == 400) {
        Navigator.of(context, rootNavigator: true).pop();
        ShowErrorDialog(jsonDecode(response.body)["message"]);
      }


      else {
        Navigator.of(context, rootNavigator: true).pop();
        ShowErrorDialog("Your Internet is not Working Properly");
      }
    }
    catch (e) {
      Navigator.of(context, rootNavigator: true).pop();
      ShowErrorDialog("Check Your Internet Connection");
      print("Error:" + e.toString());
    }
  }


  void CircularIndicator(String message)
  {
    AppAlertDialog.ShowDialog(context,message);

  }

  void ShowErrorDialog(String ErrorMessage) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: () {

            },
            child: Dialog(
              insetPadding: EdgeInsets.symmetric(horizontal: 25.0),
              shape: RoundedRectangleBorder(
                  borderRadius:
                  BorderRadius.circular(20.0)
              ), //this right here
              child: Column(
                mainAxisSize: MainAxisSize.min,

                children: [
                  SizedBox(height: MediaQuery
                      .of(context)
                      .size
                      .height * 0.015,),
                  Image(image: AssetImage("images/errorimage.png"),
                    width: 50.0,
                    height: 50.0,),
                  SizedBox(height: MediaQuery
                      .of(context)
                      .size
                      .height * 0.02,),
                  Container(
                    margin: EdgeInsets.only(left: MediaQuery
                        .of(context)
                        .size
                        .width * 0.05, right: MediaQuery
                        .of(context)
                        .size
                        .width * 0.05),
                    child: Text(ErrorMessage, style: TextStyle(
                        color: Color.fromARGB(0XFF, 0X3E, 0X40, 0X46)),
                      textScaleFactor: 1.2,
                      textAlign: TextAlign.center,),
                  ),

                  SizedBox(height: MediaQuery
                      .of(context)
                      .size
                      .height * 0.02,),

                  GestureDetector(

                    onTap: () {
                      Navigator.pop(context);
                    },

                    child: Container(
                      //width: MediaQuery.of(context).size.width*0.5,
                      //height: MediaQuery.of(context).size.height*0.05,
                      padding: EdgeInsets.only(left: MediaQuery
                          .of(context)
                          .size
                          .width * 0.05, right: MediaQuery
                          .of(context)
                          .size
                          .width * 0.05, top: MediaQuery
                          .of(context)
                          .size
                          .height * 0.015, bottom: MediaQuery
                          .of(context)
                          .size
                          .height * 0.015),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50.0),
                        color: Color(0xff00587A).withOpacity(0.8),
                      ),
                      child: Text("Okay, Acknowledge", style: TextStyle(
                          color: Color.fromARGB(0XFF, 0XFF, 0XFF, 0XFF),
                          fontWeight: FontWeight.bold), textScaleFactor: 0.9,),
                    ),
                  ),

                  SizedBox(height: MediaQuery
                      .of(context)
                      .size
                      .height * 0.025,),


                ],
              ),

            ),
          );
        });
  }

  void ToastFunction(String message)
  {
    Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Color(0xff00587A).withOpacity(0.7),
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }

}