import 'package:flutter/material.dart';
import 'package:snap_fill/Audio_Assistant/audio_screen.dart';
import 'package:snap_fill/Chat_Screen/chatting-page.dart';
import 'package:snap_fill/Video_Assistant/video_screen.dart';

class AddNewLicense extends StatefulWidget {
  @override
  _AddNewLicenseState createState() => _AddNewLicenseState();
}

class _AddNewLicenseState extends State<AddNewLicense> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [

            SizedBox(
              height: 50.0,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25.0),
              child: Text("Add new license or ID",
                textScaleFactor: 1.2,
                style: TextStyle(color: Color(0xff00587A),),
              ),
            ),
            SizedBox(
              height: 150.0,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25.0),
              child: Text("How do you want to proceed ?",
                textScaleFactor: 1.3,
                style: TextStyle(color: Colors.black,fontSize: 15),
              ),
            ),
            SizedBox(
              height: 50.0,
            ),

            Center(
              child: GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => VideoAssistant()));
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Colors.blueGrey.withOpacity(0.15),
                        spreadRadius: 5,
                        blurRadius: 8
                      )
                    ],
                    borderRadius: BorderRadius.circular(10)
                  ),

                  height: 70.0,
                  width: MediaQuery.of(context).size.width * 0.8,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: Row(
                      children: [
                        Image(image: AssetImage('images/camera.png'),width: 40.0,height: 40.0,),
                        SizedBox(width: 25,),
                        Text("Continue with audio assistant")
                      ],
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 25.0,
            ),

            Center(
              child: GestureDetector(

                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => AuidoAssitant()));
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                            color: Colors.blueGrey.withOpacity(0.15),
                            spreadRadius: 5,
                            blurRadius: 8
                        )
                      ],
                      borderRadius: BorderRadius.circular(10)
                  ),

                  height: 70.0,
                  width: MediaQuery.of(context).size.width * 0.8,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: Row(
                      children: [
                        Image(image: AssetImage('images/chat-bot.png'),width: 35.0,height: 35.0,),
                        SizedBox(width: 25,),
                        Text("Continue with audio assistant")
                      ],
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 25.0,
            ),

            Center(
              child: GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => Chatting()));
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                            color: Colors.blueGrey.withOpacity(0.15),
                            spreadRadius: 5,
                            blurRadius: 8
                        )
                      ],
                      borderRadius: BorderRadius.circular(10),

                  ),

                  height: 70.0,
                  width: MediaQuery.of(context).size.width * 0.8,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: Row(
                      children: [
                        Image(image: AssetImage('images/audio.png'),width: 35.0,height: 35.0,),
                        SizedBox(width: 25,),
                        Text("Continue with audio assistant")
                      ],
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 15.0,
            ),

          ],
        ),
      ),
    );
  }
}
