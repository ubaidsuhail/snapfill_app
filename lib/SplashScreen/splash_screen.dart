
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:snap_fill/DashboardScreens/dashboard.dart';
import 'package:snap_fill/DashboardScreens/first_page.dart';
import 'package:snap_fill/SignInScreen/create_account.dart';
import 'package:snap_fill/datamodel/sharedpreferenceapp.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SplashScreenState();
  }
}

class SplashScreenState extends State<SplashScreen> {
  String name = '';
  String userName = "";
  SharedPreferenceApp shPrefApp = SharedPreferenceApp();
  @override
  void initState() {
    super.initState();
//    getusername();
//    print('Your Name is: ' + name);
//    new Timer(new Duration(milliseconds: 3000), () {
//      Navigator.of(context)
//          .push(new MaterialPageRoute(builder: (context) => CreateAccount()));
//    });
    GoToNextScreen();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
         //
          // color: Colors.white,
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            image: DecorationImage(image: AssetImage("images/splashbackground.png"),fit: BoxFit.fill),
          ),
          child: Center(
            child: Image(image: AssetImage("images/splashforeground.png")),
          ),
    ));
  }

  void GoToNextScreen() async
  {
    userName = await shPrefApp.GetUserName();
    //print('Your Name is: ' + name);
    new Timer(new Duration(milliseconds: 3000), () {
      if(userName == "" || userName == null) {
        Navigator.of(context)
            .push(new MaterialPageRoute(builder: (context) => CreateAccount()));
      }
      else
        {
          Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
              Dashboard(1)), (Route<dynamic> route) => false);
        }
        });
  }


}
