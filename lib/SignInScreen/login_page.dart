import 'dart:convert';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';
import 'package:snap_fill/DashboardScreens/first_page.dart';
import 'package:snap_fill/SignInScreen/forget_password.dart';
import 'package:snap_fill/SignInScreen/new_account.dart';
import 'package:snap_fill/datamodel/apisurl.dart';
import 'package:snap_fill/datamodel/appalertdialog.dart';
import 'package:snap_fill/datamodel/sharedpreferenceapp.dart';
import 'package:loading_gifs/loading_gifs.dart';



class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  final _loginForm = GlobalKey<FormState>();
  bool userPasswordVisible = false;

  TextEditingController _userName = TextEditingController();
  TextEditingController _userPassword = TextEditingController();
  Pattern emailPattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

  Map<String,dynamic> loginData = Map<String,dynamic>();
  Map<String,dynamic> currentData = Map<String,dynamic>();

  SharedPreferenceApp shPrefApp = SharedPreferenceApp();


//  void _toggle() {
//    setState(() {
//      _obscureText = !_obscureText;
//    });
//  }
  bool _obscureText = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,

      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: Icon(Icons.arrow_back),
        elevation: 0.0,
      ),

      body: SingleChildScrollView(
        child:Form(
          key: _loginForm,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SizedBox(
                height: 25,
              ),
              Center(
                child: Text(
                  'Snapfill',
                  textScaleFactor: 3.5,
                  style: TextStyle(color: Color(0xff00587A),fontWeight: FontWeight.w500),
                ),
              ),
              SizedBox(
                height: 25,
              ),
              Center(
                child: Text(
                  'Log In',
                  textScaleFactor: 1.5,
                  style: TextStyle(color: Colors.black,fontWeight: FontWeight.w500),
                ),
              ),

              SizedBox(
                height: 25,
              ),
              Center(
                child: Text(
                  'Enter your login details to  access your account',
                  textScaleFactor: 1.0,
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black,fontWeight: FontWeight.w100),
                ),
              ),
              SizedBox(height: 15.0,),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: TextFormField(
                  controller: _userName,
                  validator: (value) {
                    RegExp regex = new RegExp(emailPattern);
                    if (value.isEmpty) {
                      return 'Email can not be empty';
                    }
                    else if(!regex.hasMatch(value))
                    {
                      return 'Enter Valid Email Format';
                    }

                    return null;
                  },
                  decoration: new InputDecoration(
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    errorBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.red[300], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    focusedErrorBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.red[300], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    fillColor: Colors.grey[200],
                    filled: true,
                    border: InputBorder.none,
                    prefixIcon: Icon(Icons.email,color: Color(0xff00587A),),
                    hintText: 'Email',
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 15),
                child: TextFormField(
                  controller:_userPassword,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Password can not be empty';
                    }
                    return null;
                  },
                  decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    errorBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.red[300], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    focusedErrorBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.red[300], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),

                      hintText: 'Password',
                      fillColor: Colors.grey[200],
                      filled: true,
              //          hintStyle: TextStyle(fontSize: 12.0),
                      border: InputBorder.none,
                      prefixIcon: Icon(
                        Icons.lock_open,
                        color: Color(0xff00587A),
                      ),
                      suffixIcon:  IconButton(
                        icon: userPasswordVisible
                            ? Icon(Icons.visibility,
                            color: Color(0xff00587A))
                            : Icon(Icons.visibility_off,
                            color: Color(0xff00587A)),
                        onPressed: UserPasswordToggle,
                      ),
                  ),
                  obscureText: userPasswordVisible ? false : true,
                ),

              ),
              GestureDetector(
                onTap: (){
                  Navigator.of(context)
                      .pushReplacement(new MaterialPageRoute(builder: (context) => ForgetPassword()));
                },
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                    Text("Forget Password ?",
                    style: TextStyle(
                      color: Color(0xff00587A)
                    ),
                    )
                  ],),
                ),
              ),

              SizedBox(
                height: 10.0,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 15),
                child: GestureDetector(
                  onTap: (){


                    if(_loginForm.currentState.validate()) {

                      Login();

                    }
                    else{
                      print("invalid");
                    }
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        color: Color(0xff00587A),
                        borderRadius: BorderRadius.circular(15)
                    ),
                    height: 60.0,
                    // width: MediaQuery.of(context).size.width * 0.8,
                    child: Center(
                      child: Text("Login",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ),
              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Didn\'t have an account?"),
                  SizedBox(
                    width: 10.0,
                  ),
                  GestureDetector(
                      onTap: (){

                        Navigator.of(context)
                            .pushReplacement(new MaterialPageRoute(builder: (context) => NewAccount()));
                      },
                      child: Text("Sign Up",style: TextStyle(color: Color(0xff00587A)),)),
                ],
              )
            ],
          ),
        ),
      )
    );
  }


  void UserPasswordToggle()
  {
    if(userPasswordVisible)
    {
      userPasswordVisible = false;
    }
    else
    {
      userPasswordVisible = true;
    }

    setState(() {
      userPasswordVisible;
    });
  }


  void Login() async
  {
    try {
      CircularIndicator();

      String url = loginApi;
      String json = '{"username":"${_userName.text}","password":"${_userPassword.text}"}';

      print("Json is:"+json);

      Response response = await post(url, headers: header, body: json);

      print("Response status code:"+response.statusCode.toString());
      print("Response body:"+response.body);

      if(response.statusCode == 200)
      {
       // Navigator.of(context, rootNavigator: true).pop();

        loginData = jsonDecode(response.body);

        print("User Name:"+loginData["data"]["profile"]["username"]);
        print("First Name:"+loginData["data"]["profile"]["userdata"]["first_name"]);
        print("Last Name:"+loginData["data"]["profile"]["userdata"]["last_name"]);
        print("Profile Name:"+loginData["data"]["profile"]["userdata"]["profile_name"]);
        print("Source:"+loginData["data"]["profile"]["userdata"]["signup_data"]["source"]);
        print("Role:"+loginData["data"]["profile"]["userdata"]["signup_data"]["role"]);
        //print("Nonce Base:"+loginData["data"]["profile"]["userdata"]["current_nonce_base"].toString());
        print("Password:"+loginData["data"]["profile"]["password"]);
        //print("User Id:"+loginData["data"]["profile"]["user_id"]);


        await shPrefApp.SetUserPersonalInformation(loginData["data"]["profile"]["username"], loginData["data"]["profile"]["userdata"]["first_name"], loginData["data"]["profile"]["userdata"]["last_name"], loginData["data"]["profile"]["userdata"]["profile_name"], loginData["data"]["profile"]["userdata"]["signup_data"]["source"], loginData["data"]["profile"]["userdata"]["signup_data"]["role"]);
        await shPrefApp.SetUserAppInformation(loginData["data"]["profile"]["userdata"]["current_nonce_base"], loginData["data"]["profile"]["password"], loginData["data"]["profile"]["user_id"]);

        GetCurrentData(loginData["data"]["profile"]["username"]);



      }

      else if(response.statusCode == 400)
      {
        Navigator.of(context, rootNavigator: true).pop();
        ShowErrorDialog(jsonDecode(response.body)["message"]);
      }


      else
      {
        Navigator.of(context, rootNavigator: true).pop();
        ShowErrorDialog("Your Internet is not Working Properly");
      }
    }
    catch(e) {
      Navigator.of(context, rootNavigator: true).pop();
      ShowErrorDialog("Check Your Internet Connection");
      print("Error:"+e.toString());
    }
  }



  void GetCurrentData(String username) async
  {
    try {
     // CircularIndicator();

      String url = "http://pscore-sandbox-3.herokuapp.com/utils/inspect/506623120463/$username/userdata&user_id?app=snapfill";


      //print("Json is:"+json);

      Response response = await get(url);

      print("Response status code:"+response.statusCode.toString());
      print("Current data Response body:"+response.body);

      if(response.statusCode == 200)
      {
        Navigator.of(context, rootNavigator: true).pop();

        currentData = jsonDecode(response.body);


        print("Nounce:"+currentData["data"]["userdata"]["current_nonce_base"].toString());
        print("USer Id:"+currentData["data"]["user_id"]);


        await shPrefApp.SetSignUpInformation(currentData["data"]["userdata"]["current_nonce_base"], currentData["data"]["user_id"],);

        //ToastFunction("Login Successfully");
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
            FirstTimeScreen(0)), (Route<dynamic> route) => false);

      }

      else if(response.statusCode == 400)
      {
        Navigator.of(context, rootNavigator: true).pop();
        ShowErrorDialog(jsonDecode(response.body)["message"]);
      }


      else
      {
        Navigator.of(context, rootNavigator: true).pop();
        ShowErrorDialog("Your Internet is not Working Properly");
      }
    }
    catch(e) {
      Navigator.of(context, rootNavigator: true).pop();
      ShowErrorDialog("Check Your Internet Connection");
      print("Error:"+e.toString());
    }
  }





  void CircularIndicator()
  {
    AppAlertDialog.ShowDialog(context,"Logging in");

  }

  void ShowErrorDialog(String ErrorMessage)
  {

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: (){

            },
            child: Dialog(
              insetPadding:EdgeInsets.symmetric(horizontal: 25.0),
              shape: RoundedRectangleBorder(
                  borderRadius:
                  BorderRadius.circular(20.0)
              ), //this right here
              child:  Column(
                mainAxisSize: MainAxisSize.min,

                children: [
                  SizedBox(height: MediaQuery.of(context).size.height*0.015,),
                  Image(image: AssetImage("images/errorimage.png"),width: 50.0,height: 50.0,),
                  SizedBox(height: MediaQuery.of(context).size.height*0.02,),
                  Container(
                    margin: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.05,right: MediaQuery.of(context).size.width*0.05),
                    child:Text(ErrorMessage,style: TextStyle(color: Color.fromARGB(0XFF, 0X3E, 0X40, 0X46)),textScaleFactor: 1.2,textAlign: TextAlign.center,),
                  ),

                  SizedBox(height: MediaQuery.of(context).size.height*0.02,),

                  GestureDetector(

                    onTap: (){
                      Navigator.pop(context);
                    },

                    child:Container(
                      //width: MediaQuery.of(context).size.width*0.5,
                      //height: MediaQuery.of(context).size.height*0.05,
                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.05,right: MediaQuery.of(context).size.width*0.05,top: MediaQuery.of(context).size.height*0.015,bottom: MediaQuery.of(context).size.height*0.015),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50.0),
                        color: Color(0xff00587A).withOpacity(0.8),
                      ),
                      child: Text("Okay, Acknowledge",style: TextStyle(color:Color.fromARGB(0XFF, 0XFF, 0XFF, 0XFF),fontWeight: FontWeight.bold),textScaleFactor: 0.9,),
                    ),
                  ),

                  SizedBox(height: MediaQuery.of(context).size.height*0.025,),


                ],
              ),

            ),
          );
        });

  }

  void ToastFunction(String message)
  {
    Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Color(0xff00587A).withOpacity(0.7),
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }

}
