import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';
import 'package:snap_fill/datamodel/apisurl.dart';
import 'package:snap_fill/datamodel/appalertdialog.dart';

import 'create_account.dart';

class NewPassword extends StatefulWidget {
  String oldPasswordKey;
  String userName;

  NewPassword(this.userName,this.oldPasswordKey);
  @override
  _NewPasswordState createState() => _NewPasswordState();
}

class _NewPasswordState extends State<NewPassword> {

  final _passwordForm = GlobalKey<FormState>();
  bool userPasswordVisible = false;
  bool userConfirmPasswordVisible = false;
  TextEditingController _userPassword = TextEditingController();
  TextEditingController _userConfirmPassword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Colors.white,
          leading: GestureDetector(
              onTap: (){
                Navigator.pop(context);
              },
              child: Icon(Icons.arrow_back,color: Colors.black,)
          ),
          elevation: 0.0,
        ),
        body:SingleChildScrollView(
          child: Form(
            key: _passwordForm,
            child: Column(
              children: [
                Center(
                  child: Text(
                    'Snapfill',
                    textScaleFactor: 3.5,
                    style: TextStyle(color: Color(0xff00587A),fontWeight: FontWeight.w500),
                  ),
                ),
                SizedBox(
                  height: 25,
                ),
                Center(
                  child: Text(
                    'Forget Password',
                    textScaleFactor: 1.5,
                    style: TextStyle(color: Colors.black,fontWeight: FontWeight.w500),
                  ),
                ),
                SizedBox(
                  height: 25,
                ),
                Center(
                  child: Text(
                    'Enter your new password',
                    textScaleFactor: 1.0,
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.black,fontWeight: FontWeight.w100),
                  ),
                ),
                SizedBox(height: 35.0,),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: TextFormField(
                    controller: _userPassword,

                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Password can not be empty';
                      }
                      return null;
                    },

                    decoration: new InputDecoration(
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),

                      errorBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.red[300], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      focusedErrorBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.red[300], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),

                      suffixIcon: IconButton(
                        icon: userPasswordVisible
                            ? Icon(Icons.visibility,
                            color: Color(0xff00587A))
                            : Icon(Icons.visibility_off,
                            color: Color(0xff00587A)),
                        onPressed: UserPasswordToggle,
                      ),
                      hintText: 'Password',
                      fillColor: Colors.grey[200],
                      filled: true,
                    ),
                    obscureText: userPasswordVisible ? false : true,
                  ),
                ),
                SizedBox(height: 15.0,),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: TextFormField(
                    controller: _userConfirmPassword,

                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Confirm Password can not be empty';
                      }
                      return null;
                    },

                    decoration: new InputDecoration(
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      enabledBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),

                      errorBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.red[300], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),
                      focusedErrorBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.red[300], width: 0.0),
                          borderRadius: BorderRadius.circular(10)
                      ),

                      suffixIcon: IconButton(
                        icon: userConfirmPasswordVisible
                            ? Icon(Icons.visibility,
                            color: Color(0xff00587A))
                            : Icon(Icons.visibility_off,
                            color: Color(0xff00587A)),
                        onPressed: UserConfirmPasswordToggle,
                      ),

                      hintText: 'Confirm Password',
                      fillColor: Colors.grey[200],
                      filled: true,
                    ),
                    obscureText: userConfirmPasswordVisible ? false : true,

                  ),
                ),

                SizedBox(height: MediaQuery.of(context).size.height * 0.1),

                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 15),
                  child: GestureDetector(
                    onTap: (){
                      if(_passwordForm.currentState.validate()) {

                        VerifyPassword();

                      }
                      else{
                        print("invalid");
                      }

                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: Color(0xff00587A),
                          borderRadius: BorderRadius.circular(15)
                      ),
                      height: 60.0,
                      // width: MediaQuery.of(context).size.width * 0.8,
                      child: Center(
                        child: Text("Verify",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        )
    );
  }

  void UserPasswordToggle()
  {
    if(userPasswordVisible)
    {
      userPasswordVisible = false;
    }
    else
    {
      userPasswordVisible = true;
    }

    setState(() {
      userPasswordVisible;
    });
  }


  void UserConfirmPasswordToggle()
  {
    if(userConfirmPasswordVisible)
    {
      userConfirmPasswordVisible = false;
    }
    else
    {
      userConfirmPasswordVisible = true;
    }

    setState(() {
      userConfirmPasswordVisible;
    });
  }


void VerifyPassword() async
{
  print("hello");
  if(_userPassword.text != _userConfirmPassword.text)
  {
    ShowErrorDialog("Password and Confirm Password do not match");
  }
  else
  {

    CircularIndicator();

    try {

      print("USer NAme email"+widget.userName);

      String url = changePasswordApi+widget.userName+"?app=snapfill";

      String json = '{"password":"${widget.oldPasswordKey}","new_password":"${_userPassword.text}"}';

      print("Json is:"+json);
      Map<String, String> header1 = {"Content-Type": "application/json","Authorization":"9edd804ee6005ffdfbcc87faa0a42170"};

      Response response = await put(url, headers: header1, body: json);

      print("Response status code:"+response.statusCode.toString());

      if(response.statusCode == 200)
      {
        Navigator.of(context, rootNavigator: true).pop();
        ToastFunction("Password Changed Successfully");
        //Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => ChangePasswordSuccessfully()));
        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => CreateAccount()));
      }

      else if(response.statusCode == 400)
      {
        Navigator.of(context, rootNavigator: true).pop();
        ShowErrorDialog(jsonDecode(response.body)["message"]);
      }


      else
      {
        Navigator.of(context, rootNavigator: true).pop();
        ShowErrorDialog("Your Internet is not Working Properly");
      }
    }
    catch(e) {
      Navigator.of(context, rootNavigator: true).pop();
      ShowErrorDialog("Check Your Internet Connection");
      print("Error:"+e.toString());
    }


  }

}


  void CircularIndicator()
  {
    AppAlertDialog.ShowDialog(context,"Changing password");

  }

  void ShowErrorDialog(String ErrorMessage)
  {

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: (){

            },
            child: Dialog(
              insetPadding:EdgeInsets.symmetric(horizontal: 25.0),
              shape: RoundedRectangleBorder(
                  borderRadius:
                  BorderRadius.circular(20.0)
              ), //this right here
              child:  Column(
                mainAxisSize: MainAxisSize.min,

                children: [
                  SizedBox(height: MediaQuery.of(context).size.height*0.015,),
                  Image(image: AssetImage("images/errorimage.png"),width: 50.0,height: 50.0,),
                  SizedBox(height: MediaQuery.of(context).size.height*0.02,),
                  Container(
                    margin: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.05,right: MediaQuery.of(context).size.width*0.05),
                    child:Text(ErrorMessage,style: TextStyle(color: Color.fromARGB(0XFF, 0X3E, 0X40, 0X46)),textScaleFactor: 1.2,textAlign: TextAlign.center,),
                  ),

                  SizedBox(height: MediaQuery.of(context).size.height*0.02,),

                  GestureDetector(

                    onTap: (){
                      Navigator.pop(context);
                    },

                    child:Container(
                      //width: MediaQuery.of(context).size.width*0.5,
                      //height: MediaQuery.of(context).size.height*0.05,
                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.05,right: MediaQuery.of(context).size.width*0.05,top: MediaQuery.of(context).size.height*0.015,bottom: MediaQuery.of(context).size.height*0.015),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50.0),
                        color: Color(0xff00587A).withOpacity(0.8),
                      ),
                      child: Text("Okay, Acknowledge",style: TextStyle(color:Color.fromARGB(0XFF, 0XFF, 0XFF, 0XFF),fontWeight: FontWeight.bold),textScaleFactor: 0.9,),
                    ),
                  ),

                  SizedBox(height: MediaQuery.of(context).size.height*0.025,),


                ],
              ),

            ),
          );
        });

  }




  void ToastFunction(String message)
  {
    Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Color(0xff00587A).withOpacity(0.7),
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }


}


