import 'package:flutter/material.dart';
import 'package:snap_fill/SignInScreen/newpassword.dart';

class ConfirmSecurity extends StatefulWidget {
  Map<String,dynamic> securityAnswers = Map<String,dynamic>();
  String userName;
  String userPasswordHash;
  ConfirmSecurity(this.securityAnswers,this.userName,this.userPasswordHash);

  @override
  _ConfirmSecurityState createState() => _ConfirmSecurityState();
}

class _ConfirmSecurityState extends State<ConfirmSecurity> {

  TextEditingController answer1 = TextEditingController();
  TextEditingController answer2 = TextEditingController();
  TextEditingController answer3 = TextEditingController();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        automaticallyImplyLeading: false,
        elevation: 0.0,
        title: Row(
          children: [
            SizedBox(width: 10.0),
            GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Image(image: AssetImage("images/backicon2.png"))
            ),
            SizedBox(width: 25.0),
            Text("Answer Your Security Question", textScaleFactor: 1.0,
              style: TextStyle(color: Colors.black),),

          ],
        ),

      ),


      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(left: MediaQuery
              .of(context)
              .size
              .width * 0.06, right: MediaQuery
              .of(context)
              .size
              .width * 0.06
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Answer security question to reset your password",
                textAlign: TextAlign.center,),

//1 security Question
              SizedBox(height: MediaQuery
                  .of(context)
                  .size
                  .height * 0.05,),

              Text("Security Question 1", textScaleFactor: 1.2),
              SizedBox(height: 5.0),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Color.fromARGB(0XFF, 0XC4, 0XC4, 0XC4).withOpacity(
                      0.5),
                ),
                width: MediaQuery
                    .of(context)
                    .size
                    .width * 0.88,
                padding: EdgeInsets.only(top: MediaQuery
                    .of(context)
                    .size
                    .height * 0.02, left: 12.0, bottom: MediaQuery
                    .of(context)
                    .size
                    .height * 0.02),
                child: Text(widget.securityAnswers["sq1"],
                  textScaleFactor: 1.1,),
              ),


              SizedBox(height: MediaQuery
                  .of(context)
                  .size
                  .height * 0.03,),

              Text("Your Answer", textScaleFactor: 1.2),
              SizedBox(height: 5.0),
              TextFormField(
                controller: answer1,
                //enabled: false,
                decoration: new InputDecoration(
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Colors.grey[200], width: 0.0),
                      borderRadius: BorderRadius.circular(10)
                  ),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Colors.grey[200], width: 0.0),
                      borderRadius: BorderRadius.circular(10)
                  ),
                  fillColor: Color.fromARGB(0XFF, 0XC4, 0XC4, 0XC4)
                      .withOpacity(0.4),
                  filled: true,
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.only(left:12.0),
                ),
              ),


              //2 security Question
              SizedBox(height: MediaQuery
                  .of(context)
                  .size
                  .height * 0.05,),

              Text("Security Question 2", textScaleFactor: 1.2),
              SizedBox(height: 5.0),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Color.fromARGB(0XFF, 0XC4, 0XC4, 0XC4).withOpacity(
                      0.5),
                ),
                width: MediaQuery
                    .of(context)
                    .size
                    .width * 0.88,
                padding: EdgeInsets.only(top: MediaQuery
                    .of(context)
                    .size
                    .height * 0.02, left: 12.0, bottom: MediaQuery
                    .of(context)
                    .size
                    .height * 0.02),
                child: Text(widget.securityAnswers["sq2"],
                  textScaleFactor: 1.1,),
              ),


              SizedBox(height: MediaQuery
                  .of(context)
                  .size
                  .height * 0.03,),

              Text("Your Answer", textScaleFactor: 1.2),
              SizedBox(height: 5.0),
              TextFormField(
                controller: answer2,
                //enabled: false,
                decoration: new InputDecoration(
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Colors.grey[200], width: 0.0),
                      borderRadius: BorderRadius.circular(10)
                  ),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Colors.grey[200], width: 0.0),
                      borderRadius: BorderRadius.circular(10)
                  ),
                  fillColor: Color.fromARGB(0XFF, 0XC4, 0XC4, 0XC4)
                      .withOpacity(0.4),
                  filled: true,
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.only(left:12.0),
                ),
              ),


              //3 security Question
              SizedBox(height: MediaQuery
                  .of(context)
                  .size
                  .height * 0.05,),

              Text("Security Question 3", textScaleFactor: 1.2),
              SizedBox(height: 5.0),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Color.fromARGB(0XFF, 0XC4, 0XC4, 0XC4).withOpacity(
                      0.5),
                ),
                width: MediaQuery
                    .of(context)
                    .size
                    .width * 0.88,
                padding: EdgeInsets.only(top: MediaQuery
                    .of(context)
                    .size
                    .height * 0.02, left: 12.0, bottom: MediaQuery
                    .of(context)
                    .size
                    .height * 0.02),
                child: Text(widget.securityAnswers["sq2"], textScaleFactor: 1.1,),
              ),


              SizedBox(height: MediaQuery
                  .of(context)
                  .size
                  .height * 0.03,),

              Text("Your Answer", textScaleFactor: 1.2),
              SizedBox(height: 5.0),
              TextFormField(
                controller: answer3,
                //enabled: false,
                decoration: new InputDecoration(
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Colors.grey[200], width: 0.0),
                      borderRadius: BorderRadius.circular(10)
                  ),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Colors.grey[200], width: 0.0),
                      borderRadius: BorderRadius.circular(10)
                  ),
                  fillColor: Color.fromARGB(0XFF, 0XC4, 0XC4, 0XC4)
                      .withOpacity(0.4),
                  filled: true,
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.only(left:12.0),
                ),
              ),


              SizedBox(height: 30.0,),


              GestureDetector(
                onTap: () {
                  //CompleteRegisteration();
                  CompleteRegisteration();
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: Color(0xff00587A),
                      borderRadius: BorderRadius.circular(20)
                  ),
                  height: 60.0,
                  // width: MediaQuery.of(context).size.width * 0.8,
                  child: Center(
                    child: Text("Complete Security",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ),

              SizedBox(height: 10.0,),


            ],
          ),

        ),
      ),


    );
  }


  void CompleteRegisteration()
  {
    if(answer1.text.isEmpty || answer2.text.isEmpty || answer3.text.isEmpty)
      {
        ShowErrorDialog("Security Answers can not be empty");
      }
    else if(widget.securityAnswers["sa1"] != answer1.text  || widget.securityAnswers["sa2"] != answer2.text || widget.securityAnswers["sa3"] != answer3.text)
      {
        ShowErrorDialog("Security answers are wrong");
      }

      else
        {
          Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => NewPassword(widget.userName,widget.userPasswordHash)));
        }

  }



  void ShowErrorDialog(String ErrorMessage) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            insetPadding: EdgeInsets.symmetric(horizontal: 25.0),
            shape: RoundedRectangleBorder(
                borderRadius:
                BorderRadius.circular(20.0)
            ), //this right here
            child: Column(
              mainAxisSize: MainAxisSize.min,

              children: [
                SizedBox(height: MediaQuery
                    .of(context)
                    .size
                    .height * 0.015,),
                Image(image: AssetImage("images/errorimage.png"),
                  width: 50.0,
                  height: 50.0,),
                SizedBox(height: MediaQuery
                    .of(context)
                    .size
                    .height * 0.02,),
                Container(
                  margin: EdgeInsets.only(left: MediaQuery
                      .of(context)
                      .size
                      .width * 0.05, right: MediaQuery
                      .of(context)
                      .size
                      .width * 0.05),
                  child: Text(ErrorMessage, style: TextStyle(
                      color: Color.fromARGB(0XFF, 0X3E, 0X40, 0X46)),
                    textScaleFactor: 1.2,
                    textAlign: TextAlign.center,),
                ),

                SizedBox(height: MediaQuery
                    .of(context)
                    .size
                    .height * 0.02,),

                GestureDetector(

                  onTap: () {
                    Navigator.pop(context);
                  },

                  child: Container(
                    //width: MediaQuery.of(context).size.width*0.5,
                    //height: MediaQuery.of(context).size.height*0.05,
                    padding: EdgeInsets.only(left: MediaQuery
                        .of(context)
                        .size
                        .width * 0.05, right: MediaQuery
                        .of(context)
                        .size
                        .width * 0.05, top: MediaQuery
                        .of(context)
                        .size
                        .height * 0.015, bottom: MediaQuery
                        .of(context)
                        .size
                        .height * 0.015),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50.0),
                      color: Color(0xff00587A).withOpacity(0.8),
                    ),
                    child: Text("Okay, Acknowledge", style: TextStyle(
                        color: Color.fromARGB(0XFF, 0XFF, 0XFF, 0XFF),
                        fontWeight: FontWeight.bold), textScaleFactor: 0.9,),
                  ),
                ),

                SizedBox(height: MediaQuery
                    .of(context)
                    .size
                    .height * 0.025,),


              ],
            ),

          );
        });
  }
}
