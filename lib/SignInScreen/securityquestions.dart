import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';
import 'package:snap_fill/DashboardScreens/first_page.dart';
import 'package:snap_fill/datamodel/apisurl.dart';
import 'package:snap_fill/datamodel/appalertdialog.dart';
import 'package:snap_fill/datamodel/sharedpreferenceapp.dart';

import 'create_account.dart';

class SecurityQuestions extends StatefulWidget {

  String userName,userFirstLastName,userPassword;

  SecurityQuestions(this.userName,this.userFirstLastName,this.userPassword);

  @override
  _SecurityQuestionsState createState() => _SecurityQuestionsState();
}

class _SecurityQuestionsState extends State<SecurityQuestions> {

  TextEditingController answer1 = TextEditingController();
  TextEditingController answer2 = TextEditingController();
  TextEditingController answer3 = TextEditingController();
  String _firstName = " " , _lastName = " ";
  String source = "N/A";
  String role = "user";
  String securityAnswers;
  Map<String,dynamic> loginData = Map<String,dynamic>();
  SharedPreferenceApp shPrefApp = SharedPreferenceApp();
  String question1="What was the house number and street name you lived in as a child?";
  String question2="What primary school did you attend?";
  String question3="What is your spouse or partner's mother's maiden name?";

  List<Map<String,dynamic>> questionsDropDown = [
    {
      "questions":"What was the house number and street name you lived in as a child?",
    },
    {
      "questions":"What were the last four digits of your childhood telephone number?",
    },
    {
      "questions":"What primary school did you attend?",
    },
    {
      "questions":"In what town or city was your first full time job?",
    },
    {
      "questions":" In what town or city did you meet your partner?",
    },
    {
      "questions":"What is the middle name of your oldest child?",
    },
    {
      "questions":"What are the last five digits of your driver's license number?",
    },
    {
      "questions":"What is your grandmother's (on your mother's side) maiden name?",
    },
    {
      "questions":"What is your spouse or partner's mother's maiden name?",
    },
    {
      "questions":"In what town or city did your parents meet?",
    },
    {
      "questions":"What time of the day were you born? (hh:mm)",
    },
    {
      "questions":"What time of the day was your first child born? (hh:mm)",
    }
  ];


  @override
  Widget build(BuildContext context) {
    return
      Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        automaticallyImplyLeading: false,
        elevation: 0.0,
        title: Row(
          children: [
            SizedBox(width:10.0),
            GestureDetector(
                onTap: (){
                  Navigator.pop(context);
                },
                child: Image(image:AssetImage("images/backicon2.png"))
            ),
            SizedBox(width:25.0),
            Text("Add Security Questions",textScaleFactor: 1.1,style: TextStyle(color:Colors.black),),

          ],
        ),

      ),


      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(left:MediaQuery.of(context).size.width*0.06,right:MediaQuery.of(context).size.width*0.06
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
             Text("Add security questions for password recovery"),

//1 security Question
          SizedBox(height: MediaQuery.of(context).size.height*0.05,),

          Text("Security Question 1",textScaleFactor: 1.2),
             SizedBox(height: 5.0),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              color: Color.fromARGB(0XFF, 0XC4, 0XC4, 0XC4).withOpacity(0.5),
            ),
            width: MediaQuery.of(context).size.width*0.88,
            padding: EdgeInsets.only(top:MediaQuery.of(context).size.height*0.01,left: 12.0,bottom:MediaQuery.of(context).size.height*0.01,),
            child:
            DropdownButtonHideUnderline(
              child: DropdownButton<String>(
                //isDense: true,

                //hint: new Text("Select Bank"),
                value: question1,
                onChanged: (String newValue) {
                  print(newValue);
                  setState(() {
                    question1 = newValue;

                  });

                  print(question1);
                },
                items: questionsDropDown.map((Map map) {
                  return new DropdownMenuItem<String>(
                      value: map["questions"].toString(),

                      // value: _mySelection,
                      child: Container(
                        width: MediaQuery.of(context).size.width*0.75,
                        child:Text(map["questions"],textScaleFactor: 1.0,)
                      )

                  );
                }).toList(),

              ),
            ),
          ),


             SizedBox(height: MediaQuery.of(context).size.height*0.03,),

             Text("Your Answer",textScaleFactor: 1.2),
             SizedBox(height: 5.0),
             TextFormField(
               controller: answer1,
               //enabled: false,
               decoration: new InputDecoration(
                 focusedBorder: OutlineInputBorder(
                     borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                     borderRadius: BorderRadius.circular(10)
                 ),
                 enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                     borderRadius: BorderRadius.circular(10)
                 ),
                 fillColor: Color.fromARGB(0XFF, 0XC4, 0XC4, 0XC4).withOpacity(0.4),
                 filled: true,
                 border: InputBorder.none,
                 contentPadding: EdgeInsets.only(left:12.0,right:5.0),
               ),
             ),




              //2 security Question
              SizedBox(height: MediaQuery.of(context).size.height*0.05,),

              Text("Security Question 2",textScaleFactor: 1.2),
              SizedBox(height: 5.0),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Color.fromARGB(0XFF, 0XC4, 0XC4, 0XC4).withOpacity(0.5),
                ),
                width: MediaQuery.of(context).size.width*0.88,
                padding: EdgeInsets.only(top:MediaQuery.of(context).size.height*0.01,left: 12.0,bottom:MediaQuery.of(context).size.height*0.01,),
                child:DropdownButtonHideUnderline(
                  child: DropdownButton<String>(
                    //isDense: true,

                    //hint: new Text("Select Bank"),
                    value: question2,
                    onChanged: (String newValue) {
                      print(newValue);
                      setState(() {
                        question2 = newValue;

                      });

                      print(question2);
                    },
                    items: questionsDropDown.map((Map map) {
                      return new DropdownMenuItem<String>(
                          value: map["questions"].toString(),

                          // value: _mySelection,
                          child: Container(
                              width: MediaQuery.of(context).size.width*0.75,
                              child:Text(map["questions"],textScaleFactor: 1.0,)
                          )

                      );
                    }).toList(),

                  ),
                ),
              ),


              SizedBox(height: MediaQuery.of(context).size.height*0.03,),

              Text("Your Answer",textScaleFactor: 1.2),
              SizedBox(height: 5.0),
              TextFormField(
                controller: answer2,
                //enabled: false,
                decoration: new InputDecoration(
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                      borderRadius: BorderRadius.circular(10)
                  ),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                      borderRadius: BorderRadius.circular(10)
                  ),
                  fillColor: Color.fromARGB(0XFF, 0XC4, 0XC4, 0XC4).withOpacity(0.4),
                  filled: true,
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.only(left:12.0),
                ),
              ),





              //3 security Question
              SizedBox(height: MediaQuery.of(context).size.height*0.05,),

              Text("Security Question 3",textScaleFactor: 1.2),
              SizedBox(height: 5.0),
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Color.fromARGB(0XFF, 0XC4, 0XC4, 0XC4).withOpacity(0.5),
                ),
                width: MediaQuery.of(context).size.width*0.88,
                padding: EdgeInsets.only(top:MediaQuery.of(context).size.height*0.01,left: 12.0,bottom:MediaQuery.of(context).size.height*0.01,),
                child:DropdownButtonHideUnderline(
                  child: DropdownButton<String>(
                    //isDense: true,

                    //hint: new Text("Select Bank"),
                    value: question3,
                    onChanged: (String newValue) {
                      print(newValue);
                      setState(() {
                        question3 = newValue;

                      });

                      print(question3);
                    },
                    items: questionsDropDown.map((Map map) {
                      return new DropdownMenuItem<String>(
                          value: map["questions"].toString(),

                          // value: _mySelection,
                          child: Container(
                              width: MediaQuery.of(context).size.width*0.75,
                              child:Text(map["questions"],textScaleFactor: 1.0,)
                          )

                      );
                    }).toList(),

                  ),
                ),
              ),


              SizedBox(height: MediaQuery.of(context).size.height*0.03,),

              Text("Your Answer",textScaleFactor: 1.2),
              SizedBox(height: 5.0),
              TextFormField(
                controller: answer3,
                //enabled: false,
                decoration: new InputDecoration(
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                      borderRadius: BorderRadius.circular(10)
                  ),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                      borderRadius: BorderRadius.circular(10)
                  ),
                  fillColor: Color.fromARGB(0XFF, 0XC4, 0XC4, 0XC4).withOpacity(0.4),
                  filled: true,
                  border: InputBorder.none,
                  contentPadding: EdgeInsets.only(left:12.0),
                ),

              ),


              SizedBox(height: 30.0,),


              GestureDetector(
                onTap: ()
                {
//                  print("Question1:${question1}");
//                  print("Question2:${question2}");
//                  print("Question3:${question3}");
                  CompleteRegisteration();
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: Color(0xff00587A),
                      borderRadius: BorderRadius.circular(20)
                  ),
                  height: 60.0,
                  // width: MediaQuery.of(context).size.width * 0.8,
                  child: Center(
                    child: Text("Complete Registeration",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ),

              SizedBox(height: 10.0,),



            ],
          ),

        ),
      ),




    );

  }


  void CompleteRegisteration() async
  {
    if(question1 == question2 || question1 == question3 || question2 == question3)
      {
        ShowErrorDialog("Please select different Questions");
      }

    else if(answer1.text.isEmpty || answer2.text.isEmpty || answer3.text.isEmpty )
    {
      ShowErrorDialog("Security Answers can not be Empty");
    }

    else
      {
        securityAnswers = "";

        CircularIndicator();
        try {
           _firstName = " ";
           _lastName = " ";

           _firstName = widget.userFirstLastName.split(" ")[0];

           for(int i=1 ; i<widget.userFirstLastName.split(" ").length;i++)
             {
               _lastName = widget.userFirstLastName.substring(widget.userFirstLastName.indexOf(" ") +1);
               break;
             }

             securityAnswers = '{"sq1":"$question1","sq2":"$question2","sq3":"$question3","sa1":"${answer1.text}","sa2":"${answer2.text}","sa3":"${answer3.text}"}';


            String url = signUpApi;
            String json = '{"username":"${widget.userName}","first_name":"${_firstName}","last_name":"${_lastName}","password":"${widget.userPassword}","source":"${source}","role":"${role}","sq":$securityAnswers}';

            print("Json is:"+json);

           Response response = await post(url, headers: header, body: json);

            print("Response status code:"+response.statusCode.toString());

           if(response.statusCode == 201)
             {
               Navigator.of(context, rootNavigator: true).pop();
               print("SignUp Response:"+response.body.toString());


               loginData = jsonDecode(response.body);

               print("User Name:"+loginData["userdata"]["signup_data"]["username"]);
               print("First Name:"+loginData["userdata"]["signup_data"]["first_name"]);
               print("Last Name:"+loginData["userdata"]["signup_data"]["last_name"]);
               print("Profile Name:"+loginData["userdata"]["profile_name"]);
               print("Source:"+loginData["userdata"]["signup_data"]["source"]);
               print("Role:"+loginData["userdata"]["signup_data"]["role"]);
               print("Nonce Base:"+loginData["userdata"]["current_nonce_base"].toString());
               print("Password:"+loginData["password"]);
               print("User Id:"+loginData["user_id"]);

               await shPrefApp.SetUserPersonalInformation(loginData["userdata"]["signup_data"]["username"], loginData["userdata"]["signup_data"]["first_name"], loginData["userdata"]["signup_data"]["last_name"], loginData["userdata"]["profile_name"], loginData["userdata"]["signup_data"]["source"], loginData["userdata"]["signup_data"]["role"]);
               await shPrefApp.SetUserAppInformation(loginData["userdata"]["current_nonce_base"], loginData["password"], loginData["user_id"]);
               await shPrefApp.SetSignUpInformation(loginData["userdata"]["current_nonce_base"], loginData["user_id"]);


               Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
                   FirstTimeScreen(0)), (Route<dynamic> route) => false);
             }

           else if(response.statusCode == 400)
             {
               Navigator.of(context, rootNavigator: true).pop();
               print("ya hoga:${jsonDecode(response.body)["message"]}");
               ShowErrorDialog(jsonDecode(response.body)["message"] == "Password should be at least 8 chars and include numerals and special chars" ? "The password should be at least 8 characters with at least 1 Upper Case,1 lower case, 1 numeric character, and 1 special character" : jsonDecode(response.body)["message"]);
             }


            else
              {
                Navigator.of(context, rootNavigator: true).pop();
                ShowErrorDialog("Your Internet is not Working Properly");
              }
         }
         catch(e) {
           Navigator.of(context, rootNavigator: true).pop();
           ShowErrorDialog("Check Your Internet Connection");
           print("Error:"+e.toString());
         }

        }

  }



  void CircularIndicator()
  {
    AppAlertDialog.ShowDialog(context,"Registering your account");

  }

  void ShowErrorDialog(String ErrorMessage)
  {

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: (){

            },
            child: Dialog(
              insetPadding:EdgeInsets.symmetric(horizontal: 25.0),
              shape: RoundedRectangleBorder(
                  borderRadius:
                  BorderRadius.circular(20.0)
              ), //this right here
              child:  Column(
                mainAxisSize: MainAxisSize.min,

                children: [
                  SizedBox(height: MediaQuery.of(context).size.height*0.015,),
                  Image(image: AssetImage("images/errorimage.png"),width: 50.0,height: 50.0,),
                  SizedBox(height: MediaQuery.of(context).size.height*0.02,),
                  Container(
                    margin: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.05,right: MediaQuery.of(context).size.width*0.05),
                    child:Text(ErrorMessage,style: TextStyle(color: Color.fromARGB(0XFF, 0X3E, 0X40, 0X46)),textScaleFactor: 1.2,textAlign: TextAlign.center,),
                  ),

                  SizedBox(height: MediaQuery.of(context).size.height*0.02,),

                  GestureDetector(

                    onTap: (){
                      Navigator.pop(context);
                    },

                    child:Container(
                      //width: MediaQuery.of(context).size.width*0.5,
                      //height: MediaQuery.of(context).size.height*0.05,
                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.05,right: MediaQuery.of(context).size.width*0.05,top: MediaQuery.of(context).size.height*0.015,bottom: MediaQuery.of(context).size.height*0.015),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50.0),
                        color: Color(0xff00587A).withOpacity(0.8),
                      ),
                      child: Text("Okay, Acknowledge",style: TextStyle(color:Color.fromARGB(0XFF, 0XFF, 0XFF, 0XFF),fontWeight: FontWeight.bold),textScaleFactor: 0.9,),
                    ),
                  ),

                  SizedBox(height: MediaQuery.of(context).size.height*0.025,),


                ],
              ),

            ),
          );
        });

  }




  void ToastFunction(String message)
  {
    Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Color(0xff00587A).withOpacity(0.7),
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }



}
