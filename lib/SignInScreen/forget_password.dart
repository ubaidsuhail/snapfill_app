import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_otp_text_field/flutter_otp_text_field.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';
import 'package:snap_fill/DashboardScreens/first_page.dart';
import 'package:snap_fill/SignInScreen/new_account.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';
import 'package:snap_fill/SignInScreen/newpassword.dart';
import 'package:snap_fill/datamodel/apisurl.dart';
import 'package:snap_fill/datamodel/appalertdialog.dart';

import 'confirmsecurity.dart';
import 'login_page.dart';
class ForgetPassword extends StatefulWidget {
  @override
  _ForgetPasswordState createState() => _ForgetPasswordState();
}

class _ForgetPasswordState extends State<ForgetPassword> {
  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }
  bool _obscureText = true;
  bool _isConfirm = false;
  bool _isVarify = false;
  final _emailForm = GlobalKey<FormState>();
  TextEditingController _userName= TextEditingController();
  Pattern emailPattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
            backgroundColor: Colors.white,

          appBar: AppBar(
            backgroundColor: Colors.white,
            leading: GestureDetector(
              onTap: (){
                Navigator.of(context)
                    .pushReplacement(new MaterialPageRoute(builder: (context) => LoginPage()));
              },
                child: Icon(Icons.arrow_back,color: Colors.black,)
            ),
            elevation: 0.0,
          ),

          body: SingleChildScrollView(
            child:Form(
              key:_emailForm,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Center(
                    child: Text(
                      'Snapfill',
                      textScaleFactor: 3.5,
                      style: TextStyle(color: Color(0xff00587A),fontWeight: FontWeight.w500),
                    ),
                  ),
                  SizedBox(
                    height: 25,
                  ),
                  Center(
                    child: Text(
                      'Forget Password',
                      textScaleFactor: 1.5,
                      style: TextStyle(color: Colors.black,fontWeight: FontWeight.w500),
                    ),
                  ),

                  SizedBox(
                    height: 25,
                  ),

                  Column(
                    children: [

                      Center(
                        child: Text(
                          'Enter your Email to reset your \npassword',
                          textScaleFactor: 1.0,
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.black,fontWeight: FontWeight.w100),
                        ),
                      ),
                      SizedBox(height: 35.0,),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: TextFormField(
                          controller: _userName,
                          validator: (value) {
                            RegExp regex = new RegExp(emailPattern);
                            if (value.isEmpty) {
                              return 'Email can not be empty';
                            }
                            else if(!regex.hasMatch(value))
                            {
                              return 'Enter Valid Email Format';
                            }

                            return null;
                          },
                          decoration: new InputDecoration(
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                borderRadius: BorderRadius.circular(10)
                            ),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                borderRadius: BorderRadius.circular(10)
                            ),
                            errorBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.red[300], width: 0.0),
                                borderRadius: BorderRadius.circular(10)
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.red[300], width: 0.0),
                                borderRadius: BorderRadius.circular(10)
                            ),
                            fillColor: Colors.grey[200],
                            filled: true,
                            border: InputBorder.none,
                            prefixIcon: Icon(Icons.email,color: Color(0xff00587A),),
                            hintText: 'mathew@gmail.com',
                          ),
                        ),
                      ),
                    ],
                  ),



                  SizedBox(height: MediaQuery.of(context).size.height * 0.1),

                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 15),
                    child: GestureDetector(
                      onTap: (){

                        if(_emailForm.currentState.validate()) {

                          ConfirmEmail();

                        }
                        else{
                          print("invalid");
                        }
//                      Navigator.of(context)
//                          .push(new MaterialPageRoute(builder: (context) => NewPassword()));

                      },
                      child: Container(
                        decoration: BoxDecoration(
                            color: Color(0xff00587A),
                            borderRadius: BorderRadius.circular(15)
                        ),
                        height: 60.0,
                        // width: MediaQuery.of(context).size.width * 0.8,
                        child: Center(
                          child: Text("Confirm",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  )


                ],
              ),
            ),
          )
      ),
      onWillPop: (){
        Navigator.of(context)
            .pushReplacement(new MaterialPageRoute(builder: (context) => LoginPage()));
      },
    );
  }

  void ConfirmEmail() async
  {

    //Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => ConfirmSecurity()));
    try {
      CircularIndicator();

      String url = confirmEmailApi+"${_userName.text}"+"/userdata.signup_data&password&user_id?app=snapfill";


      Response response = await get(url, headers: header);

      print("Response status code:"+response.statusCode.toString());
      print("Response body:"+response.body);

      if(response.statusCode == 200)
      {
        Navigator.of(context, rootNavigator: true).pop();
        _userName.text = "";
        print("SqQuestions"+jsonDecode(response.body)["data"]["userdata.signup_data"]["sq"].toString());
        print("User Name"+jsonDecode(response.body)["data"]["userdata.signup_data"]["username"].toString());
        print("Password"+jsonDecode(response.body)["data"]["password"].toString());
        //print(jsonDecode(response.body)["data"]["password"]);
        Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => ConfirmSecurity(jsonDecode(response.body)["data"]["userdata.signup_data"]["sq"],jsonDecode(response.body)["data"]["userdata.signup_data"]["username"],jsonDecode(response.body)["data"]["password"])));

      }

      else if(response.statusCode == 404)
      {
        Navigator.of(context, rootNavigator: true).pop();
        ShowErrorDialog(jsonDecode(response.body)["message"]);
      }


      else
      {
        Navigator.of(context, rootNavigator: true).pop();
        ShowErrorDialog("Your Internet is not Working Properly");
      }
    }
    catch(e) {
      Navigator.of(context, rootNavigator: true).pop();
      ShowErrorDialog("Check Your Internet Connection");
      print("Error:"+e.toString());
    }
 }


  void CircularIndicator()
  {
    AppAlertDialog.ShowDialog(context,"Email confirmation");

  }

  void ShowErrorDialog(String ErrorMessage)
  {

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: (){

            },
            child: Dialog(
              insetPadding:EdgeInsets.symmetric(horizontal: 25.0),
              shape: RoundedRectangleBorder(
                  borderRadius:
                  BorderRadius.circular(20.0)
              ), //this right here
              child:  Column(
                mainAxisSize: MainAxisSize.min,

                children: [
                  SizedBox(height: MediaQuery.of(context).size.height*0.015,),
                  Image(image: AssetImage("images/errorimage.png"),width: 50.0,height: 50.0,),
                  SizedBox(height: MediaQuery.of(context).size.height*0.02,),
                  Container(
                    margin: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.05,right: MediaQuery.of(context).size.width*0.05),
                    child:Text(ErrorMessage,style: TextStyle(color: Color.fromARGB(0XFF, 0X3E, 0X40, 0X46)),textScaleFactor: 1.2,textAlign: TextAlign.center,),
                  ),

                  SizedBox(height: MediaQuery.of(context).size.height*0.02,),

                  GestureDetector(

                    onTap: (){
                      Navigator.pop(context);
                    },

                    child:Container(
                      //width: MediaQuery.of(context).size.width*0.5,
                      //height: MediaQuery.of(context).size.height*0.05,
                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.05,right: MediaQuery.of(context).size.width*0.05,top: MediaQuery.of(context).size.height*0.015,bottom: MediaQuery.of(context).size.height*0.015),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50.0),
                        color: Color(0xff00587A).withOpacity(0.8),
                      ),
                      child: Text("Okay, Acknowledge",style: TextStyle(color:Color.fromARGB(0XFF, 0XFF, 0XFF, 0XFF),fontWeight: FontWeight.bold),textScaleFactor: 0.9,),
                    ),
                  ),

                  SizedBox(height: MediaQuery.of(context).size.height*0.025,),


                ],
              ),

            ),
          );
        });

  }


}
