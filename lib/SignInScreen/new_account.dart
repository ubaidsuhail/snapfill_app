import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:snap_fill/SignInScreen/login_page.dart';
import 'package:snap_fill/SignInScreen/securityquestions.dart';
import 'package:snap_fill/datamodel/apisurl.dart';

import '../constant.dart';
import 'package:http/http.dart';



class NewAccount extends StatefulWidget {
  @override
  _NewAccountState createState() => _NewAccountState();
}

class _NewAccountState extends State<NewAccount> {

  final _signUpForm = GlobalKey<FormState>();

  TextEditingController _userFirstLastName = TextEditingController();
  TextEditingController _userName = TextEditingController();
  TextEditingController _userPassword = TextEditingController();
  TextEditingController _userConfirmPassword = TextEditingController();

  Pattern emailPattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';

   bool userPasswordVisible = false;
   bool userConfirmPasswordVisible = false;

   String _firstName = " " , _lastName = " ";
   String source = "N/A";
   String role = "user";


  bool _value= false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: GestureDetector(
            onTap: (){
              Navigator.of(context).pop();
            },
            child: Icon(Icons.arrow_back,color: Colors.black,)),
        elevation: 0.0,
      ),

      body: SingleChildScrollView(
    child:Form(
      key: _signUpForm,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [


          Center(
            child: Text(
              'Snapfill',
              textScaleFactor: 3.5,
              style: TextStyle(color: Color(0xff00587A),fontWeight: FontWeight.w500),
            ),
          ),
          SizedBox(
            height: 25,
          ),
          Center(
            child: Text(
              'New Account',
              textScaleFactor: 1.5,
              style: TextStyle(color: Colors.black,fontWeight: FontWeight.w500),
            ),
          ),

          SizedBox(
            height: 25,
          ),
          Center(
            child: Text(
              'Start by entering your email address below.',
              textScaleFactor: 1.0,
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.black,fontWeight: FontWeight.w100),
            ),
          ),
          SizedBox(height: 15.0,),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: TextFormField(
              controller: _userFirstLastName,
              validator: (value) {
                if (value.isEmpty) {
                  return 'User Name can not be empty';
                }

                return null;
              },

              decoration: new InputDecoration(
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                    borderRadius: BorderRadius.circular(10)
                ),
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                    borderRadius: BorderRadius.circular(10)
                ),

                errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.red[300], width: 0.0),
                    borderRadius: BorderRadius.circular(10)
                ),
                focusedErrorBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.red[300], width: 0.0),
                    borderRadius: BorderRadius.circular(10)
                ),

                border: InputBorder.none,
                hintText: 'Name',
                fillColor: Colors.grey[200],
                filled: true,
              ),
            ),
          ),

          SizedBox(height: 15.0,),

          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: TextFormField(
              controller: _userName,

              validator: (value) {
                RegExp regex = new RegExp(emailPattern);
                if (value.isEmpty) {
                  return 'Email can not be empty';
                }
                else if(!regex.hasMatch(value))
                {
                  return 'Enter Valid Email Format';
                }

                return null;
              },


              decoration: new InputDecoration(
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                    borderRadius: BorderRadius.circular(10)
                ),
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                    borderRadius: BorderRadius.circular(10)
                ),

                errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.red[300], width: 0.0),
                    borderRadius: BorderRadius.circular(10)
                ),
                focusedErrorBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.red[300], width: 0.0),
                    borderRadius: BorderRadius.circular(10)
                ),

                hintText: 'Email',
                fillColor: Colors.grey[200],
                filled: true,
              ),
            ),
          ),
          SizedBox(height: 15.0,),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: TextFormField(
              controller: _userPassword,

              validator: (value) {
                if (value.isEmpty) {
                  return 'Password can not be empty';
                }
                return null;
              },

              decoration: new InputDecoration(
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                    borderRadius: BorderRadius.circular(10)
                ),
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                    borderRadius: BorderRadius.circular(10)
                ),

                errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.red[300], width: 0.0),
                    borderRadius: BorderRadius.circular(10)
                ),
                focusedErrorBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.red[300], width: 0.0),
                    borderRadius: BorderRadius.circular(10)
                ),

                suffixIcon: IconButton(
                  icon: userPasswordVisible
                      ? Icon(Icons.visibility,
                      color: Color(0xff00587A))
                      : Icon(Icons.visibility_off,
                      color: Color(0xff00587A)),
                  onPressed: UserPasswordToggle,
                ),
                hintText: 'Password',
                fillColor: Colors.grey[200],
                filled: true,
              ),
              obscureText: userPasswordVisible ? false : true,
            ),
          ),
          SizedBox(height: 15.0,),

          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: TextFormField(
              controller: _userConfirmPassword,

              validator: (value) {
                if (value.isEmpty) {
                  return 'Confirm Password can not be empty';
                }
                return null;
              },

              decoration: new InputDecoration(
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                    borderRadius: BorderRadius.circular(10)
                ),
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                    borderRadius: BorderRadius.circular(10)
                ),

                errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.red[300], width: 0.0),
                    borderRadius: BorderRadius.circular(10)
                ),
                focusedErrorBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.red[300], width: 0.0),
                    borderRadius: BorderRadius.circular(10)
                ),

                suffixIcon: IconButton(
                  icon: userConfirmPasswordVisible
                      ? Icon(Icons.visibility,
                      color: Color(0xff00587A))
                      : Icon(Icons.visibility_off,
                      color: Color(0xff00587A)),
                  onPressed: UserConfirmPasswordToggle,
                ),

                hintText: 'Confirm Password',
                fillColor: Colors.grey[200],
                filled: true,
              ),
              obscureText: userConfirmPasswordVisible ? false : true,

            ),
          ),


          SizedBox(
            height: 25.0,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 15),
            child: GestureDetector(

              onTap: (){
                if(_signUpForm.currentState.validate()) {

                  SignUp();

                }
                else{
                  print("invalid");
                }
              },

              child: Container(
                decoration: BoxDecoration(
                    color: Color(0xff00587A),
                    borderRadius: BorderRadius.circular(20)
                ),
                height: 60.0,
                // width: MediaQuery.of(context).size.width * 0.8,
                child: Center(
                  child: Text("Sign Up",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ),
          ),
          SizedBox(height: 15,),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(
                  child: InkWell(
                    onTap: () {
                      setState(() {
                        _value = !_value;
                      });
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          border: Border.all(color: kAppMainColor),
                          shape: BoxShape.rectangle, color: Colors.white),
                      child: Padding(
                        padding: _value ? EdgeInsets.all(3.0): EdgeInsets.all(3.0),
                        child: _value
                            ? Icon(
                          Icons.check,
                          size: 10.0,
                          color: kAppMainColor,
                        )
                            : Icon(
                          Icons.check_box_outline_blank,
                          size: 10.0,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  )),
              SizedBox(width: 12,),
              Container(
                  width: MediaQuery.of(context).size.width * 0.8,
                  child: Text("By signing up, you agree to our Terms and Conditions of Use",maxLines: 2,)),

            ],
          ),
          SizedBox(height:MediaQuery.of(context).size.height *0.05),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Already have an account?"),
              SizedBox(
                width: 10.0,
              ),
              GestureDetector(

                  onTap: (){

                    Navigator.of(context)
                        .pushReplacement(new MaterialPageRoute(builder: (context) => LoginPage()));
                  },
                  child: Text("Sign In",style: TextStyle(color: Color(0xff00587A)),)),
            ],
          ),
          SizedBox(
            height: 15.0,
          ),
        ],
      ),
    ),
    ),
    );
  }



  void UserPasswordToggle()
  {
    if(userPasswordVisible)
      {
        userPasswordVisible = false;
      }
      else
        {
          userPasswordVisible = true;
        }

      setState(() {
        userPasswordVisible;
      });
  }


  void UserConfirmPasswordToggle()
  {
    if(userConfirmPasswordVisible)
    {
      userConfirmPasswordVisible = false;
    }
    else
    {
      userConfirmPasswordVisible = true;
    }

    setState(() {
      userConfirmPasswordVisible;
    });
  }


  void SignUp() async
  {
    print("Name:"+_userFirstLastName.text);
    print("Email:"+_userName.text);
    print("Password:"+_userPassword.text);
    print("Confirm Password:"+_userConfirmPassword.text);


    if(_userPassword.text != _userConfirmPassword.text)
      {
        ShowErrorDialog("Password and Confirm Password do not match");
      }

    else if(!_value)
      {
        ShowErrorDialog("You need to agree on Terms and Condition");
      }

     else
       {
         Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => SecurityQuestions(_userName.text,_userFirstLastName.text,_userPassword.text)));
         //CircularIndicator();


       }


  }



//  void CircularIndicator()
//  {
//    showDialog(
//        context: context,
//        barrierDismissible: false,
//        builder: (context) {
//          return WillPopScope(
//            onWillPop: (){
//
//            },
//            child: Column(
//              mainAxisSize: MainAxisSize.min,
//              mainAxisAlignment: MainAxisAlignment.center,
//              children: [
//                Container(
//
//                  decoration: BoxDecoration(
//                      color:Colors.white,
////                      borderRadius: BorderRadius.circular(10.0),
//                      shape: BoxShape.circle
//                  ),
//                  height: MediaQuery.of(context).size.height*0.07,
//                  child: Center(child: CircularProgressIndicator(),),
//                )
//              ],
//            ),
//          );
//
//        });
//
//  }

  void ShowErrorDialog(String ErrorMessage)
  {

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: (){

            },
            child: Dialog(
              insetPadding:EdgeInsets.symmetric(horizontal: 25.0),
              shape: RoundedRectangleBorder(
                  borderRadius:
                  BorderRadius.circular(20.0)
              ), //this right here
              child:  Column(
                mainAxisSize: MainAxisSize.min,

                children: [
                  SizedBox(height: MediaQuery.of(context).size.height*0.015,),
                  Image(image: AssetImage("images/errorimage.png"),width: 50.0,height: 50.0,),
                  SizedBox(height: MediaQuery.of(context).size.height*0.02,),
                  Container(
                    margin: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.05,right: MediaQuery.of(context).size.width*0.05),
                    child:Text(ErrorMessage,style: TextStyle(color: Color.fromARGB(0XFF, 0X3E, 0X40, 0X46)),textScaleFactor: 1.2,textAlign: TextAlign.center,),
                  ),

                  SizedBox(height: MediaQuery.of(context).size.height*0.02,),

                  GestureDetector(

                    onTap: (){
                      Navigator.pop(context);
                    },

                    child:Container(
                      //width: MediaQuery.of(context).size.width*0.5,
                      //height: MediaQuery.of(context).size.height*0.05,
                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.05,right: MediaQuery.of(context).size.width*0.05,top: MediaQuery.of(context).size.height*0.015,bottom: MediaQuery.of(context).size.height*0.015),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50.0),
                        color: Color(0xff00587A).withOpacity(0.8),
                      ),
                      child: Text("Okay, Acknowledge",style: TextStyle(color:Color.fromARGB(0XFF, 0XFF, 0XFF, 0XFF),fontWeight: FontWeight.bold),textScaleFactor: 0.9,),
                    ),
                  ),

                  SizedBox(height: MediaQuery.of(context).size.height*0.025,),


                ],
              ),

            ),
          );
        });

  }




 void ToastFunction(String message)
 {
   Fluttertoast.showToast(
     msg: message,
     toastLength: Toast.LENGTH_SHORT,
     gravity: ToastGravity.BOTTOM,
     timeInSecForIosWeb: 1,
     backgroundColor: Color(0xff00587A).withOpacity(0.7),
     textColor: Colors.white,
     fontSize: 16.0,
   );
 }


}
