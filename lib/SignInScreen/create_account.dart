import 'dart:convert';

import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';
import 'package:snap_fill/DashboardScreens/dashboard.dart';
import 'package:snap_fill/DashboardScreens/first_page.dart';
import 'package:snap_fill/SignInScreen/login_page.dart';
import 'package:snap_fill/SignInScreen/new_account.dart';
import 'dart:io';

import 'package:snap_fill/datamodel/apisurl.dart';
import 'package:snap_fill/datamodel/appalertdialog.dart';
import 'package:snap_fill/datamodel/sharedpreferenceapp.dart';

class CreateAccount extends StatefulWidget {
  @override
  _CreateAccountState createState() => _CreateAccountState();
}

class _CreateAccountState extends State<CreateAccount> {

  DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  var specificDeviceInfo;
  String deviceId;
  DateTime currentTime;
  String userPassword = "Snapfill@123";
  Map<String,dynamic> loginData = Map<String,dynamic>();
  SharedPreferenceApp shPrefApp = SharedPreferenceApp();

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(
              height: 25.0,
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Image(image: AssetImage("images/createAccount.png"),),
            ),
            SizedBox(
              height: 15.0,
            ),
            Text("Scan and Auto Fill Your Documents With\n No Hassle",
            textAlign: TextAlign.center,
              style: TextStyle(
                color: Color(0xff00587A)
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            GestureDetector(
              onTap: (){
                Navigator.of(context)
                    .push(new MaterialPageRoute(builder: (context) => NewAccount()));
              },
              child: Container(
                decoration: BoxDecoration(
                  color: Color(0xff00587A),
                  borderRadius: BorderRadius.circular(15)
                ),
                height: 60.0,
                width: MediaQuery.of(context).size.width * 0.8,
                child: Center(
                  child: Text("Create Account",
                  style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            GestureDetector(
              onTap: (){
                Navigator.of(context)
                    .push(new MaterialPageRoute(builder: (context) => LoginPage()));
              },
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(15)
                ),
                height: 60.0,
                width: MediaQuery.of(context).size.width * 0.8,
                child: Center(
                  child: Text("Login"),
                ),
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            GestureDetector(
              onTap: (){
//                Navigator.of(context)
//                    .push(new MaterialPageRoute(builder: (context) => Dashboard()));

              ContinueWithOutCreateAccount();

              },
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(15)
                ),
                height: 60.0,
                width: MediaQuery.of(context).size.width * 0.8,
                child: Center(
                  child: Text("Continue without Creating Account"),
                ),
              ),
            ),
            SizedBox(
              height: 15.0,
            ),
          ],
        ),
      ),
    );
  }

  void ContinueWithOutCreateAccount() async
  {

    print("Ghost Account");

    try {

      CircularIndicator();

      if (Platform.isIOS) {
        specificDeviceInfo = await deviceInfo.iosInfo;
        deviceId = specificDeviceInfo.identifierForVendor;
      } else {
        specificDeviceInfo = await deviceInfo.androidInfo;
        deviceId = specificDeviceInfo.androidId;
      }

      currentTime = DateTime.now();

//      print("Device Id:" + deviceId);
//      print("Current Time:"+ currentTime.toString());

      print("Current Username:"+"${deviceId + " " + currentTime.toString()}");



      String url = signUpApi;
      String json = '{"username":"${deviceId + " " + currentTime.toString()}","first_name":"N/A","last_name":"N/A","password":"${userPassword}","source":"N/A","role":"ghost_user"}';

      print("Json is:"+json);

      Response response = await post(url, headers: header, body: json);

      print("Response status code:"+response.statusCode.toString());
      print("Response body:"+response.body.toString());

      if(response.statusCode == 201)
      {
        Navigator.of(context, rootNavigator: true).pop();
        loginData = jsonDecode(response.body);

        print("User Name:"+loginData["userdata"]["signup_data"]["username"]);
        print("First Name:"+loginData["userdata"]["signup_data"]["first_name"]);
        print("Last Name:"+loginData["userdata"]["signup_data"]["last_name"]);
        print("Profile Name:"+loginData["userdata"]["profile_name"]);
        print("Source:"+loginData["userdata"]["signup_data"]["source"]);
        print("Role:"+loginData["userdata"]["signup_data"]["role"]);
        print("Nonce Base:"+loginData["userdata"]["current_nonce_base"].toString());
        print("Password:"+loginData["password"]);
        print("User Id:"+loginData["user_id"]);

        await shPrefApp.SetUserPersonalInformation(loginData["userdata"]["signup_data"]["username"], loginData["userdata"]["signup_data"]["first_name"], loginData["userdata"]["signup_data"]["last_name"], loginData["userdata"]["profile_name"], loginData["userdata"]["signup_data"]["source"], loginData["userdata"]["signup_data"]["role"]);
        await shPrefApp.SetUserAppInformation(loginData["userdata"]["current_nonce_base"], loginData["password"], loginData["user_id"]);
        await shPrefApp.SetSignUpInformation(loginData["userdata"]["current_nonce_base"], loginData["user_id"]);

        //ToastFunction("Login Successfully");
        //Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => FirstTimeScreen()));
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
            FirstTimeScreen(0)), (Route<dynamic> route) => false);

      }

      else if(response.statusCode == 400)
      {
        Navigator.of(context, rootNavigator: true).pop();
        ShowErrorDialog(jsonDecode(response.body)["message"]);
      }

      else
      {
        Navigator.of(context, rootNavigator: true).pop();
        ShowErrorDialog("Your Internet is not Working Properly");
      }

    }

    catch(e) {
      print("Error is"+e.toString());
      Navigator.of(context, rootNavigator: true).pop();
      ShowErrorDialog("Check Your Internet Connection");
//      print("Error:"+e.toString());
    }

  }

  void CircularIndicator()
  {
    AppAlertDialog.ShowDialog(context,"Logging in");

  }

  void ShowErrorDialog(String ErrorMessage)
  {

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: (){

            },
            child: Dialog(
              insetPadding:EdgeInsets.symmetric(horizontal: 25.0),
              shape: RoundedRectangleBorder(
                  borderRadius:
                  BorderRadius.circular(20.0)
              ), //this right here
              child:  Column(
                mainAxisSize: MainAxisSize.min,

                children: [
                  SizedBox(height: MediaQuery.of(context).size.height*0.015,),
                  Image(image: AssetImage("images/errorimage.png"),width: 50.0,height: 50.0,),
                  SizedBox(height: MediaQuery.of(context).size.height*0.02,),
                  Container(
                    margin: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.05,right: MediaQuery.of(context).size.width*0.05),
                    child:Text(ErrorMessage,style: TextStyle(color: Color.fromARGB(0XFF, 0X3E, 0X40, 0X46)),textScaleFactor: 1.2,textAlign: TextAlign.center,),
                  ),

                  SizedBox(height: MediaQuery.of(context).size.height*0.02,),

                  GestureDetector(

                    onTap: (){
                      Navigator.pop(context);
                    },

                    child:Container(
                      //width: MediaQuery.of(context).size.width*0.5,
                      //height: MediaQuery.of(context).size.height*0.05,
                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.05,right: MediaQuery.of(context).size.width*0.05,top: MediaQuery.of(context).size.height*0.015,bottom: MediaQuery.of(context).size.height*0.015),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50.0),
                        color: Color(0xff00587A).withOpacity(0.8),
                      ),
                      child: Text("Okay, Acknowledge",style: TextStyle(color:Color.fromARGB(0XFF, 0XFF, 0XFF, 0XFF),fontWeight: FontWeight.bold),textScaleFactor: 0.9,),
                    ),
                  ),

                  SizedBox(height: MediaQuery.of(context).size.height*0.025,),


                ],
              ),

            ),
          );
        });

  }

  void ToastFunction(String message)
  {
    Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Color(0xff00587A).withOpacity(0.7),
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }

}
