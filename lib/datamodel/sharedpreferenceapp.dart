import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';


class SharedPreferenceApp
{
  Future SetUserPersonalInformation(String UserName,String UserFirstName,String UserLastName,String UserProfileName,String UserSource,String UserRole) async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("username", UserName);
    prefs.setString("userfirstname", UserFirstName);
    prefs.setString("userlastname", UserLastName);
    prefs.setString("userprofilename", UserProfileName);
    prefs.setString("usersource", UserSource);
    prefs.setString("userrole", UserRole);
  }

  Future SetUserAppInformation(int UserNonceBase,String UserPassword,String UserId) async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt("usernoncebase", UserNonceBase);
    prefs.setString("userpassword", UserPassword);
    prefs.setString("userid", UserId);

  }

  Future SetSignUpInformation(int UserNonceBase,String UserId) async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt("usernoncebasesignup", UserNonceBase);
    prefs.setString("useridsignup", UserId);

  }


  Future SetCategoryAbout(String about ) async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("categoryabout", about);

  }

  Future SetFormAvailable(int available) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt("formavailable", available);
  }

  Future SetPubStubCategoryAbout(String about ) async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString("pubstubcategoryabout", about);

  }

  Future SetPubStubFormAvailable(int available) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt("pubstubformavailable", available);
  }

  Future<int> GetFormAvailable() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt("formavailable");
  }

  Future<String> GetUserName() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("username");
  }

  Future<String> GetUserFirstName() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("userfirstname");
  }

  Future<String> GetUserLastName() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("userlastname");
  }

  Future<String> GetUserProfileName() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("userprofilename");
  }

  Future<String> GetUserSource() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("usersource");
  }

  Future<String> GetUserRole() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("userrole");
  }

  Future<int> GetUserNonceBase() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt("usernoncebase");
  }

  Future<String> GetUserPassword() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("userpassword");
  }

  Future<String> GetUserId() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("userid");
  }

  Future<int> GetUserNonceBaseSignUp() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt("usernoncebasesignup");
  }

  Future<String> GetUserIdSignUp() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("useridsignup");
  }


  Future<String> GetCategoryAbout() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("categoryabout");
  }


  Future<String> GetPubStubCategoryAbout() async
  {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString("pubstubcategoryabout");
  }



}
