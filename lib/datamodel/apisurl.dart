import 'package:flutter/material.dart';



// -------------- header ----------------- //
Map<String, String> header = {"Content-Type": "application/json"};


//base url
//http://ec2-3-95-53-126.compute-1.amazonaws.com:3700





// ------------------- Registeration Apis ----------------------- //

const String signUpApi = "https://pscore-sandbox-6.herokuapp.com/signup/form?app=snapfill";
const String loginApi = "https://pscore-sandbox-6.herokuapp.com/login/user?app=snapfill";
const String confirmEmailApi = "https://pscore-sandbox-5.herokuapp.com/utils/inspect/10779215329/";



// -------------------------- Setting Apis ---------------------------------//
const String changePasswordApi = "https://pscore-sandbox-6.herokuapp.com/profile/changePassword/";



// -------------------------- Document Data --------------------------//
const String downloadDocumentData = "https://pscore-sandbox-3.herokuapp.com/utils/download/A/";