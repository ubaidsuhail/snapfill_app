import 'package:flutter/material.dart';
import 'package:snap_fill/Connect-to-Linkdedn/linkedin-two.dart';
import 'package:snap_fill/DashboardScreens/Scan_Licence_2.dart';





class ConnectToLikedIn extends StatefulWidget {
  @override
  _ScanLicensesState createState() => _ScanLicensesState();
}

class _ScanLicensesState extends State<ConnectToLikedIn> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 50.0,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [

                Row(
                  children: [
                    Icon(Icons.arrow_back_ios,size: 18,),
                    SizedBox(width: 5.0,),
                    Text("9/10",
                      textScaleFactor: 1.2,
                      style: TextStyle(color: Color(0xff00587A),),
                    ),
                  ],
                ),
                GestureDetector(
                  onTap: (){
                    // Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => Dashboard()));
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        color: Color.fromRGBO(0, 54, 61, 0.1),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    width: 100.0,
                    height: 40.0,
                    // background: rgba(0, 54, 61, 0.1)

                    child: Center(
                      child: Text("Skip Setup"),
                    ),
                  ),
                )
              ],
            ),
          ),

          SizedBox(height: 20.0,),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25.0),
            child: Text("Connect to Likedin",
              style: TextStyle(color: Color(0xff00587A),fontSize: 18),
            ),
          ),Padding(
            padding: const EdgeInsets.symmetric(horizontal: 25.0,vertical: 10),
            child: Text("Connect your account to LinkedIn to get your employment and certification info",
              style: TextStyle(color: Colors.black,fontSize: 12),
            ),
          ),
          SizedBox(height: 25.0,),
          Padding(

            padding: const EdgeInsets.symmetric(horizontal: 25.0),
            child: Container(
              height: 70.0,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [

                    BoxShadow(
                        color: Colors.blueGrey.withOpacity(0.1),
                        spreadRadius: 5,
                        blurRadius: 5
                    )
                  ]
              ),
              child: Padding(
                padding: const EdgeInsets.only(left: 15),
                child: Row(
                  // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Image(image: AssetImage("images/likedin.png"),
                      width: 50.0,
                      height: 50.0,
                    ),
                    SizedBox(width: 10.0,),
                    Text("Connect to Likedin",
                      style: TextStyle(color: Color(0xff00587A),),
                      textScaleFactor: 1.1,
                    )
                  ],),
              ),

            ),
          ),
          // Padding(
          //   padding: const EdgeInsets.symmetric(horizontal: 25.0,vertical: 15),
          //   child: Text("My Licenses & IDs",
          //     style: TextStyle(color: Color(0xff00587A),fontSize: 16),
          //   ),
          // ),
          // SizedBox(height: MediaQuery.of(context).size.height *0.18),
          // Center(
          //   child: Image(image: AssetImage("images/folder.png"),width: 60.0,height: 60.0,),
          // ),

          Spacer(),


          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              GestureDetector(
                onTap: (){
                  Navigator.of(context).pop();
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(15)
                  ),
                  height: 40.0,
                  width: MediaQuery.of(context).size.width *0.35,
                  child: Center(
                    child: Text("Back",style: TextStyle(color: Color(0xff00587A)),),
                  ),
                ),
              ),
              GestureDetector(
                onTap: (){
                  Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => ConnectToLikedInTwo()));
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: Color(0xff00587A),
                      borderRadius: BorderRadius.circular(15)
                  ),
                  height: 40.0,
                  width: MediaQuery.of(context).size.width *0.35,
                  child: Center(
                    child:  Text("Next",style: TextStyle(color: Colors.white),),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(height:10.0),
        ],
      ),
    );
  }
}
