import 'package:flutter/material.dart';



class LikedinAuth extends StatefulWidget {
  @override
  _LikedinAuthState createState() => _LikedinAuthState();
}

class _LikedinAuthState extends State<LikedinAuth> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 60.0,),
                    Center(
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(15),
                          color: Colors.grey.withOpacity(0.3)
                        ),
                        width: MediaQuery.of(context).size.width * 0.8,
                        height: 35,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text("AA"),
                              Text("https://linkeind.com/auth2/"),
                              Icon(Icons.refresh)

                            ],
                          ),
                        ),
                      ),
                    ),
              SizedBox(height: 10.0,),

              Divider(thickness: 1,),
              SizedBox(height: 10.0,),

              Text("Email"),
              SizedBox(
                height: 10.0,
              ),
              Container(
                decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(10)
                ),

                child: TextField(

                  decoration: new InputDecoration(
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    hintText: '',
                  ),
                ),
              ),
              SizedBox(
                height: 25.0,
              ),
              Text("Password"),
              SizedBox(
                height: 10.0,
              ),
              Container(
                decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(10)
                ),

                child: TextField(

                  decoration: new InputDecoration(
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                        borderRadius: BorderRadius.circular(10)
                    ),
                    hintText: '',
                  ),
                ),
              ),

              SizedBox(height: MediaQuery.of(context).size.height *0.1),


              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  GestureDetector(
                    onTap: (){
                      Navigator.of(context).pop();
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.grey[200],
                          borderRadius: BorderRadius.circular(15)
                      ),
                      height: 40.0,
                      width: MediaQuery.of(context).size.width *0.35,
                      child: Center(
                        child: Text("Deny",style: TextStyle(color: Color(0xff00587A)),),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: (){
                      // Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => LikedinAuth()));
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: Color(0xff00587A),
                          borderRadius: BorderRadius.circular(15)
                      ),
                      height: 40.0,
                      width: MediaQuery.of(context).size.width *0.35,
                      child: Center(
                        child:  Text("Allow",style: TextStyle(color: Colors.white),),
                      ),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
