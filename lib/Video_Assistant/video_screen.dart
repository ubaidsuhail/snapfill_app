import 'package:flutter/material.dart';

class VideoAssistant extends StatefulWidget {
  @override
  _VideoAssistantState createState() => _VideoAssistantState();
}

class _VideoAssistantState extends State<VideoAssistant> {
  bool isTap = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(


      body: Stack(children: [

        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(
                  "images/video.png"
              ),
              fit: BoxFit.fitWidth
            ),

          ),
        ),


        Padding(
          padding: const EdgeInsets.only(top: 50.0),
          child: Align(
            alignment: Alignment.topCenter,
            child: Container(
              decoration: BoxDecoration(
                  color:Colors.white24,
                borderRadius: BorderRadius.circular(15)
              ),

              height: 70.0,
              width: MediaQuery.of(context).size.width * 0.7,
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text("List any other Social Security numbers you have used",
                  style: TextStyle(color: Colors.white),
                    textScaleFactor: 1.0,),
                ),
              ),
            ),
          ),
        ),
        isTap == true ?
        Padding(
          padding: const EdgeInsets.only(bottom: 150.0),
          child: Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              decoration: BoxDecoration(
                  color:Colors.white,
                  borderRadius: BorderRadius.circular(15)
              ),

              height: 70.0,
              width: MediaQuery.of(context).size.width * 0.7,
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text("Answer: 02273613392                  Confirm",
                    style: TextStyle(color: Colors.black),
                    textScaleFactor: 1.0,),
                ),
              ),
            ),
          ),
        ):Container(),
          isTap == false ?
          Padding(
            padding: const EdgeInsets.only(bottom: 50.0),
            child: Align(

              alignment: Alignment.bottomCenter,
              child: GestureDetector(
                onTap: (){
                  setState(() {
                    isTap = true;
                  });
                },
                child: Image(
                  image: AssetImage(
                      "images/audio-tap.png"
                  ),
                  width: 80.0,
                  height: 80.0,
                ),
              ),
            ),
          ):Padding(
            padding: const EdgeInsets.only(bottom: 50.0),
            child: Align(

              alignment: Alignment.bottomCenter,
              child: GestureDetector(
                onTap: (){
                  setState(() {
                    isTap = false;
                  });
                },
                child: Image(
                  image: AssetImage(
                      "images/stop-audio.png"
                  ),
                  width: 80.0,
                  height: 80.0,
                ),
              ),
            ),
          )
      ],),
    );
  }
}
