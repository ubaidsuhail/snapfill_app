import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';
import 'package:snap_fill/DashboardScreens/first_page.dart';
import 'package:snap_fill/datamodel/apisurl.dart';
import 'package:snap_fill/datamodel/appalertdialog.dart';
import 'package:snap_fill/datamodel/sharedpreferenceapp.dart';
import 'package:snap_fill/forms_pages/unemployment_insurance_form.dart';

import '../constant.dart';
import 'new_unemployement_insurance_form.dart';



class FormPageOne extends StatefulWidget {
  @override
  _FormPageOneState createState() => _FormPageOneState();
}

class _FormPageOneState extends State<FormPageOne> {

  String userId;
  Random rand = Random();
  int randomNumber;
  String jsonData = "";

  int randomNounce;
  int currentNounceBase;

  String waitMessage = "Waiting";

  SharedPreferenceApp shPrefApp = SharedPreferenceApp();
  Map<String,dynamic> unEmploymentData = Map<String,dynamic>();

  List unEmploymentList = List();




  @override
  void initState() {
    // TODO: implement initState
    Timer(new Duration(milliseconds: 1000), () {
      GetUnEmploymentFormList();
    });
    super.initState();

  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
//      appBar: AppBar(
//        backgroundColor: Colors.white,
//        title: Text(""),
//        elevation: 0.0,
//      ),

      body: Column(
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.height*0.08,
          ),
          Padding(
              padding: const EdgeInsets.symmetric(horizontal: 25),
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text("Snapfill",style: TextStyle(color: Color(0xff00587A),fontWeight: FontWeight.w700),textScaleFactor: 1.3,)
              )
          ),
          SizedBox(
            height: 10.0,
          ),
//          GestureDetector(
//              onTap: ()
//              {
//
//              },
//
//              child:Padding(
//                padding: const EdgeInsets.symmetric(horizontal: 20),
//                child: Align(
//                  alignment: Alignment.centerLeft,
//                  child: Container(
//                    padding: EdgeInsets.all(15.0),
//                    decoration: BoxDecoration(
//                      borderRadius: BorderRadius.circular(10.0),
//                      color: kAppMainColor,
//                    ),
//                    child:Text("Add Form",style: TextStyle(color: Colors.white),textScaleFactor: 1.0,),
//                  ),
//                ),
//              )
//          ),
          SizedBox(height:10.0),
          waitMessage == "Waiting"
          ?
          Container()
          :
          waitMessage == "Not Wait" ?


              Expanded(
                  child:ListView.builder(
            itemCount: unEmploymentList.length,
            itemBuilder: (BuildContext context , int index){
            return Container(
              // height: 80.0,
              margin: EdgeInsets.only(top:10.0,bottom:10.0,left:20.0,right:20.0),
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(15),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey.withOpacity(0.15),
                        blurRadius: 5,
                        spreadRadius: 5
                    )
                  ]
              ),
              child:
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Image(image: AssetImage('images/file.png'),width: 45,height: 45,),
                    SizedBox(width: 20.0,),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        SizedBox(height:5.0),
                        Text("Unemployment Insurance Form",style: TextStyle(fontWeight: FontWeight.w600),),

                        SizedBox(height:10.0),

                        Row(
                          children: [
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 5.0,vertical: 5.0),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5.0),
                                  color: Color(0xff009B7C)
                              ),
                              height: 25.0,
                              //width: 100.0,
                              child: Center(
                                child: Text("Form Submitted",style: TextStyle(color: Colors.white),textScaleFactor: 0.9,),
                              ),
                            ),

                           // Spacer(),
                            SizedBox(width:20.0),

                            GestureDetector(
                              onTap: () {
                                print("hello world");
                                print("UnEmployment Index:"+unEmploymentList[index].toString());
                                Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => NewUnEmployementInsuranceForm(1,unEmploymentList[index])));
                               // print(
                                  //  userLicenseList[index]["about"].toString());
//                                Navigator.push(context, MaterialPageRoute(
//                                    builder: (BuildContext context) =>
//                                        VerifyDocument(
//                                            "documents", "licenses_and_IDs",
//                                            userLicenseList[index]["about"]
//                                                .toString(), 0,
//                                            userLicenseList[index])));
                              },
                              child: Image(image: AssetImage("images/edit.png"),
                                width: 20.0,
                                height: 20.0,
                              ),
                            ),

                            SizedBox(width: 20.0,),


                            GestureDetector(
                              onTap: () {
//                                DeleteData(index,
//                                    userLicenseList[index]["about"].toString());

                                DeleteUnEmploymentFrom(index,unEmploymentList[index]["about"]);
                              },
                              child: Image(
                                image: AssetImage("images/trash.png"),
                                width: 20.0,
                                height: 20.0,
                              ),
                            ),

                            SizedBox(width: 20.0,),
                          ],
                        )
                      ],
                    )
                  ],
                ),
              ),

            );

            }

            )

              )


              :
          Container(
            margin: EdgeInsets.only(top:10.0,bottom:10.0,left:20.0,right:20.0),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(15),
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey.withOpacity(0.15),
                      blurRadius: 5,
                      spreadRadius: 5
                  )
                ]
            ),
            child: Padding(
              padding: EdgeInsets.all(8.0),
              child: Row(
                children: [
                  Image(image: AssetImage('images/file.png'),width: 45,height: 45,),
                  SizedBox(width: 20.0,),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      SizedBox(height:5.0),
                      Text("Unemployment Insurance Form",style: TextStyle(fontWeight: FontWeight.w600),),

                      SizedBox(height:10.0),

                      Row(
                        children: [
                          GestureDetector(
                            onTap: ()
                            {
                              AddForm();
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(horizontal: 5.0,vertical: 5.0),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5.0),
                                  color: Color(0xff009B7C)
                              ),
                              height: 25.0,
                              //width: 100.0,
                              child: Center(
                                child: Text("Click to fill form",style: TextStyle(color: Colors.white),textScaleFactor: 0.9,),
                              ),
                            ),
                          ),

                        ],
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void AddForm() async
  {
    int checkForm = await shPrefApp.GetFormAvailable();
    print("check form");
    print(checkForm);
//    if(checkForm == 1) {
      Navigator.push(context, MaterialPageRoute(
          builder: (BuildContext context) =>
              NewUnEmployementInsuranceForm(0, {})));
   // }
//    else
//      {
//        ToastFunction("Please upload license first");
//      }
      }

  void GetUnEmploymentFormList() async
  {

    try {
      CircularIndicator("Getting unemployment form list");
      randomNounce = await GetNounce();
      print("Random nounce:"+randomNounce.toString());
      userId = await shPrefApp.GetUserIdSignUp();
      print("user id:"+userId.toString());


      String url = "https://pscore-sandbox-3.herokuapp.com/utils/download/A/$randomNounce/$userId?app=snapfill";
      String json = '{"search_params" : {  "location" : "forms", "category" : "unemployment", "fetch_last" : "30" },"optimistic":"true"}';

      // print("Json is:" + json);


      Response response = await post(url, headers: header, body: json);

      print("Response status code:" + response.statusCode.toString());
      print("Response body Unemplyment:" + response.body);



      if(response.statusCode == 200)
      {
        Navigator.of(context, rootNavigator: true).pop();
        unEmploymentData = jsonDecode(response.body);

        print("Un employment Data:"+unEmploymentData.toString());

        unEmploymentList = unEmploymentData["data"]["package"]["forms"];
        print("form length:"+unEmploymentList.length.toString());

        setState(() {
          waitMessage = "Not Wait";
        });

      }

      else if(response.statusCode == 404)
      {
        Navigator.of(context, rootNavigator: true).pop();
        //Navigator.of(context, rootNavigator: true).pop();
//        await GetUnEmploymentFormList();

        setState(() {
          waitMessage = "No Forms";
        });


      }


      else if(response.statusCode == 400)
      {
        Navigator.of(context, rootNavigator: true).pop();
        await GetUnEmploymentFormList();
      }

      else{
        Navigator.of(context, rootNavigator: true).pop();
        setState(() {
          waitMessage = "Form Problem";
        });
//        ShowErrorDialog("Connection Timed Out");
      }

    }
    catch(e) {
      Navigator.of(context, rootNavigator: true).pop();
      setState(() {
        waitMessage = "Internet Connection Error";
      });
      //ShowErrorDialog("Check Your Internet Connection");
      print("Error:"+e.toString());
    }



  }



  void DeleteUnEmploymentFrom(index,deleteAbout) async
  {

    try {
      CircularIndicator("Deleting form");
      randomNounce = await GetNounce();
      print("Random nounce:"+randomNounce.toString());
      userId = await shPrefApp.GetUserIdSignUp();
      print("user id:"+userId.toString());


      String url = "https://pscore-sandbox-6.herokuapp.com/utils/delete/A/$randomNounce/$userId?app=snapfill";
      String json = '{"search_params" : {  "location" : "forms", "category" : "unemployment", "about" : "$deleteAbout" }}';

      // print("Json is:" + json);


      Response response = await post(url, headers: header, body: json);

      print("Response status code:" + response.statusCode.toString());
      print("Response body Unemplyment:" + response.body);



      if(response.statusCode == 200)
      {
        Navigator.of(context, rootNavigator: true).pop();
        unEmploymentList.removeAt(index);

        setState(() {

        });
      }


      else if(response.statusCode == 400)
      {
        Navigator.of(context, rootNavigator: true).pop();
        ShowErrorDialog("Please Retry");
      }

      else{
        Navigator.of(context, rootNavigator: true).pop();
        ShowErrorDialog("Error in Deleting Form");
      }

    }
    catch(e) {
      ShowErrorDialog("Check Your Internet Connection");
      print("Error:"+e.toString());
    }


  }



  Future<int> GetNounce() async
  {
    currentNounceBase =  await shPrefApp.GetUserNonceBaseSignUp();
    print("Cached nounce:"+currentNounceBase.toString());
    //print("Random rand"+rand.nextInt(1000).toString());

    randomNumber = rand.nextInt(99);
    print("Random Number:"+randomNumber.toString());
    return currentNounceBase * randomNumber;

  }


  void CircularIndicator(String message)
  {
    AppAlertDialog.ShowDialog(context,message);

  }

  void ShowErrorDialog(String ErrorMessage)
  {

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: (){

            },
            child: Dialog(
              insetPadding:EdgeInsets.symmetric(horizontal: 25.0),
              shape: RoundedRectangleBorder(
                  borderRadius:
                  BorderRadius.circular(20.0)
              ), //this right here
              child:  Column(
                mainAxisSize: MainAxisSize.min,

                children: [
                  SizedBox(height: MediaQuery.of(context).size.height*0.015,),
                  Image(image: AssetImage("images/errorimage.png"),width: 50.0,height: 50.0,),
                  SizedBox(height: MediaQuery.of(context).size.height*0.02,),
                  Container(
                    margin: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.05,right: MediaQuery.of(context).size.width*0.05),
                    child:Text(ErrorMessage,style: TextStyle(color: Color.fromARGB(0XFF, 0X3E, 0X40, 0X46)),textScaleFactor: 1.2,textAlign: TextAlign.center,),
                  ),

                  SizedBox(height: MediaQuery.of(context).size.height*0.02,),

                  GestureDetector(

                    onTap: (){
                      Navigator.pop(context);
                    },

                    child:Container(
                      //width: MediaQuery.of(context).size.width*0.5,
                      //height: MediaQuery.of(context).size.height*0.05,
                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.05,right: MediaQuery.of(context).size.width*0.05,top: MediaQuery.of(context).size.height*0.015,bottom: MediaQuery.of(context).size.height*0.015),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50.0),
                        color: Color(0xff00587A).withOpacity(0.8),
                      ),
                      child: Text("Okay, Acknowledge",style: TextStyle(color:Color.fromARGB(0XFF, 0XFF, 0XFF, 0XFF),fontWeight: FontWeight.bold),textScaleFactor: 0.9,),
                    ),
                  ),

                  SizedBox(height: MediaQuery.of(context).size.height*0.025,),


                ],
              ),

            ),
          );
        });

  }

  void ToastFunction(String message)
  {
    Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Color(0xff00587A).withOpacity(0.7),
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }



}
