import 'package:flutter/material.dart';
import 'package:snap_fill/DashboardScreens/dashboard.dart';
import 'package:snap_fill/constant.dart';

class FormSubmitted extends StatefulWidget {
  @override
  _FormSubmittedState createState() => _FormSubmittedState();
}

class _FormSubmittedState extends State<FormSubmitted> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      body: Center(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Form Submitted"),
              Image(image: AssetImage('images/form-sub.png')),

              GestureDetector(
                onTap: (){
                  Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
                      Dashboard(0)), (Route<dynamic> route) => false);
                },
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: kAppMainColor
                  ),
                  height: 60.0,
                  width: MediaQuery.of(context).size.width * 0.8,
                  child: Center(
                    child: Text(
                      'Take me to Home',style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
