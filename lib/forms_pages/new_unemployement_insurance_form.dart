import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:crypto/crypto.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';
import 'package:snap_fill/Audio_Assistant/audio_screen.dart';
import 'package:snap_fill/Chat_Screen/chatting-page.dart';
import 'package:snap_fill/DashboardScreens/dashboard.dart';
import 'package:snap_fill/Video_Assistant/video_screen.dart';
import 'package:snap_fill/constant.dart';
import 'package:snap_fill/datamodel/apisurl.dart';
import 'package:snap_fill/datamodel/appalertdialog.dart';
import 'package:snap_fill/datamodel/sharedpreferenceapp.dart';
import 'package:snap_fill/forms_pages/form_submitted.dart';
import 'package:snap_fill/pay_stub_forms/pay_stub_unemployment_form.dart';

class NewUnEmployementInsuranceForm extends StatefulWidget {

  int editUnEmployment;
  Map<String,dynamic> editUnEmploymentForm = Map<String,dynamic>();
  NewUnEmployementInsuranceForm(this.editUnEmployment,this.editUnEmploymentForm);


  @override
  _NewUnEmployementInsuranceFormState createState() => _NewUnEmployementInsuranceFormState();
}

class _NewUnEmployementInsuranceFormState extends State<NewUnEmployementInsuranceForm> {

  bool _value = false;
  bool isTapped = false;
  bool isOpen = false;

  double height = 120.0;
  double speedHeight = 40.0;

  int workRadio = 0;
  int militaryVeteranRadio = 0;

  TextEditingController _fullName = TextEditingController();
  TextEditingController _birthDate = TextEditingController();
  TextEditingController _gender = TextEditingController();
  TextEditingController _state = TextEditingController();
  TextEditingController _address = TextEditingController();
  TextEditingController _country = TextEditingController();


  String categoryDocumentAbout = "";
  String pubStubsDocumentAbout= "";
  String unEmploymentFormAbout = "";
  Map<String,dynamic> unEmploymentPayStubData=Map<String,dynamic>();

  SharedPreferenceApp shPrefApp = SharedPreferenceApp();

  String userId;
  Random rand = Random();
  int randomNumber;
  String jsonData = "";

  int randomNounce;
  int currentNounceBase;

  String waitMessage = "Waiting";

  Map<String,dynamic> unEmploymentData = Map<String,dynamic>();

  String emailHash;
  DateTime dateTime;

  String unEmploymentLocation = "";
  String unEmploymentCategory = "";
  String unEmploymentAbout = "";
  String unEmploymentNounce = "";
  String unEmploymentUserId = "";
  String unEmploymentOwner = "";


  bool state1 = false;
  bool state2 = false;


  bool military1 = false;
  bool military2 = false;
  String ethnicAnswer="American Indian or Alaska Native";
  List<Map<String,dynamic>> ethnicDropdown = [
    {
      "questions":"American Indian or Alaska Native",
    },
    {
      "questions":"Asian",
    },
    {
      "questions":"Black or African American.",
    },
    {
      "questions":"Hispanic or Latino",
    },
    {
      "questions":"Native Hawaiian or Other Pacific Islander",
    },
    {
      "questions":"White",
    },

  ];


  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if(widget.editUnEmployment == 0)
      {

        Timer(new Duration(milliseconds: 1000), () {
          UploadUserUnEmployementForm();
        });
      }
      else
        {
          EditUserUnemploymentForm();
        }


  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: waitMessage == "Waiting" ? Colors.white : kAppMainColor,
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: GestureDetector(
            onTap: (){
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back_ios,color: Colors.black,size: 20,)
        ),
        title: Text("Unemployment Insurance Form",style: TextStyle(color: Color(0xff00587A)),),
        elevation: 0.0,
      ),


      body: waitMessage == "Waiting"
      ?
      Container()

      :

      SingleChildScrollView(
        child: Column(
          children: [


            Container(
        height: MediaQuery.of(context).size.height *0.76,
              //color: Colors.red,
              child: Stack(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [

                  Container(
                    height: MediaQuery.of(context).size.height *0.76,
                    color: Colors.white,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width*0.05),
                      child: SingleChildScrollView(
                        child: Column(
                         // crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Proceed with filling the unemployment insurance form and verify the fields that have been auto-filled.",textScaleFactor: 1,),
                            SizedBox(height: 15.0,),
                            Divider(
                              thickness: 1,
                            ),
                            SizedBox(height: 15.0,),

                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("1.",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),),
                                Expanded(child: Text("Did you work in a state other than California during the last 18 months?",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),)),
                              ],
                            ),

                            SizedBox(height: 10.0,),
                        Row(
                          children: <Widget>[
                            SizedBox(width: 10.0,),
                            SizedBox(
                              width: 20.0,
                              height: 20.0,
                              child: Radio(
                                value: 1,
                                activeColor: kAppMainColor.withOpacity(0.8),
                                groupValue: workRadio,
                                onChanged: workRadioChange,
                              ),
                            ),
                            SizedBox(width: 10.0,),
                            Text(
                              'Yes',
                              style: TextStyle(color:kAppMainColor),textScaleFactor: 1.1,
                            )
                          ]
                        ),

                            SizedBox(height: 10.0,),

                            Row(
                                children: <Widget>[
                                  SizedBox(width:10.0),
                                  SizedBox(
                                    width: 20.0,
                                    height: 20.0,
                                    child: Radio(
                                      value: 2,
                                      activeColor: kAppMainColor.withOpacity(0.8),
                                      groupValue: workRadio,
                                      onChanged: workRadioChange,
                                    ),
                                  ),
                                  SizedBox(width: 10.0,),
                                  Text(
                                    'No',
                                    style: TextStyle(color:kAppMainColor),textScaleFactor: 1.1,
                                  )
                                ]
                            ),

                            SizedBox(height: 15.0,),

                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("2.",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),),
                                Expanded(child: Text("What is your Full Name?",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),)),
                              ],
                            ),

                            SizedBox(height: 10.0,),
                            Container(
                              height: 45,
                              decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.circular(10)
                              ),

                              child: TextField(
                                controller: _fullName,
                                decoration: new InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  hintText: 'Full Name',
                                  contentPadding: EdgeInsets.only(top:5.0,left: 10.0,right:5.0),
                                ),
                              ),
                            ),


                            SizedBox(height: 15.0,),

                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("3.",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),),
                                Expanded(child: Text("What is your Birth Date?",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),)),
                              ],
                            ),

                            SizedBox(height: 10.0,),
                            Container(
                              height: 45,
                              decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.circular(10)
                              ),

                              child: TextField(
                                controller: _birthDate,
                                decoration: new InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  hintText: 'Birth Rate',
                                  contentPadding: EdgeInsets.only(top:5.0,left: 10.0,right:5.0),
                                ),
                              ),
                            ),



                            SizedBox(height: 15.0,),

                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("4.",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),),
                                Expanded(child: Text("What is your gender?",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),)),
                              ],
                            ),

                            SizedBox(height: 10.0,),
                            Container(
                              height: 45,
                              decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.circular(10)
                              ),

                              child: TextField(
                                controller: _gender,
                                decoration: new InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  hintText: 'Gender',
                                  contentPadding: EdgeInsets.only(top:5.0,left: 10.0,right:5.0),
                                ),
                              ),
                            ),


                            SizedBox(height: 15.0,),

                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("5.",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),),
                                Expanded(child: Text("Do you have a Driver License issued to you by a State/entity? If Yes, What STATE?",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),)),
                              ],
                            ),

                            SizedBox(height: 10.0,),
                            Container(
                              height: 45,
                              decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.circular(10)
                              ),

                              child: TextField(
                                controller: _state,
                                decoration: new InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  hintText: 'State',
                                  contentPadding: EdgeInsets.only(top:5.0,left: 10.0,right:5.0),
                                ),
                              ),
                            ),


                            SizedBox(height: 15.0,),

                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("6.",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),),
                                Expanded(child: Text("What is your mailing address? (Include your city, State, and ZIP code)",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),)),
                              ],
                            ),

                            SizedBox(height: 10.0,),
                            Container(
                              height: 45,
                              decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.circular(10)
                              ),

                              child: TextField(
                                controller: _address,
                                decoration: new InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  hintText: 'Address',
                                  contentPadding: EdgeInsets.only(top:5.0,left: 10.0,right:5.0),
                                ),
                              ),
                            ),


                            SizedBox(height: 15.0,),

                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("7.",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),),
                                Expanded(child: Text("If you do not live in California, what is the name of the County in which you live?",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),)),
                              ],
                            ),

                            SizedBox(height: 10.0,),
                            Container(
                              height: 45,
                              decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.circular(10)
                              ),

                              child: TextField(
                                controller: _country,
                                decoration: new InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                      borderRadius: BorderRadius.circular(10)
                                  ),
                                  hintText: 'County',
                                  contentPadding: EdgeInsets.only(top:5.0,left: 10.0,right:5.0),
                                ),
                              ),
                            ),



                            SizedBox(height: 15.0,),

                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("8.",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),),
                                Expanded(child: Text("What race or ethnic group do you identify with?",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),)),
                              ],
                            ),

                            SizedBox(height: 10.0,),
                            Container(

                              height: 45,
                              padding: EdgeInsets.only(left:5.0),
                              decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.circular(10)
                              ),

                              child:DropdownButtonHideUnderline(
                                child: DropdownButton<String>(
                                  //isDense: true,

                                  //hint: new Text("Select Bank"),
                                  value: ethnicAnswer,
                                  onChanged: (String newValue) {
                                    print(newValue);
                                    setState(() {
                                      ethnicAnswer = newValue;

                                    });

                                    print(ethnicAnswer);
                                  },
                                  items: ethnicDropdown.map((Map map) {
                                    return new DropdownMenuItem<String>(
                                        value: map["questions"].toString(),

                                        // value: _mySelection,
                                        child: Container(
                                            width: MediaQuery.of(context).size.width*0.8,
                                            child:Text(map["questions"],textScaleFactor: 1.0,)
                                        )

                                    );
                                  }).toList(),

                                ),
                              ),
                            ),


                            SizedBox(height: 15.0,),

                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text("9.",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),),
                                Expanded(child: Text("Are you a Military Veteran?",textScaleFactor: 1.1,style: TextStyle(color: kAppMainColor),)),
                              ],
                            ),

                            SizedBox(height: 10.0,),
                            Row(
                                children: <Widget>[
                                  SizedBox(width: 10.0,),
                                  SizedBox(
                                    width: 20.0,
                                    height: 20.0,
                                    child: Radio(
                                      value: 1,
                                      activeColor: kAppMainColor.withOpacity(0.8),
                                      groupValue: militaryVeteranRadio,
                                      onChanged: militaryVeteranRadioChange,
                                    ),
                                  ),
                                  SizedBox(width: 10.0,),
                                  Text(
                                    'Yes',
                                    style: TextStyle(color:kAppMainColor),textScaleFactor: 1.1,
                                  )
                                ]
                            ),

                            SizedBox(height: 10.0,),

                            Row(
                                children: <Widget>[
                                  SizedBox(width:10.0),
                                  SizedBox(
                                    width: 20.0,
                                    height: 20.0,
                                    child: Radio(
                                      value: 2,
                                      activeColor: kAppMainColor.withOpacity(0.8),
                                      groupValue: militaryVeteranRadio,
                                      onChanged: militaryVeteranRadioChange,
                                    ),
                                  ),
                                  SizedBox(width: 10.0,),
                                  Text(
                                    'No',
                                    style: TextStyle(color:kAppMainColor),textScaleFactor: 1.1,
                                  )
                                ]
                            ),


                            SizedBox(height: MediaQuery.of(context).size.height*0.1,),

                          ],
                        ),
                      ),
                    ),
                  ),

                  Padding(
                    padding:  EdgeInsets.only(right: 15,bottom: speedHeight),
                    child: SpeedDial(
                      // both default to 16
                      marginRight: 18,
                      marginBottom: 20,
                      animatedIcon: AnimatedIcons.menu_close,
                      animatedIconTheme: IconThemeData(size: 22.0,color: Colors.white),
                      // this is ignored if animatedIcon is non null
                      // child: Icon(Icons.add),
                      // visible: _dialVisible,
                      // If true user is forced to close dial manually
                      // by tapping main button and overlay is not rendered.
                      closeManually: true,
                      curve: Curves.bounceIn,
                      overlayColor: Colors.black,
                      overlayOpacity: 0.5,
                      onOpen: () {
                        setState(() {
                          isOpen = true;
                        });
                      },
                      onClose: () {
                        setState(() {
                          isOpen = false;
                        });
                      },

                      tooltip: 'Speed Dial',
                      heroTag: 'speed-dial-hero-tag',
                      backgroundColor:Color(0xff00587A),
                      foregroundColor: Colors.white,
                      elevation: 0.0,
                      shape: CircleBorder(),
                      children: [
                        SpeedDialChild(
                            child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Image(image: AssetImage('images/videoicon.png'),),
                            ),
                            backgroundColor: Color(0xff00587A),
                            // label: 'First',
                            labelStyle: TextStyle(fontSize: 18.0),
                            onTap: (){
                              Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => VideoAssistant()));
                            }
                        ),
                        SpeedDialChild(
                            child: Icon(Icons.keyboard_voice),
                            backgroundColor: Color(0xff00587A),
                            // label: 'Second',
                            labelStyle: TextStyle(fontSize: 18.0),
                            onTap: (){
                              Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => AuidoAssitant()));
                            }
                        ),
                        SpeedDialChild(
                            child: Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Image(image: AssetImage('images/chat.png'),),
                            ),
                            backgroundColor: Color(0xff00587A),
                            // label: 'Third',
                            labelStyle: TextStyle(fontSize: 18.0),
                            onTap: (){
                              Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => Chatting()));
                            }
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),

          Container (

            decoration: BoxDecoration( color: kAppMainColor,
                borderRadius: BorderRadius.only(topRight: Radius.circular(15),topLeft: Radius.circular(15))
            ),
            //duration: Duration (seconds: 1),
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height*0.1,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [

                Container(
                  decoration: BoxDecoration(color: Colors.white,
                      borderRadius: BorderRadius.circular(10)
                  ),
                  height: 7,
                  width: 200.0,

                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    GestureDetector(
                      onTap: ()
                      {
                        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) =>
                            Dashboard(0)), (Route<dynamic> route) => false);
                      },
                      child: Container(
                        decoration: BoxDecoration(color: kBlueColor.withOpacity(0.5),
                            borderRadius: BorderRadius.circular(10)
                        ),
                        height: 35,
                        width: MediaQuery.of(context).size.width * 0.35,
                        child: Center(
                          child: Text(
                            "Discard",
                            style: TextStyle(
                                color: Colors.white
                            ),
                          ),
                        ),

                      ),
                    ),
                    GestureDetector(
                      onTap: (){
                        //alertDialog();
                        VerifyUnEmploymentForm();
                      },
                      child: Container(
                        decoration: BoxDecoration(color: Colors.white,
                            borderRadius: BorderRadius.circular(10)
                        ),
                        height: 35,
                        width: MediaQuery.of(context).size.width * 0.35,
                        child: Center(
                          child: Text(
                            "Submit",
                            style: TextStyle(
                                color: kAppMainColor
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              ],),
          ),
          ],
        ),
      ),

    );

  }
  void alertDialog(){
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius:
                BorderRadius.circular(20.0)), //this right here
            child: Container(
              height: 200,
              child: Padding(
                padding: const EdgeInsets.all(18.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [

                    Text("Want to leave?",
                      textScaleFactor: 1.1,
                      style: TextStyle(
                          color: Color(0xff00587A)
                      ),
                    ),

                    Text("Before you leave, it’s advised that you save your progress so far in this form.",
                      textScaleFactor: 0.9,
                      style: TextStyle(
                          color: Color(0xff00587A)
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        GestureDetector(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => FormSubmitted()));
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                                border: Border.all(color: kAppMainColor,width: 0.8)
                            ),
                            height: 35.0,
                            width: 120.0,
                            child: Center(
                              child: Text("Save & Exit"),
                            ),
                          ),
                        ),
                        SizedBox(width: 12.0,),
                        Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color: kAppMainColor
                          ),
                          height: 35.0,
                          width: 90.0,
                          child: Center(
                            child: Text("Stay",style: TextStyle(color: Colors.white),),
                          ),
                        ),
                        SizedBox(width: 10.0,),
                      ],
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }


  void workRadioChange(int radioValue)
  {
    setState(() {
      workRadio = radioValue;
    });
  }

  void militaryVeteranRadioChange(int militaryRadioValue)
  {
    setState(() {
      militaryVeteranRadio = militaryRadioValue;
    });
  }


  void UploadUserUnEmployementForm() async
  {

    try {
      CircularIndicator("Downloading forms");
      categoryDocumentAbout = await shPrefApp.GetCategoryAbout();
      randomNounce = await GetNounce();
      print("Random nounce:"+randomNounce.toString());
      userId = await shPrefApp.GetUserIdSignUp();
      print("user id:"+userId.toString());

      emailHash = await GenerateMd5();
      print("Email hash:"+emailHash.toString());

      dateTime = DateTime.now();

      unEmploymentFormAbout = "UnemploymentForms_"+emailHash+dateTime.toString();


      categoryDocumentAbout = await shPrefApp.GetCategoryAbout();
      pubStubsDocumentAbout = await shPrefApp.GetPubStubCategoryAbout();



      //jsonData = '{"unemployment_form_1" : {"Did you work in a state other than California during the last 18 months?" : { "YES" : false, "NO" : false },"What is your full name?" : "documents::licenses_and_IDs::$categoryDocumentAbout::First Name;documents::licenses_and_IDs::$categoryDocumentAbout::Last Name","What is your birth date?" : "documents::licenses_and_IDs::$categoryDocumentAbout::Date of Birth","What is your gender?" : "documents::licenses_and_IDs::$categoryDocumentAbout::Sex","Do you have a Driver License issued to you by a State/entity? If Yes, What STATE" : "documents::licenses_and_IDs::$categoryDocumentAbout::Nationality!\'_INPUT_\'.split('').slice(-2)[0]","What is your mailing address? (Include your city, State, and ZIP code)" : "documents::licenses_and_IDs::$categoryDocumentAbout::Nationality","If you do not live in California, what is the name of the County in which you live?" : "","What race or ethnic group do you identify with? Check one of the following" : {},"Are you a Military Veteran?" : { "YES" : false, "NO" : false }}}';

      jsonData = '{"unemployment_form_1" : {"Did you work in a state other than California during the last 18 months?" : { "YES" : false, "NO" : false },"What is your full name?" : "documents::licenses_and_IDs::$categoryDocumentAbout::First Name;documents::licenses_and_IDs::$categoryDocumentAbout::Last Name","What is your birth date?" : "documents::licenses_and_IDs::$categoryDocumentAbout::Date of Birth","What is your gender?" : "documents::licenses_and_IDs::$categoryDocumentAbout::Sex","Do you have a Driver License issued to you by a State/entity? If Yes, What STATE" : "documents::licenses_and_IDs::$categoryDocumentAbout::Nationality!\'_INPUT_\'.split('').slice(-2)[0]","What is your mailing address? (Include your city, State, and ZIP code)" : "documents::licenses_and_IDs::$categoryDocumentAbout::Nationality","If you do not live in California, what is the name of the County in which you live?" : "","What race or ethnic group do you identify with? Check one of the following" : {},"Are you a Military Veteran?" : { "YES" : false, "NO" : false },"Did you work in Canada during the last 18 months?" : false,"Employer Name" : "documents::paystubs::$pubStubsDocumentAbout::Employer Name","Employer Mailing Address(include your city, state and ZIP code)" : "documents::paystubs::$pubStubsDocumentAbout::Company Address","Total Wages(in \$)" : "documents::paystubs::$pubStubsDocumentAbout::NetPay","What is the complete name of your very last employer?" : "documents::paystubs::$pubStubsDocumentAbout::Employer Name","What is the mailing address of your very last employer?" : "documents::paystubs::$pubStubsDocumentAbout::Company Address","Is the physical address of your very last employer the same as their mailing address?" : true,"What is the telephone number of your very last employer at their physical address?" : "documents::paystubs::$pubStubsDocumentAbout::Company Phone Number","Are you currently working for or do you expect to work for any school or educational institution or a public or nonprofit employer performing school-related work?" : false,"Provide your employment and wages information for the past 18 months.":"","Dates Worked":"","How were you paid? (e.g., weekly, monthly, etc.)?":"","Did you work full-time or part-time?":"","How many hours did you work per week?":"","What are your gross wages for your last week of work? For Unemployment Insurance purposes, a week begins on Sunday and ends the following Saturday.":""}}';
      print("'$jsonData'");


      var uri =Uri.parse("https://pscore-sandbox-1.herokuapp.com/utils/upload/A/$randomNounce/$userId?app=snapfill");

      print("Uri is:"+uri.toString());
      var request = MultipartRequest("POST",uri);

      request.fields['location'] = 'forms';
      request.fields['category'] = 'unemployment';
      request.fields['about'] = unEmploymentFormAbout;
      request.fields['upload_json'] = '$jsonData';


      var response = await request.send();

      print("Response:"+response.statusCode.toString());


      if(response.statusCode == 200)
      {

        await GetUserUnEmploymentData();

      }

      else if(response.statusCode == 400)
      {
        Navigator.of(context, rootNavigator: true).pop();
        await UploadUserUnEmployementForm();
      }

      else{
        Navigator.of(context, rootNavigator: true).pop();
        setState(() {
          waitMessage = "Not Wait";
        });

        ToastFunction("Connection Timed Out");
      }

    }
    catch(e) {
      Navigator.of(context, rootNavigator: true).pop();
      setState(() {
        waitMessage = "Not Wait";
      });
      ToastFunction("Check Your Internet Connection");
      print("Error:"+e.toString());
    }

  }




  void GetUserUnEmploymentData() async
  {
    try {
      randomNounce = await GetNounce();
      print("Random nounce:"+randomNounce.toString());
      userId = await shPrefApp.GetUserIdSignUp();
      print("user id:"+userId.toString());



      categoryDocumentAbout = await shPrefApp.GetCategoryAbout();


      String url = "https://pscore-sandbox-3.herokuapp.com/utils/download/A/$randomNounce/$userId?app=snapfill";
      String json = '{"search_params" : {  "location" : "forms", "category" : "unemployment", "about" : "$unEmploymentFormAbout" },"optimistic":"true"}';

      // print("Json is:" + json);


      Response response = await post(url, headers: header, body: json);

      print("Response status code:" + response.statusCode.toString());
      print("Response body Unemplyment:" + response.body);



      if(response.statusCode == 200)
      {
        Navigator.of(context, rootNavigator: true).pop();
         unEmploymentData = jsonDecode(response.body);

         print("Full Name:"+unEmploymentData["data"]["package"]["forms"][0]["What is your full name?"]);


        unEmploymentPayStubData = unEmploymentData["data"]["package"]["forms"][0];

         _fullName.text = unEmploymentData["data"]["package"]["forms"][0]["What is your full name?"];
         _birthDate.text = unEmploymentData["data"]["package"]["forms"][0]["What is your birth date?"];
         _gender.text = unEmploymentData["data"]["package"]["forms"][0]["What is your gender?"];
         _address.text = unEmploymentData["data"]["package"]["forms"][0]["What is your mailing address? (Include your city, State, and ZIP code)"];


         unEmploymentLocation = unEmploymentData["data"]["package"]["forms"][0]["location"];
         unEmploymentCategory = unEmploymentData["data"]["package"]["forms"][0]["category"];
         unEmploymentAbout = unEmploymentData["data"]["package"]["forms"][0]["about"];
         unEmploymentNounce = unEmploymentData["data"]["package"]["forms"][0]["nonce"];
         unEmploymentUserId = unEmploymentData["data"]["package"]["forms"][0]["user_id"];
         unEmploymentOwner = unEmploymentData["data"]["package"]["forms"][0]["owner"];


        setState(() {
          waitMessage = "Not Wait";
        });

      }

      else if(response.statusCode == 400)
      {
        Navigator.of(context, rootNavigator: true).pop();
        await GetUserUnEmploymentData();
      }

      else{
        Navigator.of(context, rootNavigator: true).pop();
        setState(() {
          waitMessage = "Not Wait";
        });
        ToastFunction("Connection Timed Out");
      }

    }
    catch(e) {
      Navigator.of(context, rootNavigator: true).pop();
      setState(() {
        waitMessage = "Not Wait";
      });
      ToastFunction("Check Your Internet Connection");
      print("Error:"+e.toString());
    }
  }



 void VerifyUnEmploymentForm() async
 {

   if(widget.editUnEmployment == 0) {


     print("unemployment pay stub data is :$unEmploymentPayStubData");

     unEmploymentPayStubData["Did you work in a state other than California during the last 18 months?"]["YES"] = workRadio == 1 ? true : false;
     unEmploymentPayStubData["Did you work in a state other than California during the last 18 months?"]["NO"] = workRadio == 2 ? true : false;
     unEmploymentPayStubData["What is your full name?"] = _fullName.text;
     unEmploymentPayStubData["What is your birth date?"] = _birthDate.text;
     unEmploymentPayStubData["What is your gender?"] = _gender.text;
     unEmploymentPayStubData["Do you have a Driver License issued to you by a State/entity? If Yes, What STATE"] =  _state.text;
    unEmploymentPayStubData["What is your mailing address? (Include your city, State, and ZIP code)"] =  _address.text;
      unEmploymentPayStubData["If you do not live in California, what is the name of the County in which you live?"] = _country.text;
     unEmploymentPayStubData["What race or ethnic group do you identify with? Check one of the following"]["race1"] = ethnicAnswer;
     unEmploymentPayStubData["Are you a Military Veteran?"]["YES"] = militaryVeteranRadio == 1 ? true : false;
     unEmploymentPayStubData["Are you a Military Veteran?"]["NO"] = militaryVeteranRadio == 2 ? true : false;


     Navigator.push(context, MaterialPageRoute(
         builder: (BuildContext context) =>
             PayStubUnEmployementInsuranceForm(unEmploymentPayStubData)));
   }
   else
     {
       Navigator.push(context, MaterialPageRoute(
           builder: (BuildContext context) =>
               PayStubUnEmployementInsuranceForm(widget.editUnEmploymentForm)));
     }
//   print(unEmploymentLocation);
//   print(unEmploymentCategory);
//   print(unEmploymentAbout);
//   print(unEmploymentNounce);
//   print(unEmploymentUserId);
//   print(unEmploymentOwner);
//
//   print("word radio"+workRadio.toString());
//
//   workRadio == 1 ? state1 = true : workRadio == 2 ? state2 = true : state1 = false;
//
//   militaryVeteranRadio == 1 ? military1 = true : militaryVeteranRadio == 2 ? military2 = true : military1 = false;
//
//
//   try {
//     CircularIndicator("Verify UnEmployment form");
//     randomNounce = await GetNounce();
//     print("Random nounce:"+randomNounce.toString());
//     userId = await shPrefApp.GetUserIdSignUp();
//     print("user id:"+userId.toString());
//
//
//     jsonData = '{"unemployment_form_1" :{"Did you work in a state other than California during the last 18 months?": {"YES": $state1,"NO": $state2},"What is your full name?": "${_fullName.text}","What is your birth date?": "${_birthDate.text}","What is your gender?": "${_gender.text}","Do you have a Driver License issued to you by a State/entity? If Yes, What STATE": "${_state.text}","What is your mailing address? (Include your city, State, and ZIP code)": "${_address.text}","If you do not live in California, what is the name of the County in which you live?": "${_country.text}","What race or ethnic group do you identify with? Check one of the following": {"race1": "${ethnicAnswer}"},"Are you a Military Veteran?": {"YES": $military1,"NO": $military2},"location": "$unEmploymentLocation","category": "$unEmploymentCategory","about": "$unEmploymentAbout","nonce": "$unEmploymentNounce","user_id": "$unEmploymentUserId","owner": "$unEmploymentOwner"} }';
//
//
//     print("'$jsonData'");
//
//
//     var uri =Uri.parse("https://pscore-sandbox-1.herokuapp.com/utils/upload/A/$randomNounce/$userId?app=snapfill");
//
//     print("Uri is:"+uri.toString());
//     var request = MultipartRequest("POST",uri);
//
//
//     request.fields['location'] = 'forms';
//     request.fields['category'] = 'unemployment';
//     request.fields['about'] = unEmploymentFormAbout;
//     request.fields['upload_json'] = '$jsonData';
//
//
//     var response = await request.send();
//
//     print("Response:"+response.statusCode.toString());
//
//
//     if(response.statusCode == 200)
//     {
//       Navigator.of(context, rootNavigator: true).pop();
//       print("response");
//       Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => FormSubmitted()));
//
//     }
//
//     else if(response.statusCode == 400)
//     {
//       Navigator.of(context, rootNavigator: true).pop();
//       ShowErrorDialog("Error:Please Retry");
//     }
//
//     else{
//       Navigator.of(context, rootNavigator: true).pop();
//       ShowErrorDialog("Connection Timed Out");
//     }
//
//   }
//   catch(e) {
//     Navigator.of(context, rootNavigator: true).pop();
//     ShowErrorDialog("Check Your Internet Connection");
//     print("Error:"+e.toString());
//   }

 }


  void EditUserUnemploymentForm()
  {
    print("In new employment form:"+widget.editUnEmploymentForm.toString());




    _fullName.text = widget.editUnEmploymentForm["What is your full name?"];
    _birthDate.text = widget.editUnEmploymentForm["What is your birth date?"];
    _gender.text = widget.editUnEmploymentForm["What is your gender?"];
    _state.text = widget.editUnEmploymentForm["Do you have a Driver License issued to you by a State/entity? If Yes, What STATE"];
    _address.text = widget.editUnEmploymentForm["What is your mailing address? (Include your city, State, and ZIP code)"];
    _country.text = widget.editUnEmploymentForm["If you do not live in California, what is the name of the County in which you live?"];
    ethnicAnswer = widget.editUnEmploymentForm["What race or ethnic group do you identify with? Check one of the following"]["race1"];

    unEmploymentLocation = widget.editUnEmploymentForm["location"];
    unEmploymentCategory = widget.editUnEmploymentForm["category"];
    unEmploymentAbout = widget.editUnEmploymentForm["about"];
    unEmploymentNounce = widget.editUnEmploymentForm["nonce"];
    unEmploymentUserId = widget.editUnEmploymentForm["user_id"];
    unEmploymentOwner = widget.editUnEmploymentForm["owner"];

    unEmploymentFormAbout = widget.editUnEmploymentForm["about"];


    //unEmploymentData["Did you work in a state other than California during the last 18 months?"]["YES"] == true ? workRadio = 1 : workRadio == 2 ? state2 = true : state1 = false;

    if(widget.editUnEmploymentForm["Did you work in a state other than California during the last 18 months?"]["YES"] == true)
    {
      workRadio = 1;
    }

    if(widget.editUnEmploymentForm["Did you work in a state other than California during the last 18 months?"]["NO"] == true)
    {
      workRadio = 2;
    }


    if(widget.editUnEmploymentForm["Are you a Military Veteran?"]["YES"] == true)
    {
      militaryVeteranRadio = 1;
    }

    if(widget.editUnEmploymentForm["Are you a Military Veteran?"]["NO"] == true)
    {
      militaryVeteranRadio = 2;
    }

      setState(() {
      waitMessage = "Not Wait";
    });

  }



  Future<int> GetNounce() async
  {
    currentNounceBase =  await shPrefApp.GetUserNonceBaseSignUp();
    print("Cached nounce:"+currentNounceBase.toString());
    //print("Random rand"+rand.nextInt(1000).toString());

    randomNumber = rand.nextInt(99);
    print("Random Number:"+randomNumber.toString());
    return currentNounceBase * randomNumber;

  }


  Future<String> GenerateMd5() async {

    String userName = await shPrefApp.GetUserName();
    // print("MD:"+md5.convert(utf8.encode(userName)).toString());
    return md5.convert(utf8.encode(userName)).toString();
  }




  void CircularIndicator(String message)
  {
    AppAlertDialog.ShowDialog(context,message);

  }
  void ShowErrorDialog(String ErrorMessage)
  {

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return WillPopScope(
            onWillPop: (){

            },
            child: Dialog(
              insetPadding:EdgeInsets.symmetric(horizontal: 25.0),
              shape: RoundedRectangleBorder(
                  borderRadius:
                  BorderRadius.circular(20.0)
              ), //this right here
              child:  Column(
                mainAxisSize: MainAxisSize.min,

                children: [
                  SizedBox(height: MediaQuery.of(context).size.height*0.015,),
                  Image(image: AssetImage("images/errorimage.png"),width: 50.0,height: 50.0,),
                  SizedBox(height: MediaQuery.of(context).size.height*0.02,),
                  Container(
                    margin: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.05,right: MediaQuery.of(context).size.width*0.05),
                    child:Text(ErrorMessage,style: TextStyle(color: Color.fromARGB(0XFF, 0X3E, 0X40, 0X46)),textScaleFactor: 1.2,textAlign: TextAlign.center,),
                  ),

                  SizedBox(height: MediaQuery.of(context).size.height*0.02,),

                  GestureDetector(

                    onTap: (){
                      Navigator.pop(context);
                    },

                    child:Container(
                      //width: MediaQuery.of(context).size.width*0.5,
                      //height: MediaQuery.of(context).size.height*0.05,
                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width*0.05,right: MediaQuery.of(context).size.width*0.05,top: MediaQuery.of(context).size.height*0.015,bottom: MediaQuery.of(context).size.height*0.015),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(50.0),
                        color: Color(0xff00587A).withOpacity(0.8),
                      ),
                      child: Text("Okay, Acknowledge",style: TextStyle(color:Color.fromARGB(0XFF, 0XFF, 0XFF, 0XFF),fontWeight: FontWeight.bold),textScaleFactor: 0.9,),
                    ),
                  ),

                  SizedBox(height: MediaQuery.of(context).size.height*0.025,),


                ],
              ),

            ),
          );
        });

  }

  void ToastFunction(String message)
  {
    Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Color(0xff00587A).withOpacity(0.7),
      textColor: Colors.white,
      fontSize: 16.0,
    );
  }




}
