import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:snap_fill/Audio_Assistant/audio_screen.dart';
import 'package:snap_fill/Chat_Screen/chatting-page.dart';
import 'package:snap_fill/Video_Assistant/video_screen.dart';
import 'package:snap_fill/constant.dart';
import 'package:snap_fill/forms_pages/form_submitted.dart';

class UnemployemntInsuranceForm extends StatefulWidget {
  @override
  _UnemployemntInsuranceFormState createState() => _UnemployemntInsuranceFormState();
}

class _UnemployemntInsuranceFormState extends State<UnemployemntInsuranceForm> {
  String Specialization = "Yes";
  List States = [
    {'id': 0, 'name': 'Yes', 'image': 'assets/icons/surgeon_doctor.jpg'},
    {'id': 1, 'name': 'Patient', 'image': 'assets/icons/patient.png'},
    {'id': 2, 'name': 'Nurse', 'image': 'assets/icons/nurse_main.jpg'},
    {'id': 3, 'name': 'Partner', 'image': 'assets/icons/vendor.png'}
  ];String Question = "Questions 1- 5";
  List Questions = [
    {'id': 0, 'name': 'Questions 1- 5', 'image': 'assets/icons/surgeon_doctor.jpg'},
    {'id': 1, 'name': 'Patient', 'image': 'assets/icons/patient.png'},
    {'id': 2, 'name': 'Nurse', 'image': 'assets/icons/nurse_main.jpg'},
    {'id': 3, 'name': 'Partner', 'image': 'assets/icons/vendor.png'}
  ];

  bool _value = false;
  bool isTapped = false;
  bool isOpen = false;

  double height = 120.0;
  double speedHeight = 130.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        leading: GestureDetector(
          onTap: (){
            Navigator.pop(context);
          },
            child: Icon(Icons.arrow_back_ios,color: Colors.black,size: 20,)
        ),
        title: Text("Unemployment Insurance Form",style: TextStyle(color: Color(0xff00587A)),),
        elevation: 0.0,
      ),


      body: Stack(
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [

            Container(
              height: MediaQuery.of(context).size.height *0.8,
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Text("Proceed with filling the unemployment insurance form and verify the fields that have been auto-filled.",textScaleFactor: 1,),
                  ),
                  SizedBox(height: 15,),
                  Divider(
                    thickness: 1,
                  ),
                  SizedBox(height: 15,),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Text("1. Did you work in a state the unemployment insurance form and verify the fields that have been auto-filled.",textScaleFactor: 1,style: TextStyle(color: kAppMainColor),),
                  ),
                  SizedBox(height: 15,),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Container(
                      height: 45.0,
                      // width: MediaQuery.of(context).size.width * 0.9,
                      decoration: BoxDecoration(
                        color: Colors.grey[200],
                        borderRadius: BorderRadius.circular(
                          10.0,
                        ),

                      ),
                      padding: EdgeInsets.only(left: 0, right: 0, top: 0),
//                    color: Colors.white,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            child: DropdownButtonHideUnderline(
                              child: ButtonTheme(
                                alignedDropdown: true,
                                child: DropdownButton<String>(
                                  value: Specialization,
                                  iconSize: 30,
                                  icon: (Icon(
                                    Icons.arrow_drop_down,
                                    color: Colors.black,
                                  )),
                                  style: TextStyle(
                                    color: Colors.black54,
                                    fontSize: 16,
                                  ),
                                  hint: Text(
                                    'Select Usertype',
                                    textScaleFactor: 0.85,
                                    style: TextStyle(color: Colors.black),
                                  ),
                                  onChanged: (String newValue) {
                                    setState(() {
                                      Specialization = newValue;
                                      print(Specialization);
                                    });
                                  },
                                  items: States?.map((item) {
                                    return new DropdownMenuItem(
                                      child: new Text(
                                        item['name'],
                                        style: TextStyle(
                                            color: Colors.black),
                                      ),
                                      value: item['name'].toString(),
                                    );
                                  })?.toList() ??
                                      [],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 15,),

                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child:
                    Container(
                      height: 45 ,
                      decoration: BoxDecoration(
                          color: Colors.grey[200],
                          borderRadius: BorderRadius.circular(10)
                      ),

                      child:
                      TextField(

                        decoration: new InputDecoration(
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                              borderRadius: BorderRadius.circular(10)
                          ),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                              borderRadius: BorderRadius.circular(10)
                          ),
                          hintText: 'Answer',
                        ),
                      ),
                    ),
                  ),

                  SizedBox(height: 15,),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Text("If yes, check the applicable boxes below",textScaleFactor: 1,style: TextStyle(color: Colors.black),),
                  ),
                    SizedBox(height: 15,),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        children: [
                          Center(
                            child: InkWell(
                              onTap: () {
                                setState(() {
                                  _value = !_value;
                                });
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(color: kAppMainColor),
                                    shape: BoxShape.circle, color: Colors.white),
                                child: Padding(
                                  padding: _value ? EdgeInsets.all(3.0): EdgeInsets.all(5.0),
                                  child: _value
                                      ? Icon(
                                    Icons.check,
                                    size: 12.0,
                                    color: kAppMainColor,
                                  )
                                      : Icon(
                                    Icons.check_box_outline_blank,
                                    size: 8.0,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            )),
                          SizedBox(width: 12,),
                            Text("States Outise Califronia, specify states")
                        ],
                      ),
                    ),
                    SizedBox(height: 15,),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        children: [
                          Center(
                            child: InkWell(
                              onTap: () {
                                setState(() {
                                  _value = !_value;
                                });
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                    border: Border.all(color: kAppMainColor),
                                    shape: BoxShape.circle, color: Colors.white),
                                child: Padding(
                                  padding: _value ? EdgeInsets.all(3.0): EdgeInsets.all(5.0),
                                  child: _value
                                      ? Icon(
                                    Icons.check,
                                    size: 12.0,
                                    color: kAppMainColor,
                                  )
                                      : Icon(
                                    Icons.check_box_outline_blank,
                                    size: 8.0,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            )),
                          SizedBox(width: 12,),
                            Text("Canada")
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 15),
                      child: Text("2. What is your social security number form and verify the fields that have been auto-filled.",textScaleFactor: 1,style: TextStyle(color: kAppMainColor.withOpacity(0.4)),),
                    ),

                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Container(
                        height: 45 ,
                        decoration: BoxDecoration(
                            color: Colors.grey[100],
                            borderRadius: BorderRadius.circular(10)
                        ),

                        child: TextField(

                          decoration: new InputDecoration(
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                borderRadius: BorderRadius.circular(10)
                            ),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                borderRadius: BorderRadius.circular(10)
                            ),
                            hintText: '333-52-4454',
                            hintStyle: TextStyle(color: Colors.grey.withOpacity(0.5))
                          ),
                        ),
                      ),
                    ),

                    SizedBox(height: 15,),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Text("a. If the EDD assigned you state the unemployment insurance form and verify the fields that have been auto-filled.",
                        textScaleFactor: 1,style: TextStyle(color: Colors.black.withOpacity(0.5)),),
                    ),
                    SizedBox(height: 15,),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Container(
                        height: 45 ,
                        decoration: BoxDecoration(
                            color: Colors.grey[100],
                            borderRadius: BorderRadius.circular(10)
                        ),

                        child: TextField(

                          decoration: new InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                  borderRadius: BorderRadius.circular(10)
                              ),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                  borderRadius: BorderRadius.circular(10)
                              ),
                              hintText: 'xxx-xx-xxxx',
                              hintStyle: TextStyle(color: Colors.grey.withOpacity(0.5))
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 15),
                      child: Text("2A. List any other social security numbers you have used?",textScaleFactor: 1,style: TextStyle(color: kAppMainColor.withOpacity(0.4)),),
                    ),

                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Container(
                        height: 45 ,
                        decoration: BoxDecoration(
                            color: Colors.grey[100],
                            borderRadius: BorderRadius.circular(10)
                        ),

                        child: TextField(

                          decoration: new InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                  borderRadius: BorderRadius.circular(10)
                              ),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.grey[200], width: 0.0),
                                  borderRadius: BorderRadius.circular(10)
                              ),
                              hintText: 'xxx-xx-xxxx',
                              hintStyle: TextStyle(color: Colors.grey.withOpacity(0.5))
                          ),
                        ),
                      ),
                    ),
                ],
                ),
              ),
            ),
          Align(
            alignment: Alignment.bottomCenter,
            child: GestureDetector(

              onTap: (){
                  setState(() {
                    height = 220.0;
                    speedHeight= 240.0;
                    isTapped = true;
                  });

              },
              child: AnimatedContainer (
                decoration: BoxDecoration( color: kAppMainColor,
                  borderRadius: BorderRadius.only(topRight: Radius.circular(15),topLeft: Radius.circular(15))
                ),
                duration: Duration (seconds: 1),
                width: MediaQuery.of(context).size.width,
                height: height,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [

                  GestureDetector(
                    onTap: (){
                      setState(() {
                        height = 120.0;
                        speedHeight= 130.0;
                        isTapped = false;
                      });
                    },
                    child: Container(
                      decoration: BoxDecoration(color: Colors.white,
                        borderRadius: BorderRadius.circular(10)
                      ),
                      height: 7,
                      width: 200.0,

                    ),
                  ),
                    isTapped ?
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 25),
                              child: Text("Questions, total of 40 (33 unfilled)",style: TextStyle(color: Colors.white),),
                            ),
                            SizedBox(height: 10,),
                            Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 20),
                              child: Container(
                                height: 45.0,
                                // width: MediaQuery.of(context).size.width * 0.9,
                                decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.circular(
                                    10.0,
                                  ),

                                ),
                                padding: EdgeInsets.only(left: 0, right: 0, top: 0),
//                    color: Colors.white,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Expanded(
                                      child: DropdownButtonHideUnderline(
                                        child: ButtonTheme(
                                          alignedDropdown: true,
                                          child: DropdownButton<String>(
                                            value: Question,
                                            iconSize: 30,
                                            icon: (Icon(
                                              Icons.arrow_drop_down,
                                              color: Colors.black,
                                            )),
                                            style: TextStyle(
                                              color: Colors.black54,
                                              fontSize: 16,
                                            ),
                                            hint: Text(
                                              'Select Usertype',
                                              textScaleFactor: 0.85,
                                              style: TextStyle(color: Colors.black),
                                            ),
                                            onChanged: (String newValue) {
                                              setState(() {
                                                Question = newValue;
                                                print(Question);
                                              });
                                            },
                                            items: Questions?.map((item) {
                                              return new DropdownMenuItem(
                                                child: new Text(
                                                  item['name'],
                                                  style: TextStyle(
                                                      color: Colors.black),
                                                ),
                                                value: item['name'].toString(),
                                              );
                                            })?.toList() ??
                                                [],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        )
                    : Container(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Container(
                          decoration: BoxDecoration(color: kBlueColor.withOpacity(0.5),
                              borderRadius: BorderRadius.circular(10)
                          ),
                          height: 35,
                          width: MediaQuery.of(context).size.width * 0.35,
                          child: Center(
                            child: Text(
                              "Previous Questions",
                              style: TextStyle(
                                color: Colors.white
                              ),
                            ),
                          ),

                        ),
                        GestureDetector(
                          onTap: (){
                            alertDialog();
                          },
                          child: Container(
                            decoration: BoxDecoration(color: Colors.white,
                                borderRadius: BorderRadius.circular(10)
                            ),
                            height: 35,
                            width: MediaQuery.of(context).size.width * 0.35,
                            child: Center(
                              child: Text(
                                "Next Questions",
                                style: TextStyle(
                                    color: kAppMainColor
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    )
                ],),
              ),
            ),
          ),
          Padding(
            padding:  EdgeInsets.only(right: 15,bottom: speedHeight),
            child: SpeedDial(
              // both default to 16
              marginRight: 18,
              marginBottom: 20,
              animatedIcon: AnimatedIcons.menu_close,
              animatedIconTheme: IconThemeData(size: 22.0,color: Colors.white),
              // this is ignored if animatedIcon is non null
              // child: Icon(Icons.add),
              // visible: _dialVisible,
              // If true user is forced to close dial manually
              // by tapping main button and overlay is not rendered.
              closeManually: false,
              curve: Curves.bounceIn,
              overlayColor: Colors.black,
              overlayOpacity: 0.5,
              onOpen: () {
                setState(() {
                  isOpen = true;
                });
              },
              onClose: () {
                setState(() {
                  isOpen = false;
                });
              },

              tooltip: 'Speed Dial',
              heroTag: 'speed-dial-hero-tag',
              backgroundColor:Color(0xff00587A),
              foregroundColor: Colors.white,
              elevation: 0.0,
              shape: CircleBorder(),
              children: [
                SpeedDialChild(
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Image(image: AssetImage('images/videoicon.png'),),
                    ),
                    backgroundColor: Color(0xff00587A),
                    // label: 'First',
                    labelStyle: TextStyle(fontSize: 18.0),
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => VideoAssistant()));
                    }
                ),
                SpeedDialChild(
                  child: Icon(Icons.keyboard_voice),
                  backgroundColor: Color(0xff00587A),
                  // label: 'Second',
                  labelStyle: TextStyle(fontSize: 18.0),
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => AuidoAssitant()));
                    }
                ),
                SpeedDialChild(
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Image(image: AssetImage('images/chat.png'),),
                  ),
                  backgroundColor: Color(0xff00587A),
                  // label: 'Third',
                  labelStyle: TextStyle(fontSize: 18.0),
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => Chatting()));
                    }
                ),
              ],
            ),
          ),
        ],
      ),

    );

  }
  void alertDialog(){
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius:
                BorderRadius.circular(20.0)), //this right here
            child: Container(
              height: 200,
              child: Padding(
                padding: const EdgeInsets.all(18.0),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [

                    Text("Want to leave?",
                      textScaleFactor: 1.1,
                      style: TextStyle(
                          color: Color(0xff00587A)
                      ),
                    ),

                    Text("Before you leave, it’s advised that you save your progress so far in this form.",
                      textScaleFactor: 0.9,
                      style: TextStyle(
                          color: Color(0xff00587A)
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                       GestureDetector(
                         onTap: (){
                           Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => FormSubmitted()));
                         },
                         child: Container(
                           decoration: BoxDecoration(
                             borderRadius: BorderRadius.circular(10.0),
                             border: Border.all(color: kAppMainColor,width: 0.8)
                           ),
                           height: 35.0,
                           width: 120.0,
                           child: Center(
                             child: Text("Save & Exit"),
                           ),
                         ),
                       ),
                        SizedBox(width: 12.0,),
                        Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                             color: kAppMainColor
                          ),
                         height: 35.0,
                         width: 90.0,
                         child: Center(
                           child: Text("Stay",style: TextStyle(color: Colors.white),),
                         ),
                       ),
                        SizedBox(width: 10.0,),
                      ],
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }
}
