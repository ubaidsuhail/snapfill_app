import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:snap_fill/constant.dart';



class Chatting extends StatefulWidget {
  @override
  _ChattingState createState() => _ChattingState();
}

class _ChattingState extends State<Chatting> {
var sendMessgae;
bool isSend = false;
  @override
  Widget build(BuildContext context) {
    TextEditingController _sendController = new TextEditingController();
    return Scaffold(
backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.0,
        leading: GestureDetector(
          onTap: ()
            {
              Navigator.pop(context);
            },
            child: Icon(Icons.arrow_back_ios,size: 25,color: Colors.black,)
        ),
        title: Text("Chatbot Assistant",style: TextStyle(color: kAppMainColor),),
      ),

      body: Stack(
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        // crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.7,
          ),
          SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15.0),
                  child: Text("Proceed with answering the questions, there are 40 questions in total, once you reply to each "),
                ),

                SizedBox(height: 10.0,),
                Divider(
                  thickness: 1.1,
                ),

                SizedBox(height: 10.0,),


                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15.0),
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        border: Border.all(color: kAppMainColor,width: 1.0)
                    ),
                    height: 100.0,
                    width: 250.0,
                    child: Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Text("Chatbot",style: TextStyle(color: kAppMainColor,fontWeight: FontWeight.bold),),
                          Text("Did you work in a state other than California during the last 18 months?",textScaleFactor: 0.9,)
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10,),
                isSend == true ?
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [

                    Container(
                      decoration: BoxDecoration(
                        color: Color(0xff00D2A8).withOpacity(0.4),
                          borderRadius: BorderRadius.circular(15),
                          // border: Border.all(color: kAppMainColor,width: 1.0)
                      ),
                      height: 70.0,
                      width: 300.0,
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Text("You",style: TextStyle(color: Color(0xff00D2A8),fontWeight: FontWeight.bold),),
                            Text(sendMessgae,textScaleFactor: 0.9,)
                          ],
                        ),
                      ),
                    ),
                  ],
                ):Container(),
              ],
            ),
          ),

          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 100.0,
              width: MediaQuery.of(context).size.width,
              color: Colors.lightBlue[100].withOpacity(0.4),

              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                        borderRadius: BorderRadius.circular(15)
                      ),
                      height: 70.0,

                      width: MediaQuery.of(context).size.width * 0.78,
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Icon(Icons.insert_emoticon),
                                SizedBox(width:15),
                                Container(
                                  // color: Colors.red,
                                  height: 30.0,
                                  width: MediaQuery.of(context).size.width * 0.5,
                                  child: TextField(
                                    controller: _sendController,
                                    decoration: InputDecoration(
                                        border: InputBorder.none,
                                        hintText: 'Type your reply',
                                        hintStyle: TextStyle(
                                            color: Colors.black
                                        ),
                                    ),
                                    // onChanged: (text){
                                    //
                                    //   setState(() {
                                    //     sendMessgae = text;
                                    //   });
                                    // },
                                  ),
                                ),
                              ],
                            ),

                            Icon(Icons.mic),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(width: 5,),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: GestureDetector(
                          onTap: (){
                            setState(() {
                              sendMessgae = "Yes,I Worked in Canada";
                              isSend = true;
                            });

                          },
                          child: Image(image: AssetImage('images/send.png'),
                  ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
